// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://dccodashboardapi-dev.azurewebsites.us/',
  tenantId: 'da8e06a2-ecd4-4f83-a1c3-cdf8f2f75525',
  spaKey: '34ad2506-eed8-465b-b882-3eea61989d53',
  apiKey: 'c06a260f-c79d-43c7-b726-3d29c9b6adda'
};

/* localhost is https://localhost:44351/
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
