export const PROJECTED_ANNUAL_INCREASE_PERCENTAGE: any = 20;

export const NO_RECORDS_FOUND: string = 'No Records Found';

export const FISCAL_QUARTERS: { [k: number]: Array<any> } = {
  1: ['October', 'November', 'December'],
  2: ['January', 'February', 'March'],
  3: ['April', 'May', 'June'],
  4: ['July', 'August', 'September']
};

export const MONTHS = ['January','February','March','April','May','June','July','August','September','October','November','December'];

export const MONTHS_IN_FISCAL_YEAR_ORDER = ['October','November','December','January','February','March','April','May','June','July','August','September'];

export const FISCAL_QUARTERS_BY_MONTH: { [k: string]: number } = {
  0: 2,
  1: 2,
  2: 2,
  3: 3,
  4: 3,
  5: 3,
  6: 4,
  7: 4,
  8: 4,
  9: 1,
  10: 1,
  11: 1,
};
