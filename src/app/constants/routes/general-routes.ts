import { IRoute } from '../../model/IRoute';
import { DynamicDashboardComponent } from '../../components/dynamic-dashboard/dynamic-dashboard.component';
import { CONTRACT_COST_DASHBOARD_CONFIG } from '../routes/routeConfigs/general/contract-cost-dashboard-config';
import { TASK_ORDER_LIST_DASHBOARD_CONFIG } from '../routes/routeConfigs/general/task-order-list-dashboard-config';
import { SERVICE_DESK_DASHBOARD_CONFIG } from '../routes/routeConfigs/general/service-desk-dashboard-config';
import { SYSTEMS_OWNERS_ISSOS_DASHBOARD_CONFIG } from '../routes/routeConfigs/general/systems-owners-issos-dashboard-config';
import { INCIDENT_NOTIFICATIONS_DASHBOARD_CONFIG } from '../routes/routeConfigs/general/incident-notifications-dashboard-config';
import { OPERATIONAL_STATUS_DASHBOARD_CONFIG } from '../routes/routeConfigs/general/operational-status-dashboard-config';

export const GENERAL_ROUTES: IRoute[] = [
    {
      title: '',
      path: '',
      redirectTo: 'contract-cost',
      pathMatch: 'full',
    },
    {
      path: 'contract-cost',
      title: 'Contract Cost',
      icon: 'payments',
      component: DynamicDashboardComponent,
      data: {
        dashboardConfig: CONTRACT_COST_DASHBOARD_CONFIG,
      }
    },
    {
      path: 'task-order-list',
      title: 'Task Order List',
      icon: 'format_list_bulleted',
      component: DynamicDashboardComponent,
      data: {
        dashboardConfig: TASK_ORDER_LIST_DASHBOARD_CONFIG,
      }
    },
    {
      path: 'operational-status',
      title: 'Operational Status',
      icon: 'monitor_heart',
      component: DynamicDashboardComponent,
      data: {
        dashboardConfig: OPERATIONAL_STATUS_DASHBOARD_CONFIG,
      }
    },
    {
      path: 'systems-owners-issos',
      title: 'Systems & Owners & ISSOs',
      icon: 'dns',
      component: DynamicDashboardComponent,
      data: {
        dashboardConfig: SYSTEMS_OWNERS_ISSOS_DASHBOARD_CONFIG,
      }
    },
    {
      path: 'service-desk',
      title: 'Service Desk',
      icon: 'support_agent',
      component: DynamicDashboardComponent,
      data: {
        dashboardConfig: SERVICE_DESK_DASHBOARD_CONFIG,
      }
    },
    {
      path: 'incident-notifications',
      title: 'Incident Notifications',
      icon: 'warning',
      component: DynamicDashboardComponent,
      data: {
        dashboardConfig: INCIDENT_NOTIFICATIONS_DASHBOARD_CONFIG,
      }
    },
  ];