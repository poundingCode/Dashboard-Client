import { IRoute } from '../../model/IRoute';
import { DynamicDashboardComponent } from '../../components/dynamic-dashboard/dynamic-dashboard.component';
import { DEVICE_HEALTH_DASHBOARD_CONFIG } from '../routes/routeConfigs/itops/device-health-dashboard-config';
import { CAPACITY_UTILIZATION_DASHBOARD_CONFIG } from '../routes/routeConfigs/itops/capacity-utilization-dashboard-config';
import { NETWORK_PERFORMANCE_DASHBOARD_CONFIG } from '../routes/routeConfigs/itops/network-performance-dashboard-config';

export const ITOPS_ROUTES: IRoute[] = [
  {
    title: '',
    path: '',
    redirectTo: 'device-health',
    pathMatch: 'full',
  },
  {
    path: 'device-health',
    title: 'Device Health',
    icon: 'device_thermostat',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: DEVICE_HEALTH_DASHBOARD_CONFIG,
    }
  },
  {
    path: 'capacity-utilization',
    title: 'Capacity and Utilization',
    icon: 'storage',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: CAPACITY_UTILIZATION_DASHBOARD_CONFIG,
    }
  },
  {
    path: 'network-performance',
    title: 'Network Performance',
    icon: 'network_check',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: NETWORK_PERFORMANCE_DASHBOARD_CONFIG,
    }
  },
];
