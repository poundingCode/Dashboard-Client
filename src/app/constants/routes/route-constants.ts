import { IRoute } from '../../model/IRoute';
import { PURCHASING_ROUTES } from './purchasing-routes';
import { GENERAL_ROUTES } from './general-routes';
import { ITOPS_ROUTES } from './itops-routes';
import { SECOPS_ROUTES } from './secops-routes';
import { SERVOPS_ROUTES } from './servops-routes';
import { MsalGuard } from '@azure/msal-angular';
import { HomeComponent } from '../../views/general/home.component';
import { PurchasingComponent } from '../../views/purchasing/purchasing.component';
import { ItopsComponent } from '../../views/itops/itops.component';
import { SecOpsComponent } from '../../views/secops/secOps.component';
import { ServOpsComponent } from '../../views/servops/servOps.component';

export const BASE_ROUTES: IRoute[] = [
  {
    path: 'home',
    title: 'Home',
    icon: 'home',
    component: HomeComponent,
    children: GENERAL_ROUTES,
  },
  {
    path: 'purchasing',
    title: 'Purchasing',
    icon: 'payments',
    component: PurchasingComponent,
    children: PURCHASING_ROUTES,
    canActivate: [MsalGuard],
  },
  {
    path: 'itops',
    title: 'IT Operations',
    icon: 'computer',
    component: ItopsComponent,
    children: ITOPS_ROUTES,
    canActivate: [MsalGuard],
  },
  {
    path: 'secops',
    title: 'Security Operations',
    icon: 'lock',
    component: SecOpsComponent,
    children: SECOPS_ROUTES,
    canActivate: [MsalGuard],
  },
  {
    path: 'servops',
    title: 'Service Operations',
    icon: 'support-agent',
    component: ServOpsComponent,
    children: SERVOPS_ROUTES,
    canActivate: [MsalGuard],
  }
];
