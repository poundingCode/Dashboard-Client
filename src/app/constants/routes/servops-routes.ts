import { IRoute } from '../../model/IRoute';
import { DynamicDashboardComponent } from '../../components/dynamic-dashboard/dynamic-dashboard.component';
import { INFORMATION_REPOSITORY_DASHBOARD_CONFIG } from '../../constants/routes/routeConfigs/servops/information-repository-dashboard-config';
import { PERFORMANCE_ANALYSIS_DASHBOARD_CONFIG } from '../../constants/routes/routeConfigs/servops/sla/performance-analysis-dashboard-config';
import { HARDWARE_ASSETS_DASHBOARD_CONFIG } from '../../constants/routes/routeConfigs/servops/inventory-management/hardware-assets-dashboard-config';
import { SOFTWARE_ASSETS_DASHBOARD_CONFIG } from '../../constants/routes/routeConfigs/servops/inventory-management/software-assets-dashboard-config';
import { MAINTENANCE_AGREEMENTS_DASHBOARD_CONFIG } from '../../constants/routes/routeConfigs/servops/inventory-management/maintenance-agreements-dashboard-config';
import { SERVICE_MANAGEMENT_DASHBOARD_CONFIG } from '../../constants/routes/routeConfigs/servops/service-management-dashboard-config';

export const SERVOPS_INVENTORY_MANAGEMENT_ROUTES: IRoute[] = [
  {
    path: 'hardware-assets',
    title: 'Hardware Assets',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: HARDWARE_ASSETS_DASHBOARD_CONFIG,
    }
  },
  {
    path: 'software-assets',
    title: 'Software Assets',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: SOFTWARE_ASSETS_DASHBOARD_CONFIG,
    }
  },
  {
    path: 'maintenance-agreements',
    title: 'Maintenance Agreements',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: MAINTENANCE_AGREEMENTS_DASHBOARD_CONFIG,
    }
  }
];

export const SERVOPS_ROUTES: IRoute[] = [
  {
    path: '',
    title: '',
    redirectTo: 'information-repository',
    pathMatch: 'full',
  },
  {
    path: 'information-repository',
    title: 'Information Repository',
    icon: 'folder',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: INFORMATION_REPOSITORY_DASHBOARD_CONFIG,
    }
  },
  {
    path: 'inventory-management',
    title: 'Inventory Management',
    icon: 'inventory',
    component: '',
    children: SERVOPS_INVENTORY_MANAGEMENT_ROUTES,
  },
  {
    path: 'service-management',
    title: 'Service Management',
    icon: 'room_service',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: SERVICE_MANAGEMENT_DASHBOARD_CONFIG,
    }
  },
  {
    path: 'sla-performance',
    title: 'SLA Performance',
    icon: 'star',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: PERFORMANCE_ANALYSIS_DASHBOARD_CONFIG,
    },
  },
];
