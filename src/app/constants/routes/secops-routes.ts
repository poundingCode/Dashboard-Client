import { IRoute } from '../../model/IRoute';
import { DynamicDashboardComponent } from '../../components/dynamic-dashboard/dynamic-dashboard.component';
import { SECURITY_SPLUNK_DASHBOARD_CONFIG } from './routeConfigs/secops/security-splunk-dashboard-config';
import { SECURITY_SNOW_DASHBOARD_CONFIG } from './routeConfigs/secops/security-snow-dashboard-config';

export const SECOPS_SECURITY_ROUTES: IRoute[] = [
  {
    path: '',
    title: '',
    redirectTo: 'security-snow',
    pathMatch: 'full',
  },
  {
    path: 'security-snow',
    title: 'Security (ServiceNow)',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: SECURITY_SNOW_DASHBOARD_CONFIG,
    }
  },
  {
    path: 'security-splunk',
    title: 'Security (Splunk)',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: SECURITY_SPLUNK_DASHBOARD_CONFIG,
    }
  },
];

export const SECOPS_ROUTES: IRoute[] = [
  {
    path: '',
    title: '',
    redirectTo: 'security',
    pathMatch: 'full',
  },
  {
    path: 'security',
    title: 'Security',
    icon: 'security',
    children: SECOPS_SECURITY_ROUTES,
  },
];