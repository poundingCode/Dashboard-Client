import { DashboardConfig } from '../../../../model/component/DynamicDashboard';
import { IncidentNotification } from '../../../../model/general/Incident';
import { Utils } from '../../../../utils/utils';
import { protectedResources } from '../../../../auth-config';

export const INCIDENT_NOTIFICATIONS_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Incident Notifications',
  elements: [
    {
      elementType: 'TileRow',
      elementInputs: {
        tileRowConfig: [
          {
            cardType: 'base-blue',
            cardTitle: 'Severity 1 Status',
            currentValue: {
              tableColumnName: 'SEVERITY',
              tableColumnMath: 'countRecords',
              tableColumnPropertyValues: '1',
            },
            tileWidth: 'tile-third',
            linkUrl: '../../../assets/images/temp/incidents-severity-1-status.png'
          },
          {
            cardType: 'sec-blue',
            cardTitle: 'Severity 2 Status',
            currentValue: {
              tableColumnName: 'SEVERITY',
              tableColumnMath: 'countRecords',
              tableColumnPropertyValues: '2',
            },
            tileWidth: 'tile-third',
            linkUrl: '../../../assets/images/temp/incidents-severity-2-status.png'
          },
          {
            cardType: 'tert-blue',
            cardTitle: 'Severity 3 Status',
            currentValue: {
              tableColumnName: 'SEVERITY',
              tableColumnMath: 'countRecords',
              tableColumnPropertyValues: '3',
            },
            tileWidth: 'tile-third',
            linkUrl: '../../../assets/images/temp/incidents-severity-3-status.png'
          },
        ],
      },
    },
    {
      elementType: 'DataTable',
      elementInputs: {
        tableDataType: IncidentNotification,
        tableResourceUrl: protectedResources.azure.general.IncidentNotifications.endpoint,
        columnFormatter: [
          {
            dataValue: 'NUMBER',
            headerLabel: 'Ticket No.',
            cellFormatter: (row: any) => `${row.NUMBER}`
          },
          {
            dataValue: 'OPENED',
            cellFormatter: (row: any) => `${Utils.formatDateString(row.OPENED)}`
          },
          {
            dataValue: 'SEVERITY',
          },
          {
            dataValue: 'SYSTEM_ENVIRONMENT',
          },
          {
            dataValue: 'SHORT_DESCRIPTION',
            headerLabel: 'Description',
          },
          {
            dataValue: 'STATE',
            headerLabel: 'Status',
            useFilterDropdown: true,
          }
        ]
      }
    },
  ]
};