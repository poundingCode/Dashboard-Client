import { DashboardConfig } from '../../../../model/component/DynamicDashboard';

export const SERVICE_DESK_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Service Desk',
  elements: [
    {
      elementType: 'HTMLContainer',
      elementInputs: {
        sourceUrls: [
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/d6ea72aa1b0d5dd024838407624bcb70',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/1fb6d6231b6855506a57a8e6624bcba5',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/7e0865ff1bd0d1506a57a8e6624bcbb2',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/0966e13f1bd0d1506a57a8e6624bcbc2',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/3c37297f1bd0d1506a57a8e6624bcb11',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/f0f7e1bf1bd0d1506a57a8e6624bcbbc',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/ec84a97b1bd0d1506a57a8e6624bcbd1',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/ac46653f1bd0d1506a57a8e6624bcbd0'
        ],
      }
    },
    {
      elementType: 'IFrame',
      elementInputs: {
        iFrameData: [
          {
            frameTitle: 'SLA timeframe met (list)',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=aa9961731b14d1506a57a8e6624bcb18',
            linkUrl: 'https://dccotest.servicenowservices.com/$pa_dashboard.do?sysparm_dashboard=b125ac7b1b5c91506a57a8e6624bcb81&sysparm_tab=1704217b1bd0d1506a57a8e6624bcb3f&sysparm_cancelable=true&sysparm_editable=false&sysparm_active_panel=false',
          },
          {
            frameTitle: 'Ticket traffic (Trending Closed)',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=24d521fb1bd0d1506a57a8e6624bcb30',
            linkUrl: 'https://dccotest.servicenowservices.com/$pa_dashboard.do?sysparm_dashboard=b125ac7b1b5c91506a57a8e6624bcb81&sysparm_tab=1704217b1bd0d1506a57a8e6624bcb3f&sysparm_cancelable=true&sysparm_editable=false&sysparm_active_panel=false',
          },
          {
            frameTitle: 'SLA timeframe not met (list)',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=2e3925331b14d1506a57a8e6624bcb61',
            linkUrl: 'https://dccotest.servicenowservices.com/$pa_dashboard.do?sysparm_dashboard=b125ac7b1b5c91506a57a8e6624bcb81&sysparm_tab=1704217b1bd0d1506a57a8e6624bcb3f&sysparm_cancelable=true&sysparm_editable=false&sysparm_active_panel=false',
          },
        ]
      }
    }
  ]
};