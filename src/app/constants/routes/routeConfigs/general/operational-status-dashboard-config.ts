import { DashboardConfig } from '../../../../model/component/DynamicDashboard';

export const OPERATIONAL_STATUS_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Operational Information',
  elements: [
    {
      elementType: 'IFrame',
      elementInputs: {
        iFrameData: [
          {
            frameTitle: 'Operational status (e.g., planned, and unplanned outages)',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=ff1810991b991d106a57a8e6624bcb04',
          },
          {
            frameTitle: 'Announcements (e.g., upcoming maintenance, planned outages, etc.)',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=93e471e81bd519106a57a8e6624bcbc2',
          },
          {
            frameTitle: 'Schedules',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=6cd2f9a41bd519106a57a8e6624bcb1d',
          },
        ]
      }
    },
    {
      elementType: 'IFrame',
      elementInputs: {
        iFrameData: [
          {
            frameTitle: 'Network Throughput',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=60070efc1b85d1d024838407624bcb5f',
          },
          {
            frameTitle: 'Average Utilization',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=7cd30e3c1b85d1d024838407624bcb81',
          },
          {
            frameTitle: 'Bandwidth Capacity & Utilization',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=8294cef81b85d1d024838407624bcbc3',
          },
          {
            frameTitle: 'Average Availability',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=8f7cf5b01b85d1d024838407624bcbbb',
          },
          {
            frameTitle: 'Network Latency (ms)',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=ec650e3c1b85d1d024838407624bcb90',
          },
          {
            frameTitle: 'Network Performance',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=87911b5e1bc51dd024838407624bcb77',
          },
          // {
          // frameTitle: 'Outages 6cd',
          // sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=6cd2f9a41bd519106a57a8e6624bcb1d',
          // },
          // {
          // frameTitle: 'Outages',
          // sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=93e471e81bd519106a57a8e6624bcbc2',
          // },
        ]
      }
    }
  ]
};