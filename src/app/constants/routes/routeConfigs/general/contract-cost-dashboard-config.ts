import { DashboardConfig } from '../../../../model/component/DynamicDashboard';
import { protectedResources } from '../../../../auth-config';
import { TotalContractExpenditure } from '../../../../model/purchasing/TotalContractExpenditure';
import { Utils } from '../../../../utils/utils';
import { TileHelper } from '../../../../utils/tile-utils';

export const CONTRACT_COST_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Contract Cost Summary',
  elements: [
    {
      elementType: 'Gauge',
      elementWidth: 'third',
      elementInputs: {
        gaugeResourceUrl: protectedResources.azure.purchasing.AwardedVsContractCeiling.endpoint,
        gaugeOptions: {
          title: 'Awarded vs. Total Contract Ceiling',
          axisTickFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          valueFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          tooltipType: 'gauge',
        },
      }
    },
    {
      elementType: 'Gauge',
      elementWidth: 'third',
      elementInputs: {
        gaugeResourceUrl: protectedResources.azure.purchasing.FundedVsContractCeiling.endpoint,
        gaugeOptions: {
          title: 'Funded vs. Total Contract Ceiling',
          axisTickFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          valueFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          tooltipType: 'gauge',
        },
      }
    },
    {
      elementType: 'Gauge',
      elementWidth: 'third',
      elementInputs: {
        gaugeResourceUrl: protectedResources.azure.purchasing.SpentVsContractCeiling.endpoint,
        gaugeOptions: {
          title: 'Spent vs. Total Contract Ceiling',
          axisTickFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          valueFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          tooltipType: 'gauge',
        },
      }
    },
    {
      elementType: 'TileRow',
      elementInputs: {
        tileRowConfig: [
          {
            cardType: 'base-blue',
            cardTitle: 'Total Contract Ceiling',
            cardResourceUrl: protectedResources.azure.purchasing.ContractSpendingVsCeiling.endpoint,
            currentValue: {
              displayValue: 2680000000,
            },
            prevValue: {
              dataValue: 'CEILING',
              dataValueSecond: 'OBLIGATED',
              dataValueMath: 'subtract',
              periodLabel: 'Remaining Contract Ceiling'
            },
            tileWidth: 'tile-third',
            hideTrend: true,
            numType: '$',
          },
          {
            cardType: 'sec-blue',
            cardTitle: 'Total Contract Value',
            currentValue: {
              tableColumnName: 'CEILING',
              periodType: 'current',
              period: 'year',
              periodTableColumn: 'MAX_MONTH_DATE',
            },
            prevValue: {
              tableColumnName: 'CEILING',
              periodType: 'previous',
              period: 'year',
              periodTableColumn: 'MAX_MONTH_DATE',
              tableColumnMath: 'sum'
            },
            tileWidth: 'tile-third',
            numType: '$',
          },
          {
            cardType: 'tert-blue',
            cardTitle: 'Total Expenditures',
            currentValue: {
              tableColumnName: 'SPENT',
              periodType: 'current',
              period: 'year',
              periodTableColumn: 'MAX_MONTH_DATE',
            },
            prevValue: {
              tableColumnName: 'SPENT',
              periodType: 'previous',
              period: 'year',
              periodTableColumn: 'MAX_MONTH_DATE',
            },
            tileWidth: 'tile-third',
            numType: '$',
          },
        ],
      }
    },
    {
      elementType: 'DataTable',
      elementInputs: {
        tableTitle: 'Total Contract Expenditures vs. Ceiling',
        tableDataType: TotalContractExpenditure,
        tableResourceUrl: protectedResources.azure.purchasing.TaskOrderSpendingVsCeiling.endpoint,
        customDisplayedColumns: ['YEAR', 'COMPONENT_ACRONYM', 'TASK_ORDER_NAME', 'TASK_ORDER_NO', 'CEILING', 'SPENT', 'BURN_TO_DATE', 'REMAINING_CEILING'],
        columnFormatter: [
          {
            dataValue: 'YEAR',
            headerLabel: 'Fiscal Year',
            cellFormatter: (row: any) => `${row.YEAR || '2022'}`
          },
          {
            dataValue: 'MAX_MONTH_DATE',
            headerLabel: 'Fiscal Date',
            cellFormatter: (row: any) => `${Utils.formatDateString(row.MAX_MONTH_DATE)}`
          },
          {
            dataValue: 'COMPONENT_ACRONYM',
            headerLabel: 'Component',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${row.COMPONENT_ACRONYM}`
          },
          {
            dataValue: 'TASK_ORDER_ID',
            cellFormatter: (row: any) => `${row.TASK_ORDER_ID}`
          },
          {
            dataValue: 'TASK_ORDER_NO',
            headerLabel: 'Task Order Number',
            cellFormatter: (row: any) => `${row.TASK_ORDER_NO}`
          },
          {
            dataValue: 'TASK_ORDER_NAME',
            cellFormatter: (row: any) => `${row.TASK_ORDER_NAME}`
          },
          {
            dataValue: 'OBLIGATED',
            headerLabel: '',
            cellFormatter: (row: any) => `${Utils.formatUSD(row.OBLIGATED)}`
          },
          {
            dataValue: 'CEILING',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatUSD(row.CEILING)}`
          },
          { 
            dataValue: 'SPENT',
            headerLabel: 'Expenditures',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatUSD(row.SPENT)}`
          },
          {
            dataValue: 'BURN_TO_DATE',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatPercent(row.SPENT / row.CEILING * 100)}`
          },
          {
            dataValue: 'REMAINING_CEILING',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatUSD(row.CEILING - row.SPENT)}`
          },
          // {
          //   dataValue: 'ACTIONS',
          //   alignContent: 'center',
          //   noFilter: true,
          //   actionsFormatter: (row: any) => [
          //     {
          //       actionType: 'modal',
          //       buttonIconType: 'outlined',
          //       buttonLabel: 'View Details',
          //       hideLabel: true,
          //       buttonIcon: 'visibility',
          //       buttonColor: 'accent',
          //     }
          //   ]
          // }
        ],
      }
    },
  ]
};
