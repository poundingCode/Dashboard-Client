import { DashboardConfig } from '../../../../model/component/DynamicDashboard';
import { TaskOrder, STATUS_FILTER_OPTIONS } from '../../../../model/TaskOrder';
import { protectedResources } from '../../../../auth-config';
import { Utils } from '../../../../utils/utils';

export const TASK_ORDER_LIST_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Task Order List',
  elements: [
    {
      elementType: 'DataTable',
      elementInputs: {
        tableTitle: 'Task Order Details',
        tableDataType: TaskOrder,
        tableResourceUrl: protectedResources.azure.purchasing.TaskOrderList.endpoint+'/1',
        customDisplayedColumns: ['TASK_ORDER_NAME', 'TASK_ORDER_NO', 'START_DATE', 'END_DATE', 'STATUS'],
        columnFormatter: [
          {
            dataValue: 'TASK_ORDER_NAME',
            cellFormatter: (row: any) => `${row.TASK_ORDER_NAME}`,
          },
          {
            dataValue: 'TASK_ORDER_NO',
            headerLabel: 'Task Order Number',
            cellFormatter: (row: any) => `${row.TASK_ORDER_NO}`,
          },
          {
            dataValue: 'START_DATE',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatDateString(row.START_DATE)}`,
          },
          {
            dataValue: 'END_DATE',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatDateString(row.END_DATE)}`,
          },
          {
            dataValue: 'STATUS',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${Utils.capIt(row.STATUS, true)}`,
          },
        ],
      }
    }
  ]
};