import { DashboardConfig } from '../../../../model/component/DynamicDashboard';

export const SYSTEMS_OWNERS_ISSOS_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Systems & Owners & ISSOs',
  elements: [
    {
      elementType: 'ExternalResourceLayout',
      elementInputs: {
        resourceTitle: 'SharePoint Documentation',
        logoImage: '../../../assets/images/sharepoint_logo.png',
        resourceSummary: 'The DCCO Program Management Office leverages the <i><b style="font-weight: 800">DHS HQ Inventory Reference List</b></i>, an authoritative source of information regarding Systems, their Owners and the ISSOs associated with those systems. This site provides information such as:',
        bulletedList: ['System Name/Title & Acronym', 'System Number & Classification', 'System Type (Cloud/Hybrid)', 'System Model (IaaS, PaaS, etc.)', 'ISSM', 'ISSO', 'Additional informational references'],
        envMessage: 'This informational repository has role-based access applied. Please click below to continue.',
        resourceUrl: 'https://mgmt-sp.dhs.gov/hqcomp/Lists/HQ%20Inventory/AllItems.aspx',
      }
    },
  ]
};