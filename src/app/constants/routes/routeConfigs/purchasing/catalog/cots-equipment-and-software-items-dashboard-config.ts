import { DashboardConfig } from '../../../../../model/component/DynamicDashboard';
import { protectedResources } from '../../../../../auth-config';
import { CotsPurchasing, CotsSummary } from '../../../../../model/purchasing/COTS';
import { Utils } from '../../../../../utils/utils';
import { TileHelper } from '../../../../../utils/tile-utils';

export const COTS_EQUIPMENT_AND_SOFTWARE_ITEMS_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Catalog COTS Purchases',
  elements: [
    {
      elementType: 'PieChart',
      elementWidth: 'full',
      elementInputs: {
        pieChartResourceUrl: protectedResources.azure.purchasing.CotsSummary.endpoint,
        pieChartOptions: {
          title: 'Catalog COTS Purchases by Task Order',
          dataNameProperty: 'TASK_ORDER_NAME',
          dataValueProperty: 'AMOUNT',
          tooltipType: 'simpleUSD',
          isDoughnut: true,
          extraDataFormatter: [
            {
              dataValue: 'TASK_ORDER_NO',
              displayLabel: 'Task Order Number',
              valueFormatter: (data: any) => `${data.TASK_ORDER_NO}`
            },
            {
              dataValue: 'AMOUNT',
              displayLabel: 'Expenditures',
              valueFormatter: (data: any) => `${Utils.formatUSD(data.AMOUNT)}`
            },
          ]
        },
      }
    },
    {
      elementType: 'TileRow',
      elementInputs: {
        tileRowConfig: [
          {
            cardType: 'base-blue',
            cardTitle: 'Total Purchases',
            currentValue: {
              tableColumnName: 'AMOUNT',
              periodType: 'current',
              period: 'year',
              periodTableColumn: 'MONTH_DATE',
            },
            prevValue: {
              tableColumnName: 'AMOUNT',
              periodType: 'previous',
              period: 'year',
              periodTableColumn: 'MONTH_DATE',
            },
            tileWidth: 'tile-half',
            numType: '$',
          },
          {
            cardType: 'dark-blue',
            cardTitle: 'Projected One Year Estimate',
            currentValue: {
              tableColumnName: 'AMOUNT',
              periodType: 'next',
              period: 'year',
              tableColumnMath: 'projected',
            },
            tileWidth: 'tile-half',
            numType: '$',
          },
        ]
      }
    },
    {
      elementType: 'BarChart',
      elementInputs: {
        barChartResourceUrl: protectedResources.azure.purchasing.TotalPurchasingByMonthCots.endpoint,
        barChartOptions: {
          title: 'Catalog COTS Purchases by Month',
          chartType: 'vertMulti',
          xAxisLabel: 'Month',
          yAxisLabel: 'Catalog COTS Purchases',
          yAxisTickFormatting: (data: any) => `$${TileHelper.abbreviateNumber(data)}`,
          legend: true,
          tooltipType: 'simpleUSD',
        },
      }
    },
    {
      elementType: 'DataTable',
      elementInputs: {
        tableTitle: 'Catalog COTS Purchases Detail',
        tableDataType: CotsPurchasing,
        tableResourceUrl: protectedResources.azure.purchasing.CotsPurchaseDetails.endpoint+'/all',
        customDisplayedColumns: [
          // 'INVOICE_DATE',
          // 'INVOICE_ID',
          // 'YEAR',
          // 'QUARTER',
          // 'MONTH_DATE',
          'DISPLAY_DATE',
          'CONTRACT_PERIOD',
          'COMPONENT',
          'TASK_ORDER_NAME',
          'TASK_ORDER_NO',
          // 'SERVICE_CATEGORY',
          'CLIN_NAME',
          'CLIN',
          // 'ITEM_NAME',
          // 'ITEM_DESCRIPTION',
          // 'ITEM_QUANTITY',
          // 'ITEM_COST',
          'AMOUNT',
        ],
        columnFormatter: [
          {
            dataValue: 'INVOICE_DATE',
            cellFormatter: (row: any) => `${row.INVOICE_DATE}`
          },
          {
            dataValue: 'INVOICE_ID',
            headerLabel: 'Invoice ID',
            cellFormatter: (row: any) => `${row.INVOICE_ID}`
          },
          {
            dataValue: 'YEAR',
            headerLabel: 'Fiscal Year',
            cellFormatter: (row: any) => `${row.YEAR}`
          },
          {
            dataValue: 'QUARTER',
            headerLabel: 'Fiscal Quarter',
            cellFormatter: (row: any) => `${row.QUARTER}`
          },
          {
            dataValue: 'MONTH_DATE',
            headerLabel: 'Fiscal Date',
            cellFormatter: (row: any) => `${Utils.formatDateString(row.MONTH_DATE)}`
          },
          {
            dataValue: 'DISPLAY_DATE',
            headerLabel: 'Fiscal Date',
            cellFormatter: (row: any) => `${Utils.formatMonthYearString(row.MONTH_DATE)}`,
          },
          {
            dataValue: 'CONTRACT_PERIOD',
            cellFormatter: (row: any) => `${row.CONTRACT_PERIOD}`
          },
          {
            dataValue: 'TASK_ORDER_NAME',
            headerLabel: '',
            cellFormatter: (row: any) => `${row.TASK_ORDER_NAME}`
          },
          {
            dataValue: 'TASK_ORDER_NO',
            headerLabel: 'Task Order Number',
            cellFormatter: (row: any) => `${row.TASK_ORDER_NO}`
          },
          {
            dataValue: 'SERVICE_CATEGORY',
            headerLabel: '',
            cellFormatter: (row: any) => `${row.SERVICE_CATEGORY}`
          },
          {
            dataValue: 'CLIN',
            headerLabel: 'CLIN',
            cellFormatter: (row: any) => `${row.CLIN}`
          },
          {
            dataValue: 'CLIN_NAME',
            headerLabel: 'CLIN Name',
            cellFormatter: (row: any) => `${row.CLIN_NAME}`
          },
          {
            dataValue: 'COMPONENT',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${row.COMPONENT}`
          },
          {
            dataValue: 'ITEM_NAME',
            headerLabel: '',
            cellFormatter: (row: any) => `${row.ITEM_NAME}`
          },
          {
            dataValue: 'ITEM_DESCRIPTION',
            headerLabel: '',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${row.ITEM_DESCRIPTION}`
          },
          {
            dataValue: 'ITEM_QUANTITY',
            alignContent: 'right',
            cellFormatter: (row: any) => `${row.ITEM_QUANTITY}`
          },
          {
            dataValue: 'ITEM_COST',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatUSD(row.ITEM_COST)}`
          },
          {
            dataValue: 'AMOUNT',
            headerLabel: 'Total',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatUSD(row.AMOUNT)}`
          },
        ],
      }
    }
  ]
};
