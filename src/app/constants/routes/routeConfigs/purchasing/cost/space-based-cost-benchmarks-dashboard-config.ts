import { DashboardConfig } from '../../../../../model/component/DynamicDashboard';
import { protectedResources } from '../../../../../auth-config';
import { SpaceBasedCosts } from '../../../../../model/purchasing/Space';
import { Utils } from '../../../../../utils/utils';

export const SPACE_BASED_COST_BENCHMARKS_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Space-Based Cost Benchmarks',
  elements: [
    {
      elementType: 'TileRow',
      elementInputs: {
        tileRowConfig: [
          {
            cardType: 'base-blue',
            cardTitle: 'Total Purchases',
            currentValue: {
              tableColumnName: 'AMOUNT',
              periodType: 'current',
              period: 'year',
              periodTableColumn: 'MONTH_DATE',
            },
            prevValue: {
              tableColumnName: 'AMOUNT',
              periodType: 'previous',
              period: 'year',
              periodTableColumn: 'MONTH_DATE',
            },
            tileWidth: 'tile-third',
            numType: '$',
          },
          {
            cardType: 'base-blue',
            cardTitle: 'Total Square Footage',
            currentValue: {
              tableColumnName: 'SQUARE_FOOTAGE',
              periodType: 'current',
              period: 'year',
              periodTableColumn: 'MONTH_DATE',
            },
            tileWidth: 'tile-third',
          },
          {
            cardType: 'tert-blue',
            cardTitle: 'Std. Deviation Per Sq. Ft.',
            currentValue: {
              tableColumnName: 'PRICE_PER_SQ_FT',
              periodType: 'current',
              period: 'year',
              tableColumnMath: 'stddev',
              periodTableColumn: 'MONTH_DATE',
            },
            tileWidth: 'tile-third',
            numType: '$',
          },
        ]
      }
    },
    {
      elementType: 'TileRow',
      elementInputs: {
        tileRowConfig: [
          {
            cardType: 'sec-blue',
            cardTitle: 'Highest Cost Per Sq. Ft.',
            currentValue: {
              tableColumnName: 'PRICE_PER_SQ_FT',
              periodType: 'current',
              period: 'year',
              tableColumnMath: 'highVal',
              periodTableColumn: 'MONTH_DATE',
            },
            tileWidth: 'tile-third',
            numType: '$',
          },
          {
            cardType: 'sec-blue',
            cardTitle: 'Average Cost Per Sq. Ft.',
            currentValue: {
              tableColumnName: 'PRICE_PER_SQ_FT',
              periodType: 'current',
              period: 'year',
              tableColumnMath: 'avg',
              periodTableColumn: 'MONTH_DATE',
            },
            tileWidth: 'tile-third',
            numType: '$',
          },
          {
            cardType: 'sec-blue',
            cardTitle: 'Lowest Cost Per Sq. Ft.',
            currentValue: {
              tableColumnName: 'PRICE_PER_SQ_FT',
              periodType: 'current',
              period: 'year',
              tableColumnMath: 'lowVal',
              periodTableColumn: 'MONTH_DATE',
            },
            tileWidth: 'tile-third',
            numType: '$',
          },
        ]
      }
    },
    {
      elementType: 'DataTable',
      elementInputs: {
        tableTitle: 'Total by Location',
        tableDataType: SpaceBasedCosts,
        tableResourceUrl: protectedResources.azure.purchasing.SpaceBasedCosts.endpoint,
        customDisplayedColumns: [
          // 'YEAR',
          // 'QUARTER',
          // 'MONTH_DATE',
          'DISPLAY_DATE',
          'COMPONENT_ACRONYM',
          'TASK_ORDER_NAME',
          'CATEGORY_NAME',
          'IDIQ_CLIN_NAME',
          'LOCATION_NAME',
          'SQUARE_FOOTAGE',
          'AMOUNT',
          // 'PRICE_PER_SQ_FT',
        ],
        columnFormatter: [
          {
            dataValue: 'YEAR',
            headerLabel: 'Fiscal Year',
            cellFormatter: (row: any) => `${row.YEAR}`,
          },
          {
            dataValue: 'QUARTER',
            headerLabel: 'Fiscal Quarter',
            cellFormatter: (row: any) => `${row.QUARTER}`,
          },
          {
            dataValue: 'MONTH_DATE',
            headerLabel: 'Month',
            cellFormatter: (row: any) => `${Utils.formatDateString(row.MONTH_DATE)}`,
          },
          {
            dataValue: 'DISPLAY_DATE',
            headerLabel: 'Fiscal Date',
            cellFormatter: (row: any) => `${Utils.formatMonthYearString(row.MONTH_DATE)}`,
          },
          {
            dataValue: 'TASK_ORDER_NAME',
            headerLabel: '',
            cellFormatter: (row: any) => `${row.TASK_ORDER_NAME}`,
          },
          {
            dataValue: 'IDIQ_CLIN_NAME',
            headerLabel: 'CLIN',
            cellFormatter: (row: any) => `${row.IDIQ_CLIN_NAME}`,
          },
          {
            dataValue: 'CATEGORY_NAME',
            headerLabel: 'Service Category',
            cellFormatter: (row: any) => `${row.CATEGORY_NAME}`,
          },
          {
            dataValue: 'LOCATION_NAME',
            headerLabel: 'Location',
            cellFormatter: (row: any) => `${row.LOCATION_NAME}`,
          },
          {
            dataValue: 'COMPONENT_ACRONYM',
            headerLabel: 'Component',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${row.COMPONENT_ACRONYM}`,
          },
          {
            dataValue: 'SQUARE_FOOTAGE',
            headerLabel: '',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatLargeNumber(row.SQUARE_FOOTAGE)}`,
          },
          {
            dataValue: 'AMOUNT',
            headerLabel: '',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatUSD(row.AMOUNT)}`,
          },
          {
            dataValue: 'PRICE_PER_SQ_FT',
            headerLabel: '',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatUSD(row.PRICE_PER_SQ_FT)}`,
          },
        ],
      }
    }
  ]
};
