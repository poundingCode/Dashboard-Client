import { DashboardConfig } from '../../../../../model/component/DynamicDashboard';
import { protectedResources } from '../../../../../auth-config';
import { ContractLevelSummaryCosts } from '../../../../../model/purchasing/ContractLevelSummary';
import { Utils } from '../../../../../utils/utils';
import { TileHelper } from '../../../../../utils/tile-utils';

export const CONTRACT_LEVEL_SUMMARY_COSTS_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Contract-Level Summary Costs',
  elements: [
    {
      elementType: 'PieChart',
      elementWidth: 'half',
      elementInputs: {
        pieChartResourceUrl: protectedResources.azure.purchasing.TotalPurchasingByTaskOrder.endpoint,
        pieChartOptions: {
          title: 'Total Purchasing on Contract by Task Order',
          dataNameProperty: 'TASK_ORDER_NAME',
          dataValueProperty: 'SPEND_PCT',
          tooltipType: 'simplePercent',
          isDoughnut: true,
          extraDataFormatter: [
            {
              dataValue: 'TASK_ORDER_FUNDING',
              valueFormatter: (data: any) => `${Utils.formatUSD(data.TASK_ORDER_FUNDING)}`
            },
            {
              dataValue: 'AMOUNT',
              displayLabel: 'Expenditures',
              valueFormatter: (data: any) => `${Utils.formatUSD(data.AMOUNT)}`
            },
            {
              dataValue: 'SPEND_PCT',
              displayLabel: '% of Total Spent',
              valueFormatter: (data: any) => `${Utils.formatPercent(data.SPEND_PCT)}`
            }
          ]
        },
      }
    },
    {
      elementType: 'PieChart',
      elementWidth: 'half',
      elementInputs: {
        pieChartResourceUrl: protectedResources.azure.purchasing.ContractPurchasingByServiceCategory.endpoint,
        pieChartOptions: {
          title: 'Total Purchasing on Contract by Service Category',
          dataNameProperty: 'CATEGORY_NAME',
          dataValueProperty: 'AMOUNT',
          tooltipType: 'simpleUSD',
          isDoughnut: true,
          extraDataFormatter: [
            {
              dataValue: 'AMOUNT',
              displayLabel: 'Expenditures',
              valueFormatter: (data: any) => `${Utils.formatUSD(data.AMOUNT)}`
            },
            {
              dataValue: 'SPEND_PCT',
              displayLabel: '% of Total Spent',
              valueFormatter: (data: any) => `${Utils.formatPercent(data.SPEND_PCT)}`
            }
          ]
        },
      }
    },
    {
      elementType: 'TileRow',
      elementInputs: {
        tileRowConfig: [
          {
            cardType: 'base-blue',
            cardTitle: 'Total Purchases',
            currentValue: {
              tableColumnName: 'AMOUNT',
              periodType: 'current',
              period: 'year',
              periodTableColumn: 'MONTH_DATE',
            },
            prevValue: {
              tableColumnName: 'AMOUNT',
              periodType: 'previous',
              period: 'year',
              periodTableColumn: 'MONTH_DATE',
            },
            tileWidth: 'tile-half',
            numType: '$',
          },
          {
            cardType: 'dark-blue',
            cardTitle: 'Projected One Year Estimate',
            currentValue: {
              tableColumnName: 'AMOUNT',
              periodType: 'next',
              period: 'year',
              tableColumnMath: 'projected',
            },
            tileWidth: 'tile-half',
            numType: '$',
          },
        ]
      }
    },
    {
      elementType: 'BarChart',
      elementInputs: {
        barChartResourceUrl: protectedResources.azure.purchasing.TotalPurchasingByMonth.endpoint,
        barChartOptions: {
          title: 'Total Purchasing on Contract by Month',
          chartType: 'vertMulti',
          xAxisLabel: 'Month',
          yAxisLabel: 'Total Purchases',
          yAxisTickFormatting: (data: any) => `$${TileHelper.abbreviateNumber(data)}`,
          legend: true,
          tooltipType: 'simpleUSD',
        },
      }
    },
    {
      elementType: 'DataTable',
      elementInputs: {
        tableTitle: 'Total Purchasing on Contract by Task Order Details',
        tableDataType: ContractLevelSummaryCosts,
        exportAllUrl: protectedResources.azure.purchasing.ContractLevelSummaryCSVExport.endpoint,
        tableResourceUrl: protectedResources.azure.purchasing.ContractLevelSummaryCosts.endpoint,
        customDisplayedColumns: [
          // 'MONTH_DATE',
          'DISPLAY_DATE',
          'COMPONENT_ACRONYM',
          'TASK_ORDER_NAME',
          'TASK_ORDER_NO',
          'CONTRACT_YEAR',
          'TASK_ORDER_FUNDING',
          'AMOUNT',
          'PERCENT_BY_TASK_ORDER',
        ],
        columnFormatter: [
          {
            dataValue: 'YEAR',
            headerLabel: 'Fiscal Year',
            cellFormatter: (row: any) => `${row.YEAR}`,
          },
          {
            dataValue: 'QUARTER',
            headerLabel: 'Fiscal Quarter',
            cellFormatter: (row: any) => `${row.QUARTER}`,
          },
          {
            dataValue: 'MONTH_DATE',
            headerLabel: 'Fiscal Date',
            cellFormatter: (row: any) => `${Utils.formatDateString(row.MONTH_DATE)}`,
          },
          {
            dataValue: 'DISPLAY_DATE',
            headerLabel: 'Fiscal Date',
            cellFormatter: (row: any) => `${Utils.formatMonthYearString(row.MONTH_DATE)}`,
          },
          {
            dataValue: 'COMPONENT_ACRONYM',
            headerLabel: 'Component',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${row.COMPONENT_ACRONYM}`,
          },
          {
            dataValue: 'TASK_ORDER_NO',
            headerLabel: 'Task Order Number',
            cellFormatter: (row: any) => `${row.TASK_ORDER_NO}`,
          },
          {
            dataValue: 'TASK_ORDER_NAME',
            cellFormatter: (row: any) => `${row.TASK_ORDER_NAME}`,
          },
          {
            dataValue: 'CONTRACT_YEAR',
            cellFormatter: (row: any) => `${row.CONTRACT_YEAR}`,
          },
          {
            dataValue: 'CATEGORY_NAME',
            headerLabel: 'Service Category',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${row.CATEGORY_NAME}`,
          },
          {
            dataValue: 'TASK_ORDER_FUNDING',
            headerLabel: 'Total Contract Value',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatUSD(row.TASK_ORDER_FUNDING)}`,
          },
          {
            dataValue: 'AMOUNT',
            headerLabel: 'Expenditures',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatUSD(row.AMOUNT)}`,
          },
          {
            dataValue: 'PERCENT_BY_TASK_ORDER',
            headerLabel: '% of Total Spent',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatPercent(row.PERCENT_BY_TASK_ORDER)}`,
          }
        ],
      }
    },
  ]
};