import { DashboardConfig } from '../../../../../model/component/DynamicDashboard';
import { protectedResources } from '../../../../../auth-config';
import { LaborHourCosts, LaborHourCostsDetail } from '../../../../../model/purchasing/Labor';
import { TileHelper } from '../../../../../utils/tile-utils';
import { Utils } from '../../../../../utils/utils';

export const LABOR_HOUR_COSTS_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Labor Hour Costs',
  elements: [
    {
      elementType: 'DataTable',
      elementInputs: {
        tableTitle: 'Total Labor Hour Costs',
        tableDataType: LaborHourCosts,
        noService: true,
        tableResourceUrl: protectedResources.azure.purchasing.LaborHoursOverall.endpoint,
        customDisplayedColumns: ['CATEGORY_NAME', 'LABOR_CATEGORY_DESCRIPTION', 'LABOR_CATEGORY_CODE', 'LABOR_HOURS', 'LABOR_RATE', 'AMOUNT'],
        columnFormatter: [
          {
            dataValue: 'LABOR_CATEGORY_ID',
            cellFormatter: (row: any) => `${row.LABOR_CATEGORY_ID}`,
          },
          {
            dataValue: 'LABOR_CATEGORY_CODE',
            cellFormatter: (row: any) => `${row.LABOR_CATEGORY_CODE}`,
          },
          {
            dataValue: 'LABOR_CATEGORY_DESCRIPTION',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${row.LABOR_CATEGORY_DESCRIPTION}`,
          },
          {
            dataValue: 'CATEGORY_NAME',
            headerLabel: 'Service Category',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${row.CATEGORY_NAME}`,
          },
          {
            dataValue: 'LABOR_HOURS',
            alignContent: 'right',
            noFilter: true,
            cellFormatter: (row: any) => `${row.LABOR_HOURS}`,
          },
          {
            dataValue: 'LABOR_RATE',
            alignContent: 'right',
            noFilter: true,
            cellFormatter: (row: any) => `${Utils.formatUSD(row.LABOR_RATE)}`,
          },
          {
            dataValue: 'AMOUNT',
            alignContent: 'right',
            noFilter: true,
            cellFormatter: (row: any) => `${Utils.formatUSD(row.AMOUNT)}`,
          },
        ],
      }
    },
    {
      elementType: 'TileRow',
      elementInputs: {
        tileRowConfig: [
          {
            cardType: 'base-blue',
            cardTitle: 'Total Purchases',
            currentValue: {
              tableColumnName: 'AMOUNT',
              periodType: 'current',
              period: 'year',
              periodTableColumn: 'MONTH_DATE',
            },
            prevValue: {
              tableColumnName: 'AMOUNT',
              periodType: 'previous',
              period: 'year',
              periodTableColumn: 'MONTH_DATE',
            },
            tileWidth: 'tile-third',
            numType: '$',
          },
          {
            cardType: 'sec-blue',
            cardTitle: 'Total Labor Hours Purchased',
            currentValue: {
              tableColumnName: 'LABOR_HOURS',
              periodType: 'current',
              period: 'year',
              periodTableColumn: 'MONTH_DATE',
            },
            prevValue: {
              tableColumnName: 'LABOR_HOURS',
              periodType: 'previous',
              period: 'year',
              periodTableColumn: 'MONTH_DATE',
            },
            tileWidth: 'tile-third',
          },
          {
            cardType: 'tert-blue',
            cardTitle: 'Average Cost Per Labor Hour Purchased',
            currentValue: {
              tableColumnName: 'LABOR_RATE',
              tableColumnMath: 'avg',
              periodType: 'current',
              period: 'year',
              periodTableColumn: 'MONTH_DATE',
            },
            tileWidth: 'tile-third',
            numType: '$',
          },
        ]
      }
    },
    {
      elementType: 'BarChart',
      elementInputs: {
        barChartResourceUrl: protectedResources.azure.purchasing.LaborHoursByMonth.endpoint,
        barChartOptions: {
          title: 'Labor Hour Costs by Month',
          chartType: 'vertMulti',
          xAxisLabel: 'Month',
          yAxisLabel: 'Labor Hour Costs',
          yAxisTickFormatting: (data: any) => `$${TileHelper.abbreviateNumber(data)}`,
          legend: true,
          tooltipType: 'simpleUSD',
        },
      }
    },
    {
      elementType: 'DataTable',
      elementInputs: {
        tableTitle: 'Total Labor Hour Costs Detail',
        tableDataType: LaborHourCostsDetail,
        exportAllUrl: protectedResources.azure.purchasing.LaborHoursCSVExport.endpoint,
        tableResourceUrl: protectedResources.azure.purchasing.LaborHoursEverything.endpoint,
        customDisplayedColumns: ['DISPLAY_DATE', 'CATEGORY_NAME', 'LABOR_CATEGORY_DESCRIPTION', 'LABOR_CATEGORY_CODE', 'LABOR_HOURS', 'LABOR_RATE', 'AMOUNT'],
        columnFormatter: [
          {
            dataValue: 'YEAR',
            headerLabel: 'Fiscal Year',
            cellFormatter: (row: any) => `${row.YEAR}`,
          },
          {
            dataValue: 'QUARTER',
            headerLabel: 'Fiscal Quarter',
            cellFormatter: (row: any) => `${row.QUARTER}`,
          },
          {
            dataValue: 'MONTH_DATE',
            headerLabel: 'Fiscal Date',
            cellFormatter: (row: any) => `${Utils.formatDateString(row.MONTH_DATE)}`,
          },
          {
            dataValue: 'DISPLAY_DATE',
            headerLabel: 'Fiscal Date',
            cellFormatter: (row: any) => `${Utils.formatMonthYearString(row.MONTH_DATE)}`,
          },
          {
            dataValue: 'LABOR_CATEGORY_ID',
            cellFormatter: (row: any) => `${row.LABOR_CATEGORY_ID}`,
          },
          {
            dataValue: 'LABOR_CATEGORY_CODE',
            cellFormatter: (row: any) => `${row.LABOR_CATEGORY_CODE}`,
          },
          {
            dataValue: 'LABOR_CATEGORY_DESCRIPTION',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${row.LABOR_CATEGORY_DESCRIPTION}`,
          },
          {
            dataValue: 'CATEGORY_NAME',
            headerLabel: 'Service Category',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${row.CATEGORY_NAME}`,
          },
          {
            dataValue: 'LABOR_HOURS',
            alignContent: 'right',
            noFilter: true,
            cellFormatter: (row: any) => `${row.LABOR_HOURS}`,
          },
          {
            dataValue: 'LABOR_RATE',
            alignContent: 'right',
            noFilter: true,
            cellFormatter: (row: any) => `${Utils.formatUSD(row.LABOR_RATE)}`,
          },
          {
            dataValue: 'AMOUNT',
            alignContent: 'right',
            noFilter: true,
            cellFormatter: (row: any) => `${Utils.formatUSD(row.AMOUNT)}`,
          },
        ],
      }
    }
  ]
};
