import { DashboardConfig } from '../../../../../model/component/DynamicDashboard';
import { protectedResources } from '../../../../../auth-config';
import { IndividualResourceCosts } from '../../../../../model/purchasing/IndividualResource';
import { TileHelper } from '../../../../../utils/tile-utils';
import { Utils } from '../../../../../utils/utils';

export const INDIVIDUAL_RESOURCE_COSTS_CSP_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Individual Resource Costs CSP',
  elements: [
    {
      elementType: 'TileRow',
      elementInputs: {
        tileRowConfig: [
          {
            cardType: 'base-blue',
            cardTitle: 'Total Purchases',
            currentValue: {
              tableColumnName: 'AMOUNT',
              periodType: 'current',
              period: 'year',
              periodTableColumn: 'MONTH_DATE'
            },
            prevValue: {
              tableColumnName: 'AMOUNT',
              periodType: 'previous',
              period: 'year',
              periodTableColumn: 'MONTH_DATE'
            },
            tileWidth: 'tile-quarter',
            numType: '$',
          },
          {
            cardType: 'sec-blue',
            cardTitle: 'Cost Per TB of Cloud Storage Resource',
            currentValue: {
              tableColumnName: 'COST_PER_TB',
              tableFilterColumn: 'RESOURCE_TYPE_NAME',
              filterValue: 'Cloud Storage',
              periodType: 'current',
              period: 'year',
              periodTableColumn: 'MONTH_DATE',
              tableColumnMath: 'filteredAvg',
            },
            tileWidth: 'tile-quarter',
            numType: '$',
          },
          {
            cardType: 'sec-blue',
            cardTitle: 'Cost Per Cloud Computing Resource',
            currentValue: {
              tableColumnName: 'COST_PER_RESOURCE',
              tableFilterColumn: 'RESOURCE_TYPE_NAME',
              filterValue: 'Cloud Compute',
              periodType: 'current',
              period: 'year',
              periodTableColumn: 'MONTH_DATE',
              tableColumnMath: 'filteredAvg',
            },
            tileWidth: 'tile-quarter',
            numType: '$',
          },
          {
            cardType: 'sec-blue',
            cardTitle: 'Cost Per Cloud Networking Resource',
            currentValue: {
              tableColumnName: 'COST_PER_RESOURCE',
              tableFilterColumn: 'RESOURCE_TYPE_NAME',
              filterValue: 'Cloud Networking',
              periodType: 'current',
              period: 'year',
              periodTableColumn: 'MONTH_DATE',
              tableColumnMath: 'filteredAvg',
            },
            tileWidth: 'tile-quarter',
            numType: '$',
          },
        ]
      }
    },
    {
      elementType: 'BarChart',
      elementInputs: {
        barChartResourceUrl: protectedResources.azure.purchasing.ResourceCostsCSPChart.endpoint,
        barChartOptions: {
          title: 'Resource Costs (CSP) Per Month by Resource Type',
          chartType: 'vertMulti',
          xAxisLabel: 'Month',
          yAxisLabel: 'Cost',
          xAxisTickFormatting: (data: any) => `${Utils.formatMonthYearString(data)}`,
          yAxisTickFormatting: (data: any) => `$${TileHelper.abbreviateNumber(data)}`,
          legend: true,
          tooltipType: 'simpleUSD',
        },
      }
    },
    {
      elementType: 'DataTable',
      elementInputs: {
        tableTitle: 'Total by Resource',
        tableDataType: IndividualResourceCosts,
        tableResourceUrl: protectedResources.azure.purchasing.ResourceCostsCSP.endpoint,
        customDisplayedColumns: [
          // 'YEAR',
          // 'QUARTER',
          // 'MONTH_DATE',
          'DISPLAY_DATE',
          'IDIQ_CLIN_NAME',
          'IDIQ_CLIN_NUMBER',
          'ENVIRONMENT_NAME',
          // 'STORAGE_IN_GB',
          'RESOURCE_TYPE_NAME',
          // 'COST_PER_TB',
          // 'COST_PER_RESOURCE,
          'AMOUNT',
          // 'COUNT',
        ],
        columnFormatter: [
          {
            dataValue: 'YEAR',
            headerLabel: 'Fiscal Year',
            cellFormatter: (row: any) => `${row.YEAR}`,
          },
          {
            dataValue: 'QUARTER',
            headerLabel: 'Fiscal Quarter',
            cellFormatter: (row: any) => `${row.QUARTER}`,
          },
          {
            dataValue: 'MONTH_DATE',
            headerLabel: 'Fiscal Date',
            cellFormatter: (row: any) => `${Utils.formatDateString(row.MONTH_DATE)}`,
          },
          {
            dataValue: 'DISPLAY_DATE',
            headerLabel: 'Fiscal Date',
            cellFormatter: (row: any) => `${Utils.formatMonthYearString(row.MONTH_DATE)}`,
          },
          {
            dataValue: 'IDIQ_CLIN_NAME',
            headerLabel: 'Service Category',
            cellFormatter: (row: any) => `${row.IDIQ_CLIN_NAME}`,
          },
          {
            dataValue: 'IDIQ_CLIN_NUMBER',
            headerLabel: 'CLIN',
            cellFormatter: (row: any) => `${row.IDIQ_CLIN_NUMBER}`,
          },
          {
            dataValue: 'ENVIRONMENT_NAME',
            headerLabel: 'Resource ID',
            cellFormatter: (row: any) => `${row.ENVIRONMENT_NAME}`,
          },
          {
            dataValue: 'RESOURCE_TYPE_NAME',
            headerLabel: 'Type',
            cellFormatter: (row: any) => `${row.RESOURCE_TYPE_NAME}`,
          },
          {
            dataValue: 'STORAGE_IN_GB',
            headerLabel: 'Storage (GB)',
            cellFormatter: (row: any) => `${row.STORAGE_IN_GB}`,
          },
          {
            dataValue: 'COST_PER_TB',
            cellFormatter: (row: any) => `${(row.STORAGE_IN_GB ? (row.AMOUNT / (row.STORAGE_IN_GB / 1000)) : 0)}`,
          },
          {
            dataValue: 'COST_PER_RESOURCE',
            cellFormatter: (row: any) => `${(row.COUNT ? (row.AMOUNT / (row.COUNT)) : 0)}`,
          },
          {
            dataValue: 'COUNT',
            cellFormatter: (row: any) => `${row.COUNT}`,
          },
          {
            dataValue: 'AMOUNT',
            headerLabel: 'Cost',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatUSD(row.AMOUNT)}`,
          },
        ],
      }
    }
  ]
};
