import { DashboardConfig } from '../../../../../model/component/DynamicDashboard';
import { protectedResources } from '../../../../../auth-config';
import { TaskOrderPurchasing } from '../../../../../model/purchasing/TaskOrderPurchasing';
import { TileHelper } from '../../../../../utils/tile-utils';
import { Utils } from '../../../../../utils/utils';

export const TASK_ORDER_PURCHASING_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Task Order Purchasing',
  elements: [
    {
      elementType: 'BarChart',
      elementInputs: {
        barChartResourceUrl: protectedResources.azure.purchasing.TotalPurchasingByTaskOrderAndServiceCategory.endpoint,
        barChartOptions: {
          title: 'Total Task Order Purchasing By Service Category',
          chartType: 'horizStacked',
          dataNameProperty: 'CATEGORY_NAME',
          dataSeriesProperties: [ 'TASK_ORDER_NAME', 'AMOUNT' ],
          xAxisTickFormatting: (data: any) => `$${TileHelper.abbreviateNumber(data)}`,
          tooltipType: 'simpleUSD',
          legend: true,
          extraDataFormatter: [
            {
              dataValue: 'CATEGORY_NAME',
              displayLabel: 'Service Category',
            },
            {
              dataValue: 'SPEND_PCT',
              displayLabel: '% Spent',
              valueFormatter: (data: any) => `${Utils.formatPercent(data.SPEND_PCT)}`
            },
            {
              dataValue: 'AMOUNT',
              displayLabel: 'Expenditures',
              valueFormatter: (data: any) => `${Utils.formatUSD(data.AMOUNT)}`
            }
          ]
        },
      }
    },
    {
      elementType: 'BarChart',
      elementInputs: {
        barChartResourceUrl: protectedResources.azure.purchasing.TaskOrderPurchasingByMonth.endpoint,
        barChartOptions: {
          title: 'Total Task Order Purchasing by Month',
          chartType: 'horizStacked',
          xAxisLabel: 'Month',
          yAxisLabel: 'Task Order Purchasing',
          yAxisTickFormatting: (data: any) => `${Utils.formatMonthYearString(data)}`,
          xAxisTickFormatting: (data: any) => `$${TileHelper.abbreviateNumber(data)}`,
          legend: true,
          tooltipType: 'simpleUSD',
        },
      }
    },
    {
      elementType: 'TileRow',
      elementInputs: {
        tileRowConfig: [
          {
            cardType: 'base-blue',
            cardTitle: 'Total Purchases',
            currentValue: {
              tableColumnName: 'AMOUNT',
              periodType: 'current',
              period: 'year',
              periodTableColumn: 'MONTH_DATE',
            },
            prevValue: {
              tableColumnName: 'AMOUNT',
              periodType: 'previous',
              period: 'year',
              periodTableColumn: 'MONTH_DATE',
            },
            tileWidth: 'tile-third',
            numType: '$',
          },
          {
            cardType: 'dark-blue',
            cardTitle: 'Projected One Year Estimate',
            currentValue: {
              tableColumnName: 'AMOUNT',
              periodType: 'next',
              period: 'year',
              tableColumnMath: 'projected',
            },
            tileWidth: 'tile-third',
            numType: '$',
          },
          {
            cardType: 'base-blue',
            cardTitle: 'Percentage of Total Expenditures',
            cardResourceUrl: protectedResources.azure.purchasing.ContractSpendingVsCeiling.endpoint,
            currentValue: {
              tableColumnName: 'AMOUNT',
              periodType: 'current',
              period: 'year',
              dataValue: 'SPENT',
              dataValueMath: 'divideDisplayValByDataVal',
            },
            tileWidth: 'tile-third',
            numType: '%',
          },
        ]
      }
    },
    {
      elementType: 'DataTable',
      elementInputs: {
        tableTitle: 'Total Task Order Purchasing By Service Category Details',
        tableDataType: TaskOrderPurchasing,
        exportAllUrl: protectedResources.azure.purchasing.TaskOrderPurchasingCSVExport.endpoint,
        tableResourceUrl: protectedResources.azure.purchasing.TaskOrderPurchasing.endpoint,
        customDisplayedColumns: [
          // 'MONTH_DATE',
          'DISPLAY_DATE',
          'COMPONENT_ACRONYM',
          'TASK_ORDER_NAME',
          'TASK_ORDER_NO',
          'CATEGORY_NAME',
          'AMOUNT',
          'SPEND_PCT',
        ],
        columnFormatter: [
          {
            dataValue: 'YEAR',
            headerLabel: 'Fiscal Year',
            cellFormatter: (row: any) => `${row.YEAR}`,
          },
          {
            dataValue: 'QUARTER',
            headerLabel: 'Fiscal Quarter',
            cellFormatter: (row: any) => `${row.QUARTER}`,
          },
          {
            dataValue: 'MONTH_DATE',
            headerLabel: 'Fiscal Date',
            cellFormatter: (row: any) => `${Utils.formatDateString(row.MONTH_DATE)}`,
          },
          {
            dataValue: 'DISPLAY_DATE',
            headerLabel: 'Fiscal Date',
            cellFormatter: (row: any) => `${Utils.formatMonthYearString(row.MONTH_DATE)}`,
          },
          {
            dataValue: 'COMPONENT_ACRONYM',
            headerLabel: 'Component',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${row.COMPONENT_ACRONYM}`,
          },
          {
            dataValue: 'TASK_ORDER_NAME',
            cellFormatter: (row: any) => `${row.TASK_ORDER_NAME}`,
          },
          {
            dataValue: 'TASK_ORDER_NO',
            headerLabel: 'Task Order Number',
            cellFormatter: (row: any) => `${row.TASK_ORDER_NO}`,
          },
          {
            dataValue: 'CATEGORY_NAME',
            headerLabel: 'Service Category',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${row.CATEGORY_NAME}`,
          },
          {
            dataValue: 'IDIQ_CLIN_NAME',
            headerLabel: 'CLIN Name',
            cellFormatter: (row: any) => `${row.IDIQ_CLIN_NAME}`,
          },
          {
            dataValue: 'SPEND_PCT',
            headerLabel: '% of Total Spent',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatPercent(row.SPEND_PCT)}`,
          },
          {
            dataValue: 'AMOUNT',
            headerLabel: 'Expenditures',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatUSD(row.AMOUNT)}`,
          },
        ],
      }
    }
  ]
};
