import { DashboardConfig } from '../../../../model/component/DynamicDashboard';

export const POINTS_OF_CONTACT_PAGE_CONFIG: DashboardConfig = {
  title: 'Points of Contact',
  elements: [
    {
      elementType: 'HTMLContainer',
      elementInputs: {
        htmlSource: (
          `<h2>Program Manager</h2>
          <div>John Chandler</div>
          <a mailto="john.w.chandler@mail.peraton.com">john.w.chandler@mail.peraton.com</a>
          <div></div>
          <br>
          <h2>Service Delivery Manager</h2>
          <div>Keith Blodgett</div>
          <a mailto="keith.blodgett@mail.peraton.com">keith.blodgett@mail.peraton.com</a>
          <div>(571) 261-0843</div>
          <br>
          <h2>Security Manager</h2>
          <div>Anil Katarki</div>
          <a mailto="anil.katarki@mail.peraton.com">anil.katarki@mail.peraton.com</a>
          <div>(703) 742-2326</div>
          <br>
          <h2>Customer Advocates</h2>
          <div>Vicki Nguyen</div>
          <a mailto="victoria.nguyen@mail.peraton.com">victoria.nguyen@mail.peraton.com</a>
          <div></div>
          <br>
          <div>Darryl Eadi</div>
          <a mailto="deadi@mail.peraton.com">deadi@mail.peraton.com</a>
          <div>(571) 331-5937</div>
          <br>
          <h2>COR</h2>
          <div>Kenan Dunson</div>
          <div>DC1 IDIQ / DCCO COR</div>
          <div>USM/OCIO/BMO</div>
          <div>Department of Homeland Security</div>
          <a mailto="Kenan.Dunson@hq.dhs.gov">Kenan.Dunson@hq.dhs.gov</a>
          <div>(202) 507-3024</div>
          <br>
          <h2>CO</h2>
          <div>Breean Jaroski</div>
          <div>Office of the Chief Information Officer</div>
          <div>Information Technology Operations</div>
          <a mailto="breean.jaroski@hq.dhs.gov">breean.jaroski@hq.dhs.gov</a>
          <div>(202) 447-0337</div>
          <br>
          <br>
          <br>
          `
        ),
      }
    },
  ]
};