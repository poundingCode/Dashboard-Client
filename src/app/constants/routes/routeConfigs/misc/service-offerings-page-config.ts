import { DashboardConfig } from '../../../../model/component/DynamicDashboard';

export const SERVICE_OFFERINGS_PAGE_CONFIG: DashboardConfig = {
  title: 'Service Offerings',
  elements: [
    {
      elementType: 'HTMLContainer',
      elementInputs: {
        htmlSource: (
          `<div>Our Service Offerings</div>`
        ),
      }
    },
  ]
};