import { DashboardConfig } from '../../../../model/component/DynamicDashboard';

export const ORDERING_INFO_PAGE_CONFIG: DashboardConfig = {
  title: 'Ordering Information',
  elements: [
    {
      elementType: 'HTMLContainer',
      elementInputs: {
        htmlSource: (
          `
          <div class="ordering-info-wrapper">
            <ul>
              <li>The DHS Component</li>
              <ul>
                <li>Identifies opportunity for services</li>
                <li>Contacts Perspecta DCCO Client Advocate (CA)</li>
                <li>Submits requirements to the Perspecta CA</li>
                <li>Submits requirements and the non-binding Rough Order of Magnitude (ROM), the BE or binding quote to the DHS Contracting Officer Representative (COR)</li>
              </ul>
              <li>The Perspecta DCCO Client Advocate</li>
              <ul>
                <li>Collaborates with DHS component to define scope of work and requirements with communication between the DHS DCCO CA and the Perspecta DCCO CA</li>
                <li>Provides requirements templates to the DHS component, if applicable, and to the DHS CA</li>
                <li>Submits the non-binding Rough Order of Magnitude (ROM), and BE, or binding quote to the Contracting Officer Representative (COR) with communication between them</li>
              </ul>
              <li>The DHS Task Order COR</li>
              <ul>
                <li>Reviews the submission and communicates and coordinates with the Perspecta CA on questions</li>
                <li>Prepares and submits the Task Order Requirement Package (TORP) to the DHS Task Order Contracting Officer (CO)</li>
              </ul>
              <li>The DHS Task Order Contracting Officer (CO)</li>
              <ul>
                <li>Reviews the TORP and issues the Task Order Solicitation</li>
                <li>Evaluates the proposal and works with Perspecta on questions or issues</li>
                <li>Review, and submits, the Task Order Solicitation to/with the Perspecta DCCO Contracts Representative (Perspecta COR)</li>
                <li>Submits to, and communicates with, the proposal and works with Perspecta COR on questions and updates</li>
                <li>Issues the Task Order Award</li>
                <ul>
                  <li>The Perspecta COR reviews and signs the Task Order Award</li>
                </ul>
                <li>Countersigns and issues the fully executed Task Order to the Perspecta COR for review</li>
              </ul>
              <li>The Perspecta DCCO Contracts representative (Perspecta COR)</li>
              <ul>
                <li>Reviews the Task Order Solicitation with the Perspecta CA</li>
                <li>Submits the proposal within the deadline recommended by the DHS Task Order CO</li>
                <li>Reviews and signs the Task Order Award</li>
                <ul>
                  <li>DHS Task Order CO Countersigns and issues the fully executed Task Order to the Perspecta</li>
                </ul>
                <li>The Perspecta COR receives the fully executed Task Order award</li>
              </ul>
            </ul>
          </div>
          `
        ),
      }
    },
  ]
};