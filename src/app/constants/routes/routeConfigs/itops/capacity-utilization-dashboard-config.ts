import { DashboardConfig } from '../../../../model/component/DynamicDashboard';
import { CapacityUtilization } from '../../../../model/CapUt';
import { protectedResources } from '../../../../auth-config';
import { ChartHelper } from '../../../../utils/chart-utils';
import { Utils } from '../../../../utils/utils';

export const CAPACITY_UTILIZATION_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Capacity and Utilization',
  elements: [
    {
      elementType: 'ChipList',
      elementInputs: {
        chipListConfig: [
          {
            label: 'Nominal (<70%)',
            color: 'severity-none',
            icon: 'check_circle',
          },
          {
            label: 'Approaching Threshold (>70%)',
            color: 'severity-med',
            icon: 'error',
          },
          {
            label: 'Exceeded Threshold (>80%)',
            color: 'severity-crit',
            icon: 'cancel',
          },
        ]
      }
    },
    {
      elementType: 'Gauge',
      elementWidth: 'third',
      elementInputs: {
        gaugeResourceUrl: protectedResources.azure.itops.CapUtilSummary.endpoint,
        gaugeOptions: {
          title: 'CPU Utilization - All Devices',
          singleNameProperty: 'CPU Utilization',
          axisTickFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          valueFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          tooltipType: 'severityGauge',
          linkUrl: 'https://atlas.dhs.gov',
        },
      }
    },
    {
      elementType: 'Gauge',
      elementWidth: 'third',
      elementInputs: {
        gaugeResourceUrl: protectedResources.azure.itops.CapUtilSummary.endpoint+'/Physical',
        gaugeOptions: {
          title: 'CPU Utilization - Physical',
          singleNameProperty: 'CPU Utilization',
          axisTickFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          valueFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          tooltipType: 'severityGauge',
          linkUrl: 'https://atlas.dhs.gov',
        },
      }
    },
    {
      elementType: 'Gauge',
      elementWidth: 'third',
      elementInputs: {
        gaugeResourceUrl: protectedResources.azure.itops.CapUtilSummary.endpoint+'/Virtual',
        gaugeOptions: {
          title: 'CPU Utilization - Virtual',
          singleNameProperty: 'CPU Utilization',
          axisTickFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          valueFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          tooltipType: 'severityGauge',
          linkUrl: 'https://atlas.dhs.gov',
        },
      }
    },
    {
      elementType: 'Gauge',
      elementWidth: 'third',
      elementInputs: {
        gaugeResourceUrl: protectedResources.azure.itops.CapUtilSummary.endpoint,
        gaugeOptions: {
          title: 'Memory Utilization - All Devices',
          singleNameProperty: 'Memory Utilization',
          axisTickFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          valueFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          tooltipType: 'severityGauge',
          linkUrl: 'https://atlas.dhs.gov',
        },
      }
    },
    {
      elementType: 'Gauge',
      elementWidth: 'third',
      elementInputs: {
        gaugeResourceUrl: protectedResources.azure.itops.CapUtilSummary.endpoint+'/Physical',
        gaugeOptions: {
          title: 'Memory Utilization - Physical',
          singleNameProperty: 'Memory Utilization',
          axisTickFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          valueFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          tooltipType: 'severityGauge',
          linkUrl: 'https://atlas.dhs.gov',
        },
      }
    },
    {
      elementType: 'Gauge',
      elementWidth: 'third',
      elementInputs: {
        gaugeResourceUrl: protectedResources.azure.itops.CapUtilSummary.endpoint+'/Virtual',
        gaugeOptions: {
          title: 'Memory Utilization - Virtual',
          singleNameProperty: 'Memory Utilization',
          axisTickFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          valueFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          tooltipType: 'severityGauge',
          linkUrl: 'https://atlas.dhs.gov',
        },
      }
    },
    {
      elementType: 'DataTable',
      elementInputs: {
        tableTitle: 'Network Metrics',
        tableDataType: CapacityUtilization,
        linkUrl: 'https://atlas.dhs.gov',
        tableResourceUrl: protectedResources.azure.itops.CapacityUtilization.endpoint,
        customDisplayedColumns: [
          'DEVICE_ID',
          'DEVICE_NAME',
          'COMPONENT',
          'TASK_ORDER',
          'LOCATION',
          'DEVICE_TYPE',
          'CPU_UTILIZATION',
          'CPU_THRESHOLD',
          'MEMORY_UTILIZATION',
          'MEMORY_THRESHOLD',
        ],
        columnFormatter: [
          {
            dataValue: 'DEVICE_ID',
            headerLabel: 'Device ID',
            cellFormatter: (row: any) => `${row.DEVICE_ID}`,
          },
          {
            dataValue: 'DEVICE_NAME',
            headerLabel: '',
            cellFormatter: (row: any) => `${row.DEVICE_NAME}`,
          },
          {
            dataValue: 'COMPONENT',
            headerLabel: '',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${row.COMPONENT}`,
          },
          {
            dataValue: 'CPU_UTILIZATION',
            headerLabel: 'CPU Utilization',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatPercent(row.CPU_UTILIZATION)}`,
          },
          {
            dataValue: 'CPU_THRESHOLD',
            headerLabel: 'CPU Threshold',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${ChartHelper.getSeverityThresholdText(row.CPU_UTILIZATION)}`,
          },
          {
            dataValue: 'MEMORY_UTILIZATION',
            headerLabel: '',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatPercent(row.MEMORY_UTILIZATION)}`,
          },
          {
            dataValue: 'MEMORY_THRESHOLD',
            headerLabel: 'Memory Threshold',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${ChartHelper.getSeverityThresholdText(row.MEMORY_UTILIZATION)}`,
          },
          {
            dataValue: 'DEVICE_TYPE',
            headerLabel: '',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${Utils.capIt(row.DEVICE_TYPE)}`,
          },
          {
            dataValue: 'TASK_ORDER',
            headerLabel: '',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${row.TASK_ORDER}`,
          },
          {
            dataValue: 'LOCATION',
            headerLabel: '',
            cellFormatter: (row: any) => `${row.LOCATION}`,
          }
        ]
      }
    },
  ]
};