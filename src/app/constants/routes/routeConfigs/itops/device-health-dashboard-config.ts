import { DashboardConfig } from '../../../../model/component/DynamicDashboard';
import { DeviceHealth, DeviceHealthByMonth } from '../../../../model/Device';
import { protectedResources } from '../../../../auth-config';
import { ChartHelper } from '../../../../utils/chart-utils';
import { Utils } from '../../../../utils/utils';

export const DEVICE_HEALTH_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Device Health',
  elements: [
    {
      elementType: 'ChipList',
      elementInputs: {
        chipListConfig: [
          {
            label: 'Nominal (<70%)',
            color: 'severity-none',
            icon: 'check_circle',
          },
          {
            label: 'Approaching Threshold (>70%)',
            color: 'severity-med',
            icon: 'error',
          },
          {
            label: 'Exceeded Threshold (>80%)',
            color: 'severity-crit',
            icon: 'cancel',
          },
        ]
      }
    },
    {
      elementType: 'Gauge',
      elementWidth: 'half',
      elementInputs: {
        gaugeResourceUrl: protectedResources.azure.itops.DeviceHealthThreshold.endpoint+'/CPU',
        gaugeOptions: {
          title: 'Server CPU Utilization',
          dataNameProperty: 'name',
          dataValueProperty: 'value',
          axisTickFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          valueFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          tooltipType: 'severityGauge',
          linkUrl: 'https://atlas.dhs.gov',
        },
      }
    },
    {
      elementType: 'Gauge',
      elementWidth: 'half',
      elementInputs: {
        gaugeResourceUrl: protectedResources.azure.itops.DeviceHealthThreshold.endpoint+'/Mem',
        gaugeOptions: {
          title: 'Server Memory Utilization',
          dataNameProperty: 'name',
          dataValueProperty: 'value',
          axisTickFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          valueFormatting: (data: any) => `${Utils.formatPercent(data)}`,
          tooltipType: 'severityGauge',
          linkUrl: 'https://atlas.dhs.gov',
        },
      }
    },
    {
      elementType: 'TileRow',
      elementInputs: {
        tileRowConfig: [
          {
            cardType: 'sec-blue',
            cardTitle: 'CPU Utilization',
            currentValue: {
              tableColumnName: 'CPU_UTILIZATION',
              periodType: 'current',
              period: 'month',
              periodTableColumn: 'DATE',
            },
            prevValue: {
              tableColumnName: 'CPU_UTILIZATION',
              periodType: 'previous',
              period: 'month',
              periodTableColumn: 'DATE',
            },
            tileWidth: 'tile-half',
            invertTrend: true,
            numType: '%',
            linkUrl: 'https://atlas.dhs.gov',
          },
          {
            cardType: 'sec-blue',
            cardTitle: 'Memory Utilization',
            currentValue: {
              tableColumnName: 'MEMORY_UTILIZATION',
              periodType: 'current',
              period: 'month',
              periodTableColumn: 'DATE',
            },
            prevValue: {
              tableColumnName: 'MEMORY_UTILIZATION',
              periodType: 'previous',
              period: 'month',
              periodTableColumn: 'DATE',
            },
            tileWidth: 'tile-half',
            invertTrend: true,
            numType: '%',
            linkUrl: 'https://atlas.dhs.gov',
          },
        ]
      }
    },
    {
      elementType: 'BarChart',
      elementInputs: {
        barChartResourceUrl: protectedResources.azure.itops.DeviceHealthChart.endpoint,
        barChartOptions: {
          title: 'Utilization by Month (Past 12 Months)',
          chartType: 'vertMulti',
          xAxisLabel: 'Month',
          yAxisLabel: 'Utilization (CPU & Memory)',
          xAxisTickFormatting: (data: any) => `${Utils.formatMonthYearString(data)}`,
          legend: true,
          tooltipType: 'simplePercent',
          linkUrl: 'https://atlas.dhs.gov',
        }
      }
    },
    {
      elementType: 'DataTable',
      elementInputs: {
        tableTitle: 'Device Health By Month',
        tableDataType: DeviceHealthByMonth,
        linkUrl: 'https://atlas.dhs.gov',
        tableResourceUrl: protectedResources.azure.itops.DeviceHealthByMonth.endpoint,
        customDisplayedColumns: [
          'DATE',
          'CPU_UTILIZATION',
          'CPU_THRESHOLD',
          'MEMORY_UTILIZATION',
          'MEMORY_THRESHOLD'
        ],
        columnFormatter: [
          {  
            dataValue: 'YEAR',
            headerLabel: 'YEAR',
            cellFormatter: (row: any) => `${row.YEAR}`,
          },
          {  
            dataValue: 'MONTH',
            headerLabel: 'MONTH',
            cellFormatter: (row: any) => `${row.MONTH}`,
          },
          {
            dataValue: 'DATE',
            headerLabel: 'Date',
            cellFormatter: (row: any) => `${Utils.formatDateString(new Date(row.YEAR, row.MONTH-1))}`,
          },
          {  
            dataValue: 'CPU_UTILIZATION',
            headerLabel: 'CPU Utilization',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatPercent(row.CPU_UTILIZATION)}`,
          },
          {
            dataValue: 'CPU_THRESHOLD',
            headerLabel: 'CPU Threshold',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${ChartHelper.getSeverityThresholdText(row.CPU_UTILIZATION)}`,
          },
          {  
            dataValue: 'MEMORY_UTILIZATION',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatPercent(row.MEMORY_UTILIZATION)}`,
          },
          {
            dataValue: 'MEMORY_THRESHOLD',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${ChartHelper.getSeverityThresholdText(row.MEMORY_UTILIZATION)}`,
          },
        ],
      }
    },
    {
      elementType: 'DataTable',
      elementInputs: {
        tableTitle: 'Device Health Details',
        tableDataType: DeviceHealth,
        linkUrl: 'https://atlas.dhs.gov',
        noService: true,
        tableResourceUrl: protectedResources.azure.itops.DeviceHealth.endpoint,
        customDisplayedColumns: [
          'DATE',
          'DEVICE_ID',
          'DEVICE_NAME',
          'COMPONENT',
          'TASK_ORDER',
          'LOCATION',
          'DEVICE_TYPE',
          'CPU_UTILIZATION',
          'CPU_THRESHOLD',
          'MEMORY_UTILIZATION',
          'MEMORY_THRESHOLD'
          // 'MONTH',
          // 'YEAR',
        ],
        columnFormatter: [
          {
            dataValue: 'YEAR',
            cellFormatter: (row: any) => `${row.YEAR}`
          },
          {
            dataValue: 'MONTH',
            cellFormatter: (row: any) => `${row.MONTH}`
          },
          {
            dataValue: 'DATE',
            cellFormatter: (row: any) => `${Utils.formatDateString(new Date(row.YEAR, row.MONTH-1))}`,
          },
          {
            dataValue: 'COMPONENT',
            cellFormatter: (row: any) => `${row.COMPONENT}`
          },
          {
            dataValue: 'DEVICE_ID',
            headerLabel: 'Device ID',
            cellFormatter: (row: any) => `${row.DEVICE_ID}`
          },
          {
            dataValue: 'DEVICE_NAME',
            cellFormatter: (row: any) => `${row.DEVICE_NAME}`
          },
          {
            dataValue: 'DEVICE_TYPE',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${Utils.capIt(row.DEVICE_TYPE)}`
          },
          {
            dataValue: 'TASK_ORDER',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${row.TASK_ORDER}`
          },
          {
            dataValue: 'CPU_UTILIZATION',
            headerLabel: 'CPU Utilization',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatPercent(row.CPU_UTILIZATION)}`
          },
          {
            dataValue: 'CPU_THRESHOLD',
            headerLabel: 'CPU Threshold',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${ChartHelper.getSeverityThresholdText(row.CPU_UTILIZATION)}`,
          },
          {
            dataValue: 'MEMORY_UTILIZATION',
            alignContent: 'right',
            cellFormatter: (row: any) => `${Utils.formatPercent(row.MEMORY_UTILIZATION)}`
          },
          {
            dataValue: 'MEMORY_THRESHOLD',
            headerLabel: 'Memory Threshold',
            useFilterDropdown: true,
            cellFormatter: (row: any) => `${ChartHelper.getSeverityThresholdText(row.MEMORY_UTILIZATION)}`,
          },
          {
            dataValue: 'LOCATION',
            cellFormatter: (row: any) => `${row.LOCATION}`
          },
        ]
      }
    },
  ]
};