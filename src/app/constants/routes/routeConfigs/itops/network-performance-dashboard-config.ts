import { DashboardConfig } from '../../../../model/component/DynamicDashboard';

export const NETWORK_PERFORMANCE_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Network Performance',
  elements: [
    {
      elementType: 'IFrame',
      elementInputs: {
        iFrameData: [
          // {
          //   frameTitle: 'test3',
          //   sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/app/dcco/test?form.site=*&form.circuit=*',
          // },
          // {
          //   frameTitle: 'test2',
          //   sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/account/insecurelogin?username=test&password=TE6ASe8eQKBfgD73&return_to=/app/dcco/test?form.site=*',
          // },
          // {
          //   frameTitle: 'test',
          //   sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/app/dcco/test?form.site=DC1',
          // },
          {
            frameTitle: 'Bandwidth Utilization',	
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fnobody%2Fdcco%2Fsaved%2Fsearches%2Fvisual_network_band2&oid=rUEYB3_5E9bSw%5E8rJV9ylacyRNYZGKDXuhxRPJSrCjUCNMv4L2M7HlCqanBxfO3aWQsNJIyJ1V79FkJvLjlGuPji1Ukmlf%5ErPvmGcKISwYnZNxgB3ACifycHJM8IPIE3KMaHeD1JM3rgSi2sGvPZm2WHQyC287WO%5Eo',
            height: '425px',
          },
          {
            frameTitle: 'Network Throughput',	
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fnobody%2Fdcco%2Fsaved%2Fsearches%2Fvisual_network_through2&oid=0BKFiXi2QbEgIDy5m7Qp2qbab1gFxfn55YpJklzUg4OQuZiDXiqACP3NX1dOZ%5Ez_%5EJwAFZl4nS0qtVIs9Ky3zGVTNH3RDVw4My9yxeWAWbheP1vcrDCq80_%5EJc_hQONX2_QMZvxK0nHmbzpUOLVTOAqV0O%5Ed',
            height: '425px',
          },
          {
            frameTitle: 'Network Latency',	
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fbtannehill_adm%2Fdcco%2Fsaved%2Fsearches%2Fvisual_network_latency2&oid=HhzYXEmcMqIhhMaLGhzKWfc2W4x%5E8Tva5_AUlzpxWKtSvYsD9%5EI0Aoo2wWdDY5jaY09K16VgLjLzDxm6pkY4n5Jaj3BFFz72vXEAEc4US0t%5EdGHt4KKdCioajP7s0FQgFrbNGfp_I%5EpxDHcL6YUVhp%5Enkw%5EmHN7IJoOzQnXLSF',
            height: '425px',
          },
          {
            frameTitle: 'Dropped Packets',	
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fbtannehill_adm%2Fdcco%2Fsaved%2Fsearches%2Fvisual_network_pakets2&oid=pJJHDvrE9DG0Poa11nmDmWkYDr54fT49YxGl57pkSzFgGFJ6d6fEnqRLXKbSoc17YlSdTMpdDduYneYGl%5EM8b12U95tx0ToWEJHXB6zYFU1uBnAUcOV796KpxnDtta_G%5EpMVNbKqTX9xT2W1%5ErMBLjj68AejZYZKPwqxFxg',
            height: '425px',
          },
          {
            frameTitle: 'Network Utilization',	
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fnobody%2Fdcco%2Fsaved%2Fsearches%2Fvisual_network_util2&oid=wzaSaEfNcHRqoxv_vav5HpyyzEG0xnPyX0OlUvS6lf1v%5EHBTygWyXcfijf%5EMQ6%5ECLc67VBu_za%5Etzl34ywb547Ft3vlS5Oxugun8N%5Eoa5eNI60G9Dw9UQPtXttEG07P470NNFHb5LgB3e94BS2Tcvypd8ocUB3A',
            height: '425px',
          },
          {
            frameTitle: 'Network Capacity',	
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fnobody%2Fdcco%2Fsaved%2Fsearches%2Fvisual_network_capacity&oid=neZeQDUEWyTNPjsn8lcq030mfXP%5ENia8RGqNLpK1meBgKK6BHp980BG0CswoxBMAgWc5AOAUso%5EI50QPyKKgNC0mIAuXWNL7K3dnXEq6Ez5sMITwdYA3QwqYTERsWm4_ECgnam3hSQQWHVdPynI4IAVW2e_D%5EM4a',
            height: '425px',
          },
        ]
      }
    },
  ]
};
