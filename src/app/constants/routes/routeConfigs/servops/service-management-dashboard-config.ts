import { HttpStatusCode } from '@angular/common/http';
import { protectedResources } from 'src/app/auth-config';
import { DashboardConfig } from '../../../../model/component/DynamicDashboard';

export const SERVICE_MANAGEMENT_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Service Management',
  elements: [
    {
      elementType: 'HTMLContainer',
      elementInputs: {
        sourceUrls: [
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/aea4ad7b1bd0d1506a57a8e6624bcb50',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/ec84a97b1bd0d1506a57a8e6624bcbd1',
        ]
      }
    },
    {
      elementType: 'DataTable',
      elementWidth: 'half',
      elementInputs: {
        tableTitle: 'SLA Met - List View',
        tableDataType: '',
        tableResourceUrl: protectedResources.azure.itops.SNowSlaMetList.endpoint+'?skip=0&take=100',
        noService: true,

      }
    },
    {
      elementType: 'DataTable',
      elementWidth: 'half',
      elementInputs: {
        tableTitle: 'SLA Not Met - List View',
        tableDataType: '',
        tableResourceUrl: protectedResources.azure.itops.SNowSlaNotMetList.endpoint+'?skip=0&take=100',
        noService: true,

      }
    },
    // {
    //   elementType: 'IFrame',
    //   elementInputs: {
    //     iFrameData: [
    //       {
    //         frameTitle: 'SLA Met',
    //         sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=aa9961731b14d1506a57a8e6624bcb18',
    //         linkUrl: 'https://dccotest.servicenowservices.com/nav_to.do?uri=%2F$pa_dashboard.do%3Fsysparm_dashboard%3Db125ac7b1b5c91506a57a8e6624bcb81%26sysparm_breakdown_source%3Df0418835d7231100b96d45a3ce6103b2%26sysparm_element%3D%26sysparm_element_value%3D%26sysparm_tab%3D1704217b1bd0d1506a57a8e6624bcb3f',
    //       },
    //       {
    //         frameTitle: 'SLA Not Met',
    //         sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=2e3925331b14d1506a57a8e6624bcb61',
    //         linkUrl: 'https://dccotest.servicenowservices.com/nav_to.do?uri=%2F$pa_dashboard.do%3Fsysparm_dashboard%3Db125ac7b1b5c91506a57a8e6624bcb81%26sysparm_breakdown_source%3Df0418835d7231100b96d45a3ce6103b2%26sysparm_element%3D%26sysparm_element_value%3D%26sysparm_tab%3D1704217b1bd0d1506a57a8e6624bcb3f',
    //       },
    //     ]
    //   }
    // },
    {
      elementType: 'HTMLContainer',
      elementInputs: {
        sourceUrls: [
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/0966e13f1bd0d1506a57a8e6624bcbc2',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/3c37297f1bd0d1506a57a8e6624bcb11',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/f0f7e1bf1bd0d1506a57a8e6624bcbbc',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/7e0865ff1bd0d1506a57a8e6624bcbb2',
        ]
      }
    },
    // {
    //   elementType: 'IFrame',
    //   elementInputs: {
    //     iFrameData: [
    //       {
    //         frameTitle: 'Status, Aging, and Escalated Tickets (All)',
    //         sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=64d926ca1bb055906a57a8e6624bcb9a',
    //         linkUrl: 'https://dccotest.servicenowservices.com/nav_to.do?uri=%2F$pa_dashboard.do%3Fsysparm_dashboard%3Db125ac7b1b5c91506a57a8e6624bcb81%26sysparm_breakdown_source%3Df0418835d7231100b96d45a3ce6103b2%26sysparm_element%3D%26sysparm_element_value%3D%26sysparm_tab%3D1704217b1bd0d1506a57a8e6624bcb3f',
    //       }
    //     ]
    //   }
    // },
    {
      elementType: 'DataTable',
      elementWidth: 'full',
      elementInputs: {
        tableTitle: 'Status, Aging, and Escalated Tickets (All)',
        tableDataType: '',
        tableResourceUrl: protectedResources.azure.itops.SNowDayAgingByHardwareAsset.endpoint+'?skip=0&take=100',
        noService: true,

      }
    },
  ]
};