import { DashboardConfig } from '../../../../../model/component/DynamicDashboard';
import { protectedResources } from '../../../../../auth-config';
import { TableHelper } from '../../../../../utils/table-utils';
import { SlaSummary } from '../../../../../model/SLA';

export const PERFORMANCE_ANALYSIS_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'SLA Performance',
  elements: [
    {
      elementType: 'TileRow',
      elementInputs: {
        tileRowConfig: [
          {
            cardType: 'base-blue',
            cardTitle: 'AQL Total Met or Exceeded',
            currentValue: {
              tableColumnName: '7/1/2022',
              tableColumnPropertyName: 'outcome',
              tableColumnPropertyValues: ['met', 'exceeded'],
              tableColumnValuesExcludedFromTotal: ['notapplicable'],
              tableColumnMath: 'percentFilter',
              showRecordsOf: true,
              periodType: 'previous',
              period: 'month',
            },
            prevValue: {
              tableColumnName: '6/1/2022',
              tableColumnPropertyName: 'outcome',
              tableColumnPropertyValues: ['met', 'exceeded'],
              tableColumnValuesExcludedFromTotal: ['notapplicable'],
              tableColumnMath: 'percentFilter',
              showRecordsOf: true,
              periodType: 'two-ago',
              period: 'month',
            },
            tileWidth: 'tile-half',
            numType: '%'
          },
          {
            cardType: 'base-red',
            cardTitle: 'AQL Total Not Met',
            currentValue: {
              tableColumnName: '7/1/2022',
              tableColumnPropertyName: 'outcome',
              tableColumnPropertyValues: ['failed'],
              tableColumnValuesExcludedFromTotal: ['notapplicable'],
              tableColumnMath: 'percentFilter',
              showRecordsOf: true,
              periodType: 'previous',
              period: 'month',
            },
            prevValue: {
              tableColumnName: '6/1/2022',
              tableColumnPropertyName: 'outcome',
              tableColumnPropertyValues: ['failed'],
              tableColumnValuesExcludedFromTotal: ['notapplicable'],
              tableColumnMath: 'percentFilter',
              showRecordsOf: true,
              periodType: 'two-ago',
              period: 'month',
            },
            tileWidth: 'tile-half',
            numType: '%',
            invertTrend: true,
          },
        ]
      }
    },
    {
      elementType: 'DataTable',
      elementInputs: {
        tableTitle: 'Performance Requirements',
        tableResourceUrl: protectedResources.azure.servOps.serviceLevelAgreement.endpoint+'summary',
        tableLegend: [
          {
            legendText: 'The SLA exceeds the Performance Standard Target',
            legendIcon: 'grade',
            legendColor: 'accent',
          },
          {
            legendText: 'The SLA meets the Performance Standard Target',
            legendIcon: 'check',
            legendColor: 'success',
          },
          {
            legendText: 'The SLA does not meet the Performance Standard Target',
            legendIcon: 'close',
            legendColor: 'warn',
          },
          {
            legendText: 'The SLA is not applicable in this reporting period',
            legendIcon: 'remove',
            legendColor: 'neutral',
          }
        ],
        tableDataType: SlaSummary,
        customDisplayedColumns: [
          'SLA_NUMBER',
          'SERVICE_LEVEL',
          'ACCEPTED_QUALITY_LEVEL',
          '7/1/2022',
          '6/1/2022',
          '5/1/2022',
          '4/1/2022',
          '3/1/2022',
          '2/1/2022',
          '1/1/2022',
          '12/1/2021',
          '11/1/2021',
          '10/1/2021',
          '9/1/2021',
          '8/1/2021',
        ],
        columnFormatter: [
          {
            dataValue: 'SLA_NUMBER',
            headerLabel: 'SLA #',
            cellFormatter: (row: any) => `${row.SLA_NUMBER}`,
          },
          {
            dataValue: 'SERVICE_LEVEL',
            headerLabel: 'Service Level',
            cellFormatter: (row: any) => `${row.SERVICE_LEVEL}`,
            actionsFormatter: (row: any) => [
              {
                buttonIconType: 'outlined',
                buttonLabel: row.PERFORMANCE_OBJECTIVE,
                hideLabel: true,
                buttonIcon: 'info_circle',
                buttonColor: '',
                tooltipBackground: 'neutral',
              },
            ],
          },
          {
            dataValue: 'ACCEPTED_QUALITY_LEVEL',
            headerLabel: 'AQL',
            alignContent: 'right',
            cellFormatter: (row: any) => `${row.ACCEPTED_QUALITY_LEVEL}`,
          },
          {...TableHelper.slaColumnFormatter('7/1/2022')},
          {...TableHelper.slaColumnFormatter('6/1/2022')},
          {...TableHelper.slaColumnFormatter('5/1/2022')},
          {...TableHelper.slaColumnFormatter('4/1/2022')},
          {...TableHelper.slaColumnFormatter('3/1/2022')},
          {...TableHelper.slaColumnFormatter('2/1/2022')},
          {...TableHelper.slaColumnFormatter('1/1/2022')},
          {...TableHelper.slaColumnFormatter('12/1/2021')},
          {...TableHelper.slaColumnFormatter('11/1/2021')},
          {...TableHelper.slaColumnFormatter('10/1/2021')},
          {...TableHelper.slaColumnFormatter('9/1/2021')},
          {...TableHelper.slaColumnFormatter('8/1/2021')},
        ],
      }
    }
  ]
};
