import { DashboardConfig } from '../../../../model/component/DynamicDashboard';
import { protectedResources } from '../../../../auth-config';
import { TableHelper } from '../../../../utils/table-utils';

export const INFORMATION_REPOSITORY_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Information Repository',
  elements: [
    {
      elementType: 'ExternalResourceLayout',
      elementInputs: {
        resourceTitle: 'SharePoint Documentation',
        logoImage: '../../../assets/images/sharepoint_logo.png',
        resourceSummary: 'The DCCO Program Management Office leverages SharePoint for our content and document management. This site will store all Program-related documentation such as:',
        bulletedList: ['Deliverables', 'Contract Documentation', 'Task Orders', 'Operational Policies and Procedures', 'Architectural Drawings and Diagrams', 'Other Documentation'],
        envMessage: 'This informational repository has role-based access applied. Please click below to continue.',
        resourceUrl: 'https://usdhs.sharepoint.com/sites/Perspecta_DCCO_Portal/SitePages/Home.aspx',
      }
    },
    {
      elementType: 'ExternalResourceLayout',
      elementInputs: {
        resourceTitle: 'ServiceNow Knowledgebase',
        logoImage: '../../../assets/images/servicenow_logo.png',
        resourceSummary: 'The DCCO Program Management Office leverages ServiceNow for our knowledge management articles. This site will store all Program-related documentation such as:',
        bulletedList: ['Configuration Documentation', 'Configuration Addendums', 'General Knowledgebase Articles'],
        envMessage: 'ServiceNow has role-based access applied. Please click below to continue.',
        resourceUrl: 'https://servicenow.dhs.gov/sp?id-kb_view2',
      }
    },
    {
      elementType: 'ExternalResourceLayout',
      elementInputs: {
        resourceTitle: 'GitLab',
        logoImage: '../../../assets/images/gitlab_logo.png',
        resourceSummary: 'The DCCO Program Management Office leverages GitLab for our source code and developer documentation. This site will store all Program-related documentation such as:',
        bulletedList: ['Source Code', 'Build Documentation', 'Software Specific Wiki'],
        envMessage: 'GitLab has role-based access applied. Please click below to continue.',
        resourceUrl: 'https://maestro.dhs.gov/gitlab/dccod',
      }
    },
  ]
};
