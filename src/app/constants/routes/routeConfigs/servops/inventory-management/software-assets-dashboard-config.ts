import { DashboardConfig } from '../../../../../model/component/DynamicDashboard';

export const SOFTWARE_ASSETS_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Software Assets',
  elements: [
    {
      elementType: 'IFrame',
      elementInputs: {
        iFrameData: [
          {
            frameTitle: 'Software with Status Pie',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=50472da71b2455506a57a8e6624bcbdb',
            linkUrl: 'https://dccotest.servicenowservices.com/$pa_dashboard.do?sysparm_dashboard=8580b9671b6455506a57a8e6624bcb61&sysparm_tab=4a6171e71b6455506a57a8e6624bcb94&sysparm_cancelable=true&sysparm_editable=false&sysparm_active_panel=false',
          },
          {
            frameTitle: 'Software with Status Data',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=99402dd91b6cd5106a57a8e6624bcb38',
            linkUrl: 'https://dccotest.servicenowservices.com/$pa_dashboard.do?sysparm_dashboard=8580b9671b6455506a57a8e6624bcb61&sysparm_tab=4a6171e71b6455506a57a8e6624bcb94&sysparm_cancelable=true&sysparm_editable=false&sysparm_active_panel=false',
          },
          {
            frameTitle: 'Software Items Including Status',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=b86934111ba8d5106a57a8e6624bcb17',
            linkUrl: 'https://dccotest.servicenowservices.com/$pa_dashboard.do?sysparm_dashboard=8580b9671b6455506a57a8e6624bcb61&sysparm_tab=4a6171e71b6455506a57a8e6624bcb94&sysparm_cancelable=true&sysparm_editable=false&sysparm_active_panel=false',
          },
          {
            frameTitle: 'All Software',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=d68f95991b6cd5106a57a8e6624bcb11',
            linkUrl: 'https://dccotest.servicenowservices.com/$pa_dashboard.do?sysparm_dashboard=8580b9671b6455506a57a8e6624bcb61&sysparm_tab=4a6171e71b6455506a57a8e6624bcb94&sysparm_cancelable=true&sysparm_editable=false&sysparm_active_panel=false',
          },
          {
            frameTitle: 'Software Installed by Location (Pie)',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=dbe9a9ab1b2455506a57a8e6624bcb4e',
            linkUrl: 'https://dccotest.servicenowservices.com/$pa_dashboard.do?sysparm_dashboard=8580b9671b6455506a57a8e6624bcb61&sysparm_tab=4a6171e71b6455506a57a8e6624bcb94&sysparm_cancelable=true&sysparm_editable=false&sysparm_active_panel=false',
          },
          {
            frameTitle: 'Software Items with Status',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=eae5a5671b2455506a57a8e6624bcb57',
            linkUrl: 'https://dccotest.servicenowservices.com/$pa_dashboard.do?sysparm_dashboard=8580b9671b6455506a57a8e6624bcb61&sysparm_tab=4a6171e71b6455506a57a8e6624bcb94&sysparm_cancelable=true&sysparm_editable=false&sysparm_active_panel=false',
          },
          {
            frameTitle: 'Software Assets by Task Order',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=0dca7fef1bf81d9024838407624bcbdf',
            linkUrl: 'https://dccotest.servicenowservices.com/$pa_dashboard.do?sysparm_dashboard=8580b9671b6455506a57a8e6624bcb61&sysparm_tab=4a6171e71b6455506a57a8e6624bcb94&sysparm_cancelable=true&sysparm_editable=false&sysparm_active_panel=false',
          },
          {
            frameTitle: 'Software by Component (List)',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=eece58821b8d59d024838407624bcb05',
            linkUrl: 'https://dccotest.servicenowservices.com/$pa_dashboard.do?sysparm_dashboard=8580b9671b6455506a57a8e6624bcb61&sysparm_tab=4a6171e71b6455506a57a8e6624bcb94&sysparm_cancelable=true&sysparm_editable=false&sysparm_active_panel=false',
          },
          {
            frameTitle: 'All Software Items',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=f8cdf0d51ba8d5106a57a8e6624bcb52',
            linkUrl: 'https://dccotest.servicenowservices.com/$pa_dashboard.do?sysparm_dashboard=8580b9671b6455506a57a8e6624bcb61&sysparm_tab=4a6171e71b6455506a57a8e6624bcb94&sysparm_cancelable=true&sysparm_editable=false&sysparm_active_panel=false',
          },
        ]
      }
    },
    {
      elementType: 'HTMLContainer',
      elementInputs: {
        sourceUrls: [
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/add268dc1bb459506a57a8e6624bcbf1',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/9a81c0f91b0519d024838407624bcbcc',
        ]
      }
    }
  ]
};
