import { DashboardConfig } from '../../../../../model/component/DynamicDashboard';

export const HARDWARE_ASSETS_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Hardware Assets',
  elements: [
    {
      elementType: 'HTMLContainer',
      elementInputs: {
        sourceUrls: [
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/d2c777b91b8959d024838407624bcbe9',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/c207f7d61bb4559024838407624bcb7c',
        ],
      }
    },
    {
      elementType: 'IFrame',
      elementInputs: {
        iFrameData: [
          {
            frameTitle: 'Hardware Assets',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=f7ea1a7d1b60d91024838407624bcb41',
            linkUrl: 'https://dccotest.servicenowservices.com/$pa_dashboard.do?sysparm_dashboard=9cae4ba51b20991024838407624bcb31&sysparm_tab=08de03291b20991024838407624bcb81&sysparm_cancelable=true&sysparm_editable=false&sysparm_active_panel=false',
          },
          {
            frameTitle: 'Hardware Assets by State',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=e88908461b2cd91024838407624bcb04',
            linkUrl: 'https://dccotest.servicenowservices.com/$pa_dashboard.do?sysparm_dashboard=9cae4ba51b20991024838407624bcb31&sysparm_tab=08de03291b20991024838407624bcb81&sysparm_cancelable=true&sysparm_editable=false&sysparm_active_panel=false',
          },
          {
            frameTitle: 'Hardware Assets by Location',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=16c7dbe11b60991024838407624bcb1b',
            linkUrl: 'https://dccotest.servicenowservices.com/$pa_dashboard.do?sysparm_dashboard=9cae4ba51b20991024838407624bcb31&sysparm_tab=08de03291b20991024838407624bcb81&sysparm_cancelable=true&sysparm_editable=false&sysparm_active_panel=false',
          },
          {
            frameTitle: 'Hardware Assets by Component',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=9dbf7c9e1b09d9d024838407624bcbfb',
            linkUrl: 'https://dccotest.servicenowservices.com/$pa_dashboard.do?sysparm_dashboard=9cae4ba51b20991024838407624bcb31&sysparm_tab=08de03291b20991024838407624bcb81&sysparm_cancelable=true&sysparm_editable=false&sysparm_active_panel=false',
          },
          // {
          //   frameTitle: 'Warranty Expires - 90 Days',
          //   sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=d2c777b91b8959d024838407624bcbe9',
          //   linkUrl: '',
          // },
          // {
          //   frameTitle: 'Warranty Expires - 6 Months',
          //   sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=c207f7d61bb4559024838407624bcb7c',
          //   linkUrl: '',
          // },
          {
            frameTitle: 'Hardware Assets by Task Order',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=5139ccab1bb4d99024838407624bcb79',
            linkUrl: 'https://dccotest.servicenowservices.com/$pa_dashboard.do?sysparm_dashboard=9cae4ba51b20991024838407624bcb31&sysparm_tab=08de03291b20991024838407624bcb81&sysparm_cancelable=true&sysparm_editable=false&sysparm_active_panel=false',
          },
        ]
      }
    },
  ]
};
