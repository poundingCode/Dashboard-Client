import { DashboardConfig } from '../../../../../model/component/DynamicDashboard';

export const MAINTENANCE_AGREEMENTS_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Maintenance Agreements',
  elements: [
    {
      elementType: 'IFrame',
      elementInputs: {
        iFrameData: [
          {
            frameTitle: 'Maintenance Agreements Expiring in 90 Days',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=1c4ee1631b6455506a57a8e6624bcbf5',
            linkUrl: 'https://dccotest.servicenowservices.com/nav_to.do?uri=$pa_dashboard.do%3Fsysparm_dashboard%3D8580b9671b6455506a57a8e6624bcb61%26sysparm_tab%3D4fd1312b1b6455506a57a8e6624bcb06%26sysparm_cancelable%3Dtrue%26sysparm_editable%3Dundefined%26sysparm_active_panel%3Dfalse',
          },
          {
            frameTitle: 'Maintenance Agreements Expiring in 6 Months',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=b63f25c81bc99d9024838407624bcb5a',
            linkUrl: 'https://dccotest.servicenowservices.com/nav_to.do?uri=$pa_dashboard.do%3Fsysparm_dashboard%3D8580b9671b6455506a57a8e6624bcb61%26sysparm_tab%3D4fd1312b1b6455506a57a8e6624bcb06%26sysparm_cancelable%3Dtrue%26sysparm_editable%3Dundefined%26sysparm_active_panel%3Dfalse',
          },
        ]
      }
    },
  ]
};
