import { DashboardConfig } from '../../../../model/component/DynamicDashboard';

export const SECURITY_SPLUNK_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Security',
  elements: [
    {
      elementType: 'IFrame',
      elementInputs: {
        iFrameData: [
          {
            frameTitle: 'Vulnerability Severity Level - Total Across All Devices',
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fbtannehill_adm%2Fdcco%2Fsaved%2Fsearches%2Fvisual_vuln_total&oid=%5EX_qNGCwCRAb9Wrd2DgpXCyJYEEo1ue6R%5Er5vMPAilKiIZBVs1UUnMLOTzDwURxsbR5expDIehfIkTsEHM0QHsguJD5kFGMGyfCe7BbXQK_t3XdLV_sH%5EovmfXgoGv7Fz3Qord79ywSK_Lv1Bvjx_3uGxsrAdijW%5EM7',
            height: '300px',
          },
          {
            frameTitle: 'Vulnerability Severity Level - Total Across All Devices',
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fbtannehill_adm%2Fdcco%2Fsaved%2Fsearches%2Fvisual_vuln_totals&oid=q7aSTt_hlk_sqY_1JPbt%5EtVDE_PlDQmURuholew31smyuTv5RbEjJn0CLCJ9XVMIUMbtRihZsQewbqdEHOs_6lhWhoBUtU2IktNWoeR1AslQDz7y3v5gFgPhrcS_VefnZASDWu_H6z9Rl170AVQHLQTk4AiRhumdXF',
            height: '300px',
          },
          {
            frameTitle: 'Severity Level - Critical',
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fnobody%2Fdcco%2Fsaved%2Fsearches%2Fvisual_vuln_crit&oid=xneR2v3BF4Bnb861EXiMtkHBtDqcJz6EXmbo6FgFU83p67Vyi%5EYZUspmOqqVLPZYPMHsO3OqPsC4NXQN_Xnx_m219nkthN1fqEg7JL_LkZJvIeDGqFVXyl8dvnPIFxQEEuNqhKpyDFWPHfBNPO0dR7ZdSC',
            height: '300px',
          },
          {
            frameTitle: 'Severity Level - High',
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fnobody%2Fdcco%2Fsaved%2Fsearches%2Fvisual_vuln_high&oid=MzuE45n1we_2LK_VqlQyYKfveCMSFQxY2ZZgrRFVtVBNU_0BAiiQSoHu3YmIcUY0aess3KI7YVG%5E1V2A6hzoyyl_JKG35eWkiJYDCYeub%5E2h8A2vrrLng%5ERlM4nUKpGhQn3ml3f1EADK%5ESzxDxiU',
            height: '300px',
          },
          {
            frameTitle: 'Severity Level - Medium',
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fnobody%2Fdcco%2Fsaved%2Fsearches%2Fvisual_vulns_medium&oid=jEKxX%5Epxv6E_36y2NNEVEwu_uwugf1ztKzFWlwnPvewWNeDtImJqrAzVtXWtP1_GLFzbdeDcOkyVq8k2BthOHuqKtvSVIorPXHh0W%5EydYlLPgstGlpiO1hxS1ERM%5EgBk6PEBgET6q19ZPa%5ETE3Y_fb_LWkSy2px',
            height: '300px',
          },
          {
            frameTitle: 'Severity Level - Low',
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fnobody%2Fdcco%2Fsaved%2Fsearches%2Fvisual_vuln_low&oid=TWfY59ywuYlf_nL3SfIMJ52YahnRSf1fOEWXmTDSd1eLBm3nsWOdoWAhkwriG9AVbqsRxJl31OUOHd4kRPiD80s_vg145Meayp6mRH3Qnw8P0z5AdXTYFuTJUhHpkxAxgsgOOa7tOTkPvSYA7RMVUaHEAzo',
            height: '300px',
          },
          {
            frameTitle: 'Severity Level - None',
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fbtannehill_adm%2Fdcco%2Fsaved%2Fsearches%2Fvisual_vulns_none&oid=HW0fo2YWMOhVtvf3zQ8VvkuX_XlNKnzhqwpJwh_1C889aSUgVhgLYfkavJ97NZKlpFNNYLMl40kLkRYvmXKLsmdznhs8mE_uuEY%5EM%5Ee3UrVO8VMReDFd12VCKF8hlLnRyMK26EOG5F7ynsWcScQkiD0mo2tJhC3ZiIg',
            height: '300px',
          },
          {
            frameTitle: 'Vulnerability Identifier - Top 50',
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fnobody%2Fdcco%2Fsaved%2Fsearches%2Fvisual_vuln_top_ip&oid=%5E8kVrcKruTSjZdc7S7P9VwdBpmXs9Y4sm5Xh95m%5EF0HaYouLmETVzcWhbfhdfLocvxCk2ybkGp9cM_gxTn6ZbIBXSI3RY2kmimEiPo7kXBYkfgqLHkO8H6C14nOzK3J5EvbgyD24kLkwsf6wXsj_KSr',
            height: '300px',
          },
          {
            frameTitle: 'Vulnerabilities by Host/System - Top 50',
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fnobody%2Fdcco%2Fsaved%2Fsearches%2Fvisual_vuln_top_hostname&oid=itW68XD8Rdu2eXOKNdCg_UNxchAMR45G6%5Eq83HI1d97wF9qxnlj7bPFEvOoQHxL6MNuViiyHIPBj1Ri%5EshVphBvU9bgpg%5E15Hxf%5Ev7NEcQk%5EaRKjTRg0JUkTH49rUwHwbyj0Cv4HX8qHcwUOkCMVVoWp6y6%5EHF6WZo',
            height: '300px',
          },
          {
            frameTitle: 'Vulnerabilities by IP Address - Top 50',
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fnobody%2Fdcco%2Fsaved%2Fsearches%2Fvisual_vuln_top_ip&oid=S2O8CsNpMgMoU1OTrKbqPKJI3FqoVcbV%5EtQZOZ1mPEfDqKH_1CaX0Fmybri%5EXkVkHybYhNPPTaNJzx5HUFxGkEeHHtcDh2qDkhPRJ2L9TkZ_r_FiE5VPZMIS_OUMr6tvLJ2E2n3YKJDt4SOIQeu5jN',
            height: '300px',
          },
          {
            frameTitle: 'Date and Time - First Detected',
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fbtannehill_adm%2Fdcco%2Fsaved%2Fsearches%2Fvisual_vulns_oldest&oid=tIB4f2NpnQhrOHCmXWTGsKbSAqeVvW81IoDuxGAAoDksE8Epn%5ERuuVPtfRpWbIMWJYU1lA4unPCkmIPfLPITHrz8Kau14AE_pVD7AWd%5EKSR4kd5OO1za50OLXQyS6IL8r3ycEPf1rKKh%5EWEzMtti417%5EUOQJyB1mX3Lc4F0',
            height: '300px',
          },
          {
            frameTitle: 'Date and Time - Last Detected',
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fbtannehill_adm%2Fdcco%2Fsaved%2Fsearches%2Fvisual_vulns_recent&oid=cc8oM8pwfL3s0169VJ8C8Wz7mXK60nXRkzinxBrBJMCilGGNGaPLt6PDSPI9ajkC8UfE2pELuHpLpEkmvGMS83AVh2Ls010rdjMngXiix6uMuEShFOKKm6Pfr_BzVMKEfcpgJWeHTLXWroX6cK6ZzTeLwlJMk5OPQaR',
            height: '300px',
          },
          {
            frameTitle: 'Vulnerability Count by OS',
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fnobody%2Fdcco%2Fsaved%2Fsearches%2Fvisual_vulns_os&oid=56SUiDsELFuKU44YPXQM47tq_rNdrLJF%5E18Isncn_wpOv0MgS4r2LOBBjg7Y%5E1%5EjE_06DYlkfqxF8RStsGV49uN5znT4_uLMPDspWy0FcQIMRmehBqi9l07eSdmN%5E8jV0VKb4oRZrPK%5EwlmMMqW7p8Ei',
            height: '300px',
          },
          {
            frameTitle: 'Vulnerabilities by Score Range',
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fbtannehill_adm%2Fdcco%2Fsaved%2Fsearches%2Fvisual_vuln_score&oid=Z6qm_4pGHaTa2Q6BjUij_e2Hs9f6l32OYhyjUbf6iHGQapdZB1O60K384DPKaWuIfQyDAxm8o5Y9pDVtZI3s3faZU4ZYuF16xl2KVvelE11dbrUz1VFtB_sXTcAGsDU%5EohedEAhfMlh1rowa9CLC4DC3MzlS1UlWIgsV',
            height: '300px',
          },
          {
            frameTitle: 'Vulnerabilities by Business Unit',
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fnobody%2Fdcco%2Fsaved%2Fsearches%2Fvisual_vuln_isso&oid=e2mwTgPwxlJEhGA0h%5EkmyEpv9XbXfFWsuZH5Kw_qyrMX0nsESdEYuo0tP1V4o1bFQ9IyLERtcYJzMpcYTqhcep44VRhykqGx0medF97I_KrfxlxFybnteF4E7TqWHPXyYDo4%5ExJeLTGTLNwoUxiPdN',
            height: '300px',
          },
          {
            frameTitle: 'Average Time to Mitigate',
            sourceUrl: 'https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fnobody%2Fdcco%2Fsaved%2Fsearches%2Fvisual_vulns_ttm&oid=RMMKRddSAoerLZO8GJ8O%5EOXAv8lNWpmGYqClS1ZN7wsVbcmxjxXxrDnyHUx42apt1DEndFgH4Qzl1huljwKx_K6hxCH8h6ADL5WBTCRzylwj9Uxbc_zp1IV3gQRxnGtkV%5EKMV9Yn_Y0bM0K7EQoA7h5Cyo',
            height: '300px',
          },
          {
            frameTitle: 'Vulnerabilities by Application',
            sourceUrl: '  https://dccodev-splunk-es.usgovvirginia.cloudapp.usgovcloudapi.net:8088/en-US/embed?s=%2FservicesNS%2Fnobody%2Fdcco%2Fsaved%2Fsearches%2Fvisual_vuln_app&oid=t9MsU9isJ6HwAgFWdKgBg_qoLCIA5J_3WHMIkQFzhw0Nff0mhBJIh4dMKOWXuWnE0onj4rha_8EqcAdeaum6%5E3LXfznd1udq2_lkKA_YWrP7vbjBXXb_FagbVpTjHUApqeJQzgchfN2KHTg5TfYdH4CK5AF',
            height: '300px',
          }
        ]
      }
    }
  ]
};