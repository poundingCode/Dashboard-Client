import { DashboardConfig } from '../../../../model/component/DynamicDashboard';

export const SECURITY_SNOW_DASHBOARD_CONFIG: DashboardConfig = {
  title: 'Security',
  elements: [
    {
      elementType: 'IFrame',
      elementInputs: {
        iFrameData: [
          {
            frameTitle: 'Vulnerability Severity Level - Total Across All Devices',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=c89c69ae1bc015506a57a8e6624bcb93',
          },
          {
            frameTitle: 'Scan Results by OS',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=2f507b1f1b14115024838407624bcb34',
          }
        ]
      }
    },
    {
      elementType: 'HTMLContainer',
      elementInputs: {
        sourceUrls: [
          // 'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/c89c69ae1bc015506a57a8e6624bcb93',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/31e5d27f1b20d5506a57a8e6624bcb26',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/a4f5927f1b20d5506a57a8e6624bcbdb',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/c106567f1b20d5506a57a8e6624bcb8d',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/5b069a3f1b20d5506a57a8e6624bcb2d',
          'https://dccotest.servicenowservices.com/api/g_pf/dcco_score/report/8c26da7f1b20d5506a57a8e6624bcb4c'
        ],
      }
    },
    {
      elementType: 'IFrame',
      elementInputs: {
        iFrameData: [
          {
            frameTitle: 'Vulnerabilities by Host/System - Top 50',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=59eeb3fc1be0951024838407624bcbaa', 
          },
          {
            frameTitle: 'Vulnerability Identifier - Top 50',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=a464e9161bcc91506a57a8e6624bcb9d', 
          },
          {
            frameTitle: 'Vulnerabilities by IP Address - Top 50',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=c30b5ba71b18515024838407624bcb00', 
          },
          {
            frameTitle: 'Date and Time - First Detected',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=c55347b31b64d5506a57a8e6624bcb3d', 
          },
          {
            frameTitle: 'Date and Time - Last Detected',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=284cd2a81b24151024838407624bcbf4',
          },
          {
            frameTitle: 'Time to Mitigation - within 7 Days',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=9c164f821bb455906a57a8e6624bcb33',
          },
          {
            frameTitle: 'Time to mitigation - within 14 Days',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=ca1380e21b78559024838407624bcb92',
          },
          {
            frameTitle: 'Time to mitigation - within 21 Days',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=855d0ce21bb8559024838407624bcb65',
          },
          {
            frameTitle: 'Time to mitigation - within 30 Days',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=723f4ce61bb8559024838407624bcb3f',
          },
          {
            frameTitle: 'Time to mitigation - within 60 Days',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=53c0d46a1bb8559024838407624bcb0c',
          },
          {
            frameTitle: 'Time to mitigation - Over 60 Days',
            sourceUrl: 'https://dccotest.servicenowservices.com/sys_report_display.do?sysparm_report_id=edc1542e1bb8559024838407624bcb83',
          }
        ]
      }
    }
  ]
};