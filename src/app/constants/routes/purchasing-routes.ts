import { DynamicDashboardComponent } from 'src/app/components/dynamic-dashboard/dynamic-dashboard.component';
import { IRoute } from '../../model/IRoute';
// purchasing > cost
import { CONTRACT_LEVEL_COST_BENCHMARKS_DASHBOARD_CONFIG } from './routeConfigs/purchasing/cost/contract-level-cost-benchmarks-dashboard-config';
import { CONTRACT_LEVEL_SUMMARY_COSTS_DASHBOARD_CONFIG } from './routeConfigs/purchasing/cost/contract-level-summary-cost-dashboard-config';
import { TASK_ORDER_PURCHASING_DASHBOARD_CONFIG } from './routeConfigs/purchasing/cost/task-order-purchasing-dashboard-config';
import { SPACE_BASED_COST_BENCHMARKS_DASHBOARD_CONFIG } from './routeConfigs/purchasing/cost/space-based-cost-benchmarks-dashboard-config';
import { INDIVIDUAL_RESOURCE_COSTS_HCE_DASHBOARD_CONFIG } from './routeConfigs/purchasing/cost/individual-resource-costs-hce-dashboard-config';
import { INDIVIDUAL_RESOURCE_COSTS_CSP_DASHBOARD_CONFIG } from './routeConfigs/purchasing/cost/individual-resource-costs-csp-dashboard-config';
import { LABOR_HOUR_COSTS_DASHBOARD_CONFIG } from './routeConfigs/purchasing/cost/labor-hour-costs-dashboard-config';
// purchasing > catalog purchases
import { COTS_EQUIPMENT_AND_SOFTWARE_ITEMS_DASHBOARD_CONFIG } from './routeConfigs/purchasing/catalog/cots-equipment-and-software-items-dashboard-config';
import { PROFESSIONAL_SERVICES_DASHBOARD_CONFIG } from './routeConfigs/purchasing/catalog/professional-services-dashboard-config';

export const PURCHASING_COST_ROUTES: IRoute[] = [
  {
    path: '',
    title: '',
    redirectTo: 'contract-level-cost-benchmarks',
    pathMatch: 'full',
  },
  {
    path: 'contract-level-cost-benchmarks',
    title: 'Contract-Level Cost Benchmarks',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: CONTRACT_LEVEL_COST_BENCHMARKS_DASHBOARD_CONFIG,
    },
  },
  {
    path: 'contract-level-summary-costs',
    title: 'Contract-Level Summary Costs',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: CONTRACT_LEVEL_SUMMARY_COSTS_DASHBOARD_CONFIG,
    },
  },
  {
    path: 'task-order-purchasing',
    title: 'Task Order Purchasing',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: TASK_ORDER_PURCHASING_DASHBOARD_CONFIG,
    },
  },
  {
    path: 'space-based-cost-benchmarks',
    title: 'Space-Based Cost Benchmarks',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: SPACE_BASED_COST_BENCHMARKS_DASHBOARD_CONFIG,
    },
  },
  {
    path: 'individual-resource-costs-hce',
    title: 'Individual Resource Costs HCE',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: INDIVIDUAL_RESOURCE_COSTS_HCE_DASHBOARD_CONFIG,
    },
  },
  {
    path: 'individual-resource-costs-csp',
    title: 'Individual Resource Costs CSP',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: INDIVIDUAL_RESOURCE_COSTS_CSP_DASHBOARD_CONFIG,
    },
  },
  {
    path: 'labor-hour-costs',
    title: 'Labor Hour Costs',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: LABOR_HOUR_COSTS_DASHBOARD_CONFIG,
    },
  },
];

export const PURCHASING_CATALOG_PURCHASES_ROUTES: IRoute[] = [
  {
    path: 'cots-equipment-and-software-items',
    title: 'COTS Equipment & Software Items',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: COTS_EQUIPMENT_AND_SOFTWARE_ITEMS_DASHBOARD_CONFIG,
    },
  },
  {
    path: 'professional-services',
    title: 'Professional Services',
    component: DynamicDashboardComponent,
    data: {
      dashboardConfig: PROFESSIONAL_SERVICES_DASHBOARD_CONFIG,
    },
  },
];

export const PURCHASING_ROUTES: IRoute[] = [
  {
    path: '',
    title: '',
    redirectTo: 'cost',
    pathMatch: 'full',
  },
  {
    path: 'cost',
    title: 'Cost',
    icon: 'sell',
    children: PURCHASING_COST_ROUTES,
  },
  {
    path: 'catalog-purchases',
    title: 'Catalog Purchases',
    icon: 'shopping_cart',
    children: PURCHASING_CATALOG_PURCHASES_ROUTES,
  },
];
