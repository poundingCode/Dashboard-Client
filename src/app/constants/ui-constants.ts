export const DATA_VIZ_PALETTE = [
  '#005288', '#7FD1AF', '#008BAC', '#C1FAA8', '#003E67', '#A1E6B4', '#22739F', '#DDFEA1', '#54B8BC'
];

export const DATA_VIZ_GRADIENT_PALETTE = [
  '#003E67', '#005288', '#22739F', '#008BAC', '#54B8BC', '#7FD1AF', '#A1E6B4', '#C1FAA8', '#DDFEA1'
];

export const SEVERITY_LEVEL_PALETTE = [
  '#32976C',
  '#93C164',
  '#DEC667',
  '#D87C32',
  '#C41230',
];