import { Utils } from './utils';
import { MONTHS, MONTHS_IN_FISCAL_YEAR_ORDER, FISCAL_QUARTERS_BY_MONTH, PROJECTED_ANNUAL_INCREASE_PERCENTAGE } from '../constants/data-constants';

export class TileHelper {
  static abbreviateNumber(val: any, numType: any = '') {
    const value = Utils.parseNum(val);
    if (Utils.isNumeric(value) && +value > 1000) {
      const suffixes = ["", "K", "M", "B", "T"];
      const suffixNum = Math.ceil(("" + Math.floor(+value)).length / 3 - 1);
      let abbrevVal = parseFloat((+value / Math.pow(1000, suffixNum)).toPrecision(4));
      if (abbrevVal % 1 !== 0) {
        abbrevVal = +abbrevVal.toFixed(2);
      }
      return `${numType === '$' ? '$' : ''}${abbrevVal + suffixes[suffixNum]}${numType === '%' ? '%' : ''}`;
    }
    const numTypes: any = {
      '$': Utils.formatUSD(value),
      '%': Utils.formatPercent(value),
      '': value,
    }
    return numTypes[numType];
  }

  static tableHasColumn = (tableData: Array<any>, columnName: string) => tableData && tableData.find((tableRow) => tableRow[columnName]);

  static sumRecords = (tableData: Array<any>) => tableData.length;

  static sumTableColumn = (tableData: Array<any>, columnName: string) => tableData.reduce((acc, obj) => +acc + (Utils.isObject(obj[columnName]) ? 0 : +Utils.parseNum(obj[columnName])), 0).toFixed(2);

  static shiftToFiscal = (monthlyTotals: Array<any>) => {
    let fiscalMonths = monthlyTotals;
    fiscalMonths.unshift(fiscalMonths[11]);
    fiscalMonths.pop();
    fiscalMonths.unshift(fiscalMonths[11]);
    fiscalMonths.pop();
    fiscalMonths.unshift(fiscalMonths[11]);
    fiscalMonths.pop();
    return fiscalMonths;
  }

  static reportTableColumnValueByMonth(tableData: Array<any>, columnName: string, periodColumnName: string, value: 'highMonth' | 'lowMonth') {
    const totalsByMonth = this.shiftToFiscal(this.totalTableColumnsByMonth(tableData, columnName, periodColumnName));

    return {
      'highMonth': Math.max(...totalsByMonth).toFixed(2),
      'lowMonth': Math.min(...totalsByMonth).toFixed(2),
    }[value];
  }

  static tableColumnValueByMonthLabel(tableData: Array<any>, columnName: string, periodColumnName: string, value: 'highMonth' | 'lowMonth') {
    const totalsByMonth = this.shiftToFiscal(this.totalTableColumnsByMonth(tableData, columnName, periodColumnName));

    const highLowValueMap = {
      'highMonth': Math.max(...totalsByMonth),
      'lowMonth': Math.min(...totalsByMonth),
    };
    return MONTHS_IN_FISCAL_YEAR_ORDER[totalsByMonth.indexOf(highLowValueMap[value].toFixed(2).toString())];
  }

  static totalTableColumnsByMonth = (tableData: Array<any>, columnName: string, periodColumnName: string) => this.sortTableRowsByMonth(tableData, periodColumnName).map((monthArray: Array<any>) => this.sumTableColumn(monthArray, columnName));

  static getPercentageOfTotalRecordsByFilter = (tableData: Array<any>, columnName: string, propertyName: string, propertyValues: Array<any>, excludedValues: any) => {
    const filterByProperty = this.filterTableRows(tableData, columnName, propertyName, propertyValues);
    return ((this.sumRecords(filterByProperty) / this.sumRecords(this.tableRowExclusionFilter(tableData, columnName, propertyName, excludedValues))) * 100).toFixed(2);
  };

  static sortTableRowsByMonth(tableData: Array<any>, periodColumnName: string) {
    const latestDate = this.getLatestDateInTableColumn(tableData, periodColumnName);
    return tableData
      .filter((row) => this.dateIsInFiscalYear(latestDate, row[periodColumnName], 'current'))
      .reduce((acc, obj) => {
        const rowDatePeriods = this.getTimePeriodsForDate(new Date(obj[periodColumnName]));
        acc[rowDatePeriods.month].push(obj);
        return acc;
      }, [ [], [], [], [], [], [], [], [], [], [], [], [] ]);
  }

  static getHighestValueInColumn = (tableData: Array<any>, tableColumnName: string) => Math.max(...tableData.map((tableRow) => Utils.parseNum(tableRow[tableColumnName])));

  static getLowestValueInColumn = (tableData: Array<any>, tableColumnName: string) => Math.min(...tableData.map((tableRow) => Utils.parseNum(tableRow[tableColumnName])));

  static averageTableColumn = (tableData: Array<any>, columnName: string) => tableData.length ? (this.sumTableColumn(this.removeZeroValues(tableData, columnName), columnName) / this.sumRecords(this.removeZeroValues(tableData, columnName))) : 0.00;

  static filteredAverageTableColumn = (tableData: Array<any>, columnName: string, filterColumn: string, filterValue: string) => this.averageTableColumn(tableData.filter((tableRow) => tableRow[filterColumn] === filterValue), columnName);

  static filterTableRows = (tableData: Array<any>, columnName: string, propertyName: string | null, filterValues: string | Array<any>) => tableData.filter((tableRow) => propertyName ? filterValues.includes(tableRow[columnName][propertyName]) : filterValues.includes(tableRow[columnName]));

  static removeZeroValues = (tableData: Array<any>, columnName: string) => tableData.filter((tableRow) => Utils.parseNum(tableRow[columnName]) !== 0);

  static countRecordsWithValue = (tableData: Array<any>, columnName: string, propertyName: string, filterValues: string | Array<any>) => this.sumRecords(this.filterTableRows(tableData, columnName, propertyName, filterValues));

  static tableRowExclusionFilter = (tableData: Array<any>, columnName: string, propertyName: string, excludedValues: string | Array<any>) => tableData.filter((tableRow) => propertyName ? !excludedValues.includes(tableRow[columnName][propertyName]) : !excludedValues.includes(tableRow[columnName]));

  static getStandardDeviation = (tableData: Array<any>, columnName: string) => Math.sqrt(tableData.map((el) => (+Utils.parseNum(el[columnName]) - +this.averageTableColumn(tableData, columnName)) ** 2).reduce((acc, curr)=> acc + +curr, 0) / tableData.length).toFixed(2);

  static shiftMonth = (month: number, shift: 'next' | 'previous') => ({
    'next': month === 11 ? 0 : month + 1,
    'previous': month === 0 ? 11 : month - 1,
  }[shift]);

  static shiftQuarter = (quarter: number, shift: 'next' | 'previous') => ({
    'next': quarter === 4 ? 1 : quarter + 1,
    'previous': quarter === 1 ? 4 : quarter - 1,
  }[shift]);

  static dateIsInFiscalYear = (currentDate: any, date: any, testYear: 'next' | 'current' | 'previous') => {
    const today = this.getTimePeriodsForDate(currentDate || new Date());
    const compareDate = this.getTimePeriodsForDate(new Date(date));
    const { month: thisMonth, year: thisYear } = today;
    const { month, year } = compareDate;
    const fiscalYears = {
      'next': thisMonth > 8 ? year === thisYear + 1 : (year === thisYear + 1 && month <= 8) || (year === thisYear && month > 8),
      'current': thisMonth > 8 ? year === thisYear : (year === thisYear && month <= 8) || (year === thisYear - 1 && month > 8),
      'previous': thisMonth > 8 ? year === thisYear - 1 : (year === thisYear - 1 && month <= 8) || (year === thisYear - 2 && month > 8),
    };
    return fiscalYears[testYear];
  };

  static twoDigitYear = (date = new Date()) => date.toLocaleDateString('en', { year: '2-digit'});

  static getTwoDigitFiscalYear = (date = new Date()) => date.getMonth() > 8 ? +this.twoDigitYear(date) + 1 : +this.twoDigitYear(date);

  static getFiscalYear = (date = new Date()) => (date.getMonth() > 8 ? +date.getFullYear() + 1 : +date.getFullYear());

  static fiscalYearOfPriorQuarter = (date = new Date()) => this.shiftQuarter(FISCAL_QUARTERS_BY_MONTH[date.getMonth()], 'previous') === 4 ? this.getFiscalYear(date) - 1 : this.getFiscalYear(date);

  static fiscalYearOfPriorMonth = (date = new Date()) => this.shiftMonth(date.getMonth(), 'previous') === 8 ? this.getFiscalYear(date) - 1 : this.getFiscalYear(date);

  static getLatestDateInTableColumn = (tableData: Array<any>, columnName: string) => {
    let latestDate;
    if (tableData.length > 1 && columnName) {
      latestDate = tableData.reduce((a, b) => this.checkYearOnlyDate(a[columnName], columnName) > this.checkYearOnlyDate(b[columnName], columnName) ? a[columnName] : b[columnName]);
    } else if (tableData.length === 1 && columnName) {
      latestDate = tableData.map((tableRow) => this.checkYearOnlyDate(tableRow[columnName], columnName));
    } else {
      return null;
    }
    return latestDate === 'Invalid Date' ? '' : this.checkYearOnlyDate(latestDate, columnName);
  }

  static checkYearOnlyDate = (date: any, dateColumn: string) => dateColumn === 'YEAR' ? new Date(date, 9) : new Date(date);

  static filterTableRowsByDateRange(tableData: Array<any>, dateColumn: string, period: string, periodType: string) {
    const currentDate = this.getLatestDateInTableColumn(tableData, dateColumn) || new Date();
    const currentDatePeriods = this.getTimePeriodsForDate(currentDate);
    const { month: thisMonth, quarter: thisQuarter, year: thisFiscalYear } = currentDatePeriods;

    return tableData.filter((tableRow) => {
      const dateValue = tableRow[dateColumn];
      const rowDate = this.checkYearOnlyDate(dateValue, dateColumn);
      const rowDateStamp = this.getTimePeriodsForDate(rowDate);
      const { month, quarter, year: rowFiscalYear } = rowDateStamp;

      const periodComparisonMap: any = {
        'next': {
          'month': month === this.shiftMonth(thisMonth, 'next') && rowFiscalYear === thisFiscalYear + 1,
          'quarter': quarter === this.shiftQuarter(thisQuarter, 'next') && rowFiscalYear === thisFiscalYear + 1,
          'year': this.dateIsInFiscalYear(currentDate, rowDate, 'next'),
        },
        'current': {
          'month': month === thisMonth && rowFiscalYear === thisFiscalYear,
          'quarter': quarter === thisQuarter && rowFiscalYear === thisFiscalYear,
          'year': this.dateIsInFiscalYear(currentDate, rowDate, 'current'),
        },
        'previous': {
          'month': month === this.shiftMonth(thisMonth, 'previous') && this.fiscalYearOfPriorMonth(rowDate) === this.fiscalYearOfPriorMonth(currentDate),
          'quarter': quarter === this.shiftQuarter(thisQuarter, 'previous') && this.fiscalYearOfPriorQuarter(rowDate) === this.fiscalYearOfPriorQuarter(currentDate),
          'year': this.dateIsInFiscalYear(currentDate, rowDate, 'previous'),
        },
        'year-ago': {
          'month': month === thisMonth && rowFiscalYear === thisFiscalYear - 1,
          'quarter': quarter === thisQuarter && rowFiscalYear === thisFiscalYear - 1,
          'year': this.dateIsInFiscalYear(currentDate, rowDate, 'previous'),
        }
      };

      return periodComparisonMap[periodType][period];
    })
  }

  static formatPeriodLabel(periodType: string, period: string, tableData: Array<any>, dateColumn: string) {
    const currentDate = this.getLatestDateInTableColumn(tableData, dateColumn) || new Date();
    const currentDatePeriods = this.getTimePeriodsForDate(currentDate);
    const { month: thisMonth, quarter: thisQuarter, year: thisFiscalYear } = currentDatePeriods;
    // const daysLastMonth = new Date(thisFiscalYear, thisMonth, 0).getDate();

    if (period && periodType) {
      const periodLabelMap: any = {
        next: {
          month: `${MONTHS[this.shiftMonth(thisMonth, 'next')]}`,
          quarter: 'Next Quarter',
          year: `FY${thisFiscalYear+1} (+${PROJECTED_ANNUAL_INCREASE_PERCENTAGE}%)`,
          genericYear: 'Next Fiscal Year',
        },
        current: {
          month: `${MONTHS[thisMonth]} FY${thisFiscalYear}`,
          quarter: `Q${thisQuarter} FY${thisFiscalYear}`,
          year: `FY${thisFiscalYear} through ${MONTHS[thisMonth]}`, // ${daysLastMonth}`,
        },
        previous: {
          month: `${MONTHS[this.shiftMonth(thisMonth, 'previous')]} FY${this.fiscalYearOfPriorMonth(currentDate)}`,
          quarter: `Q${this.shiftQuarter(thisQuarter, 'previous')} FY${this.fiscalYearOfPriorQuarter(currentDate)}`,
          year: `FY${+thisFiscalYear - 1}`,
        },
        'two-ago': {
          month: `${MONTHS[this.shiftMonth(this.shiftMonth(thisMonth, 'previous'), 'previous')]}`,
          quarter: `Q${this.shiftQuarter(this.shiftQuarter(thisQuarter, 'previous'), 'previous')}`,
          year: `FY${+thisFiscalYear - 2}`,
        },
        'year-ago': {
          month: `${MONTHS[thisMonth]} FY${+thisFiscalYear - 1}`,
          quarter: `Q${thisQuarter} FY${+thisFiscalYear - 1}`,
          year: `FY${+thisFiscalYear - 1}`,
        }
      };
      return periodLabelMap[periodType][period];
    }
    return '';
  }

  static getTimePeriodsForDate = (date: Date) =>
    ({
      month: date.getMonth(),
      quarter: FISCAL_QUARTERS_BY_MONTH[date.getMonth()],
      year: this.getFiscalYear(date),
    });
}
