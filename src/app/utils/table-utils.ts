import { Utils } from './utils';
import { TableColumn } from '../model/component/DataTable';
import { PERFORMANCE_ANALYSIS_FILTER_OPTIONS, OutcomeToIconMap, OutcomeToColorMap } from '../model/SLA';

export class TableHelper {
  // pre-process the table data based on config settings BEFORE it is stored as the table's dataSource
  static formatTableData = (tableData: Array<any>, columnFormatter: TableColumn[]) => {
    return !columnFormatter
      ? tableData
      : (tableData ? tableData.map((tableRow: any) => {
        let formattedRow = { ...tableRow };
        columnFormatter.forEach((formatter: TableColumn) => {
          const cellKey = formatter.dataValue;
          const cellFormattedValue = formatter?.cellFormatter ? formatter?.cellFormatter(tableRow) : tableRow[cellKey];
          if (cellFormattedValue === {} || cellFormattedValue === '[object Object]') {
            formattedRow = { ...formattedRow, [cellKey]: '' };
          } else {
            formattedRow = { ...formattedRow, [cellKey]: cellFormattedValue };
          }
        });
        return formattedRow;
      }) : []);
  }

  static downloadCSVFile = (data: any, tableTitle: string) => {
    let blob = new Blob([data], {type: 'text/plain'});
    let fileName = Utils.dashString(tableTitle) + '-Export-All.csv';
    let a = document.createElement('a');
    a.href = URL.createObjectURL(blob);
    a.download = fileName;
    a.click();
  }

  static getUniqueColumnValuesForFilterDropdown = (tableData: Array<any>, columnName: string, propertyName: string) => [...new Set(tableData.map((tableRow) => propertyName ? tableRow[columnName][propertyName] : tableRow[columnName]))].reduce((a, b) => ({ ...a, [b]: b }), {});

  static slaOutcomeFormatter = (slaOutcome: string) => Utils.capIt(slaOutcome);

  static slaDateHeaderFormatter = (slaColumnDate: string) => new Date(slaColumnDate).toLocaleString('en-us', { month: 'short' }) + ' \'' + new Date(slaColumnDate).getFullYear().toString().slice(-2);

  static slaColumnFormatter = (columnValue: any) => {
    return ({
      dataValue: columnValue,
      headerLabel: this.slaDateHeaderFormatter(columnValue),
      alignContent: 'center',
      filterOptions: PERFORMANCE_ANALYSIS_FILTER_OPTIONS,
      filterProperty: 'outcome',
      actionsFormatter: (row: any) => [
        {
          buttonLabel: `${row[columnValue]?.value} (${TableHelper.slaOutcomeFormatter(row[columnValue]?.outcome)})`,
          hideLabel: true,
          buttonIcon: OutcomeToIconMap[row[columnValue]?.outcome || 'default'],
          buttonColor: OutcomeToColorMap[row[columnValue]?.outcome || 'default'],
          actionType: 'button',
        },
      ],
    });
  }
}