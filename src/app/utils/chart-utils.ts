import { Utils } from './utils';

export class ChartHelper {
  static processSimpleChartData = (data: any, nameProperty: any, valueProperty: any, useLog: boolean = false) => data.map((dataRow: any) => ({
    name: dataRow[nameProperty] || dataRow.name,
    value: useLog ? Math.log10(dataRow[valueProperty] || dataRow.value) : dataRow[valueProperty] || dataRow.value,
    extra: {
      ...dataRow.extra || dataRow,
    }
  }));

  static processSeriesChartData = (data: any, useLog: boolean = false) => (data || []).map((dataRow: any) => ({
    name: dataRow.name,
    series: [
      ...(dataRow.series || dataRow.name || []).map((seriesItem: { name: string, value: number, extra: object }) => ({
          name: seriesItem.name,
          value: useLog ? Math.log10(seriesItem.value) : seriesItem.value,
          ...(seriesItem.extra && seriesItem.extra)
      }))
    ],
  }));

  static getChartDataPercent = (data: any, nameProperty: any, percentPair: any) => (data || []).map((dataRow: any) => ({
    name: dataRow[nameProperty] || dataRow.name || '',
    value: Utils.getPercent(dataRow[percentPair[0]], dataRow[percentPair[1]]),
    extra: {
      ...dataRow.extra || dataRow,
    }
  }));

  static normalizedToSeriesChart = (data: any, nameProperty: any, seriesProperties: [ string, string ], useLog: boolean = false) => this.objectOfArraysToSeriesChart(this.sortRowsByField(data, nameProperty), seriesProperties, useLog);

  static sortRowsByField = (data: any, nameProperty: any) => {
    return data
      .reduce((acc: any, obj: any) => {
        const nameProp = obj[nameProperty];
        if (!acc[nameProp] && Object.keys(nameProp || {}).length > 0) { acc[nameProp] = [] };
        Object.keys(nameProp || {}).length > 0 && acc[nameProp].push(obj);
        return acc;
      }, {});
  };

  static objectOfArraysToSeriesChart = (objOfArrays: any, seriesProperties: [ string, string ], useLog: boolean = false) => Object.keys(objOfArrays).map((key: any, val: any) => ({
    name: key,
    series: [
      ...(objOfArrays[key] || []).map((dataRow: any) => ({
        name: seriesProperties? dataRow[seriesProperties[0]] : '',
        value: seriesProperties ? (useLog ? Math.log10(dataRow[seriesProperties[1]]) : dataRow[seriesProperties[1]]) : '',
        extra: {
          ...dataRow.extra || dataRow,
        }
      }))
    ]
  }));

  static pruneMultiToLatest = (data: any, numOfRecords: number = 12) => data.slice(-numOfRecords);

  static pruneChartDataToSingleRecord = (data: any, recordName: string) => [data.find((dataObj: any) => dataObj.name === recordName)];

  static getSeverityThresholdText(data: any, thresh1: number = 70, thresh2: number = 80) {
    if (data > thresh2) {
      return 'Exceeded Threshold';
    }
    if (data > thresh1) {
      return 'Approaching Threshold';
    }
    return 'Nominal';
  }
}

