import { HttpErrorResponse } from "@angular/common/http";
import { MONTHS } from '../constants/data-constants';

export class Utils {
  public static formatError(error: HttpErrorResponse): string {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      return 'An error occurred: ' + error.error.message;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      let msg = "Unknown error";
      if (error.error && typeof error.error === 'string') {
        msg = <string>error.error;
      }
      else if (error.message) {
        msg = error.message;
      }
      return `Backend returned code ${error.status}, ${error.error}`;
    }
  };

  static isNumeric = (n: any) => !isNaN(parseFloat(n)) && isFinite(n);

  static isAlphaNumeric = (str: any) => str.match(/[a-zA-Z]/) !== null && str.match(/[0-9]/) !== null;

  static isAlpha = (str: any) => str.match(/[a-zA-Z]/) !== null;

  static parseNum = (n: any) => parseFloat(n.toString().replace(/[^\d.]/g, ""));

  public static capIt = (str: string, lower = false) => (lower ? str.toLowerCase() : str).replace(/(?:^|\s|["'([{])+\S/g, match => match.toUpperCase());

  public static lowStr = (val: any) => val.toString().toLowerCase();

  public static dashString = (str: string) => str.replace(/ /g, '-');

  public static underString = (str: string) => str.replace(/ /g, '_');

  public static spaceString = (str: string) => str.replace(/-/g, ' ').replace(/_/g, ' ');

  public static getPercent = (num: any, den: any) => parseFloat(num)/parseFloat(den)*100;

  public static isObject = (variable: any) => typeof variable === 'object' && variable !== null && !Array.isArray(variable);

  public static formatColumnTitle = (title: string) => this.capIt(this.spaceString(title), true);

  public static formatInteger(value: string) {
    const val = parseInt(value, 10);
    return val > 0 ? '+' + val : val;
  }

  public static formatUSD = (money: number) => !Number.isNaN(money) ? new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(money) : '';

  public static formatLargeNumber = (num: number) => num.toLocaleString();

  public static formatPercent = (number: number) => (+Number(number).toFixed(2) || 0)+'%';

  public static formatBadValue = (val: any) => (val === undefined || val === 'undefined' || Number.isNaN(val) || val === '{}' || val === {} || Utils.isObject(val) || val === '[object Object]') ? '' : val;

  public static formatDateString = (date: any) => {
    const dateString = new Date(date).toLocaleDateString();
    return dateString === 'Invalid Date' ? '' : dateString;
  }

  public static formatMonthYearString = (date: any) => {
    const dateObj = new Date(date);
    return dateObj.toLocaleDateString() === 'Invalid Date' ? date : (MONTHS[dateObj.getMonth()] + ' ' + dateObj.getFullYear());
  }
}
