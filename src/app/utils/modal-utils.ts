export class ModalHelper {

  static formatOptions = (options: any, startText: string = '') => {
    const formatStartText = startText ? startText + ' - ' : '';
    return {
      title: typeof options === 'string' ? formatStartText + options : formatStartText + options.name,
      ...(typeof options !== 'string' && options),
    }
  }

  static handleModalClick(modalService: any, event: any, chartProperties: any) {
    const getDataForLegendClick = chartProperties.results.find((chartEl: any) => chartEl.name === event);
    const modalOptions = ModalHelper.formatOptions(getDataForLegendClick || event);
    if (modalOptions.extra) {
      modalService.openModal({ ...modalOptions, extra: modalOptions.extra, ...( chartProperties.extraDataFormatter && { extraDataFormatter: chartProperties.extraDataFormatter || [] } ) });

      modalService.confirmed().subscribe((confirmed: any) => {
        if (confirmed) {
          
        }
      })
    }
  }

}
