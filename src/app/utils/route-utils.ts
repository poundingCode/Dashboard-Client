import { BASE_ROUTES } from '../constants/routes/route-constants';
import { IRoute } from '../model/IRoute';

export class RouteHelper {
  static getBaseUrl(url: string) {
    return url.split('/')[1];
  }

  static getSecUrl(url: string) {
    return url.split('/')[2] || '';
  }

  static findRouteObject(url: string) {
    return BASE_ROUTES.find((route: IRoute) => route.path === this.getBaseUrl(url))?.children;
  }
}