import { Component, Input, OnInit } from '@angular/core';
import { TableLegendItem } from '../../../model/component/DataTable';
import { Button } from '../../../model/component/Button';

@Component({
  selector: 'app-table-legend',
  templateUrl: './table-legend.component.html',
  styleUrls: ['./table-legend.component.scss']
})
export class TableLegendComponent implements OnInit {

  @Input() legendConfig!: TableLegendItem[];

  legendButtonSettings!: Button[];

  constructor() { }

  ngOnInit(): void {
    this.legendButtonSettings = this.legendConfig.map((legendItem: TableLegendItem) => {
      return {
        buttonLabel: legendItem.legendText,
        hideLabel: true,
        buttonIcon: legendItem.legendIcon,
        buttonIconType: 'filled',
        buttonColor: legendItem.legendColor,
      }
    })
  }

}
