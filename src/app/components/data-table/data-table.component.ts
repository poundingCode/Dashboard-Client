import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TableColumn } from 'src/app/model/component/DataTable';
import { MatTableDataSource } from '@angular/material/table';
import { APIService } from '../../services/api.service';
import { FileDownloadService } from '../../services/file-download.service';
import { TableHelper } from '../../utils/table-utils';
import { Utils } from '../../utils/utils';
import { TableData, TableLegendItem } from '../../model/component/DataTable';
import { MatTableFilter } from 'mat-table-filter';
import { TableDataService } from '../../services/table-data.service';
import { ThemeColor } from 'src/app/model/Theme';
import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {

  @Input() tableTitle: string = '';
  @Input() staticTableData!: Array<any>;
  @Input() tableResourceUrl!: any;
  @Input() tableDataType: any = {};
  @Input() columnFormatter!: TableColumn[];
  @Input() customDisplayedColumns!: string[];
  @Input() noService: boolean = false;
  @Input() tableLegend!: TableLegendItem[];
  @Input() linkUrl!: string | undefined;
  @Input() exportAllUrl!: string | undefined;
  @Input() panelIcon!: string | undefined;
  @Input() panelIconColor!: ThemeColor;

  @ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  isLoading: boolean = true;
  dataSource = new MatTableDataSource<Array<any>>();
  tableColumns!: TableColumn[];
  displayedColumns!: string[];
  filterEntity!: any;
  filterType!: MatTableFilter;
  propertyOptions!: any;

  selectedFilterValues: {[k: string]: any} = {};

  constructor(
    private _apiService: APIService<TableData>,
    private _fileDownloadService: FileDownloadService,
    private _tableDataService: TableDataService,
    private _modalService: ModalService,
  ) {}

  ngOnInit() {
    if (this.tableResourceUrl) {
      this._apiService.getAll(this.tableResourceUrl).subscribe({
        next: (response) => {
          this.dataSource = new MatTableDataSource<Array<any>>(TableHelper.formatTableData(response, this.columnFormatter));
          this.setTableProperties();
        },
        error: (error) => {
          console.log(error);
          console.log(this.dataSource.data);
          this.isLoading = false;
        }
      });
    }
    if(this.staticTableData) {
      this.dataSource = new MatTableDataSource<Array<any>>(TableHelper.formatTableData(this.staticTableData, this.columnFormatter));
      this.setTableProperties();
    }
  }

  get tableHasLoadedSuccessfully() {
    return !this.isLoading && this.dataSource.data.length > 0;
  }

  setTableProperties() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = (data: any, header: any) => (Utils.isAlphaNumeric(data[header]) || Utils.isAlpha(data[header])) ? data[header] : Utils.parseNum(data[header]);
    this.isLoading = false;
    this.tableColumns = this.mapTableColumns();
    this.displayedColumns = this.mapDisplayedColumns();
    this.filterEntity = new this.tableDataType();
    this.filterType = MatTableFilter.ANYWHERE;
    !this.noService && this.dataSource.connect().subscribe(() => this.storeFilteredTableData());
    this.propertyOptions = this.setColumnFilters();
  }

  setColumnFilters() {
    let columnFilters: any = {};
    this.tableColumns.forEach((tableColumn: TableColumn) => {
      const { useFilterDropdown, filterOptions, filterProperty, filterType, dataValue } = tableColumn;
      const hasDropdown = filterOptions || useFilterDropdown;
      if (hasDropdown) {
        if (filterType === 'EQUALS' || !filterType) {
          columnFilters[dataValue] = (fieldValue: string) => filterProperty ? Utils.lowStr(fieldValue[filterProperty]) === Utils.lowStr(this.filterEntity[dataValue]) : Utils.lowStr(fieldValue) === Utils.lowStr(this.filterEntity[dataValue]);
        }
        if (filterType === 'ANYWHERE') {
          columnFilters[dataValue] = (fieldValue: string) => filterProperty ? Utils.lowStr(fieldValue[filterProperty]).includes(Utils.lowStr(this.filterEntity[dataValue])) : Utils.lowStr(fieldValue).includes(Utils.lowStr(this.filterEntity[dataValue]));
        }
      }
      if (filterType && !hasDropdown) {
        columnFilters[dataValue] = { filterType: MatTableFilter[filterType] };
      }
    });
    return columnFilters;
  }

  getDefaultTableColumnsFromData = (): string[] => Object.keys(this.dataSource.data[0] || []);

  mapTableColumns = (): TableColumn[] => this.columnFormatter || this.getDefaultTableColumnsFromData().map((columnName) => ({ 'dataValue': columnName, 'headerLabel': this.getColumnTitle(columnName) }));

  mapDisplayedColumns = (): string[] => this.customDisplayedColumns || this.getDefaultTableColumnsFromData();

  getColumnTitle = (str: string) => Utils.formatColumnTitle(str);

  getColumnActions = (actions: any, row: any) => !!actions ? actions(row) : null;

  formatFilterLabel = (column: any) => 'Filter by '+(column.headerLabel || this.getColumnTitle(column.dataValue));

  formatFilterOptions = (column: any) => column.filterOptions ? column.filterOptions : TableHelper.getUniqueColumnValuesForFilterDropdown(this.dataSource.data, column.dataValue, '');

  formatCellData = (cell: any) => Utils.formatBadValue(cell);

  openModal = (action: any) => {
    const { buttonLabel } = action;
    const modalOptions = {
      title: buttonLabel,
      ...action,
    }
    this._modalService.openModal(modalOptions);

    this._modalService.confirmed().subscribe((confirmed: any) => {
      if (confirmed) {

      }
    })
  }

  alignHeader(column: TableColumn) {
    if (column?.alignContent) {
      return column.alignContent === 'right' ? 'center' : column.alignContent;
    }
    return '';
  }

  storeFilteredTableData() {
    this._tableDataService.storeTableData(this.dataSource.filteredData);    
  }

  get hasTableBeenFiltered() {
    const { data, filteredData } = this.dataSource;
    return data.length === filteredData.length;
  }

  get exportBtnTooltip() {
    if (this.exportAllUrl) {
      return this.hasTableBeenFiltered ? 'Download the data displayed in the table below (all pages).' : 'Download the filtered data displayed in the table below (all pages).'
    }
    return '';
  }

  trackTooltips = (index: number, button: any) => button.buttonLabel;

  exportAll = () => {
    if (this.exportAllUrl) {
      this._fileDownloadService.getFile(this.exportAllUrl, {observe: 'response', responseType: 'text/plain'}).subscribe({
        next: (response: any) => {
          TableHelper.downloadCSVFile(response.body, this.tableTitle);
        },
        error: (error: any) => {
          console.log(error);
        }
      })
    }
  }

  onChangePage(e: PageEvent) {
    // console.log(e.pageIndex);
    // console.log(e.pageSize);
  }

  get tableExportFileName() {
    return this.tableTitle + ' Table Export';
  }

  get tableExportSheetName() {
    return this.tableTitle.substring(0, 30);
  }
}
