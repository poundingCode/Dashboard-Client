import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ModalSettings } from 'src/app/model/component/Modal';
import { Utils } from 'src/app/utils/utils';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  isLoading = true;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ModalSettings,
    private _modalDialogRef: MatDialogRef<ModalComponent>
  ) {}

  ngOnInit() {
    if (this.data.modalContentResourceUrl) {
      
    } else {
      this.isLoading = false;
    }
  }

  get extraDataFormatter() {
    return this.data.extraDataFormatter;
  }

  public cancel() {
    this.closeModal(false);
  }

  public closeModal(value: boolean) {
    this._modalDialogRef.close(value);
  }

  public confirm() {
    this.closeModal(true);
  }

  @HostListener("keydown.esc")
    public onEsc() {
      this.closeModal(false);
    }
  
  public formatTitle = (str: any) => Utils.formatColumnTitle(str);
}
