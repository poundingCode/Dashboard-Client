import { Component, Input, OnInit } from '@angular/core';
import { DynamicDashboardElement } from '../../model/component/DynamicDashboard';
import { TableDataService } from '../../services/table-data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dynamic-dashboard',
  templateUrl: './dynamic-dashboard.component.html',
  styleUrls: ['./dynamic-dashboard.component.scss'],
  providers: [TableDataService]
})
export class DynamicDashboardComponent implements OnInit {

  @Input() dashboardTitle!: string;
  @Input() dashboardElements!: DynamicDashboardElement[];

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.dashboardTitle = data?.dashboardConfig?.title || '';
      this.dashboardElements = data?.dashboardConfig?.elements;
    });
  }
}
