import { Component, Input, OnInit } from '@angular/core';
import { Utils } from 'src/app/utils/utils';
import { ChartHelper } from '../../utils/chart-utils';

@Component({
  selector: 'app-chart-tooltip',
  templateUrl: './chart-tooltip.component.html',
  styleUrls: ['./chart-tooltip.component.scss'],
})
export class ChartTooltipComponent implements OnInit {

  @Input() model!: any;
  @Input() type!: any;

  constructor() {}

  ngOnInit(): void {}

  formatPercent = (val: any) => Utils.formatPercent(val);

  formatUSD = (val: any) => Utils.formatUSD(val);

  getSeverityThresholdText = (val: any) => ChartHelper.getSeverityThresholdText(val);

  formatMonthYearString = (val: any) => Utils.formatMonthYearString(val);

  formatLabel = (model: any, labelType: string) => {
    return (model.series ? {
      'default': this.formatMonthYearString(model.series) + ' • ',
    } [labelType || 'default'] : '') + model.name;
  }

  formatValue = (model: any, valueType: string) => {
    return {
      'usd': this.formatUSD(model.value),
      'logUSD': this.formatUSD(Math.pow(10, model.value)),
      'percent': this.formatPercent(model.value),
      'logPercent': this.formatPercent(Math.pow(10, model.value)),
      'default': model.value,
    }[valueType || 'default'];
  }
}
