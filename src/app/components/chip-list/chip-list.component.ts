import { Component, Input, OnInit } from '@angular/core';
import { Chip } from '../../model/component/Chip';

@Component({
  selector: 'app-chip-list',
  templateUrl: './chip-list.component.html',
  styleUrls: ['./chip-list.component.scss']
})
export class ChipListComponent implements OnInit {

  @Input() chipListConfig!: Chip[];

  constructor() { }

  ngOnInit(): void {
  }

}
