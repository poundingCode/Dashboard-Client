import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicDashboardElementComponent } from './dynamic-dashboard-element.component';

describe('DynamicDashboardElementComponent', () => {
  let component: DynamicDashboardElementComponent;
  let fixture: ComponentFixture<DynamicDashboardElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DynamicDashboardElementComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DynamicDashboardElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
