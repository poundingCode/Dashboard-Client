import { Component, OnInit, Input } from '@angular/core';
import { DynamicDashboardElement } from '../../model/component/DynamicDashboard';

@Component({
  selector: 'app-dynamic-dashboard-element',
  templateUrl: './dynamic-dashboard-element.component.html',
  styleUrls: ['./dynamic-dashboard-element.component.scss']
})
export class DynamicDashboardElementComponent implements OnInit {

  @Input() element!: DynamicDashboardElement;

  panelProps: any = {};

  constructor() { }

  ngOnInit(): void {
  }

  getPanelProps(elementProps: any) {
    const { title, linkUrl, panelIcon, panelIconColor } = this.element.elementInputs[elementProps] || {};
    return { title: title || '', linkUrl, panelIcon, panelIconColor };
  }
}
