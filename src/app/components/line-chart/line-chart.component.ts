import { Component, Input, OnInit } from '@angular/core';
import { APIService } from 'src/app/services/api.service';
import { LineChartOptions } from 'src/app/model/component/LineChart';
import { ScaleType } from '@swimlane/ngx-charts';
import { DATA_VIZ_PALETTE } from '../../constants/ui-constants';
import { ModalService } from '../../services/modal.service';
import { ModalHelper } from '../../utils/modal-utils';
// import { ChartHelper } from 'src/app/utils/chart-utils';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit {

  @Input() lineChartResourceUrl!: string;
  @Input() lineChartOptions!: LineChartOptions;

  lineChartProperties!: LineChartOptions;

  isLoading: boolean = true;

  globalLineProps: any = {
    showGridLines: false,
  };

  constructor(
    private _apiService: APIService<any>,
    private _modalService: ModalService
  ) {}

  ngOnInit() {
    if (this.lineChartResourceUrl) {
      this._apiService.getAll(this.lineChartResourceUrl).subscribe({
        next: (response: any) => {
          // this.lineChartProperties = this.setLineChartOptions(ChartHelper.processSimpleChartData(response, this.lineChartOptions.dataNameProperty, this.lineChartOptions.dataValueProperty));
          this.lineChartProperties = this.setLineChartOptions(response);
          this.isLoading = false;
        },
        error: (error: any) => {
          console.log(error);
          this.isLoading = false;
        }
      })
    }
    else if (this.lineChartOptions) {
      this.lineChartProperties = this.setLineChartOptions(null);
      this.isLoading = false;
    }
  }

  setLineChartOptions(data: any) {
    const {
      title,
      results,
      colorScheme,
    } = this.lineChartOptions;
    return {
      ...this.lineChartOptions,
      title: title || '',
      results: data || results || [],
      colorScheme: colorScheme || {
        name: 'scheme',
        selectable: false,
        group: ScaleType.Linear,
        domain: DATA_VIZ_PALETTE,
      },
    };
  }

  onSelect(event: any) {
    ModalHelper.handleModalClick(this._modalService, event, this.lineChartProperties);
  }
}
