import { Component, OnInit } from '@angular/core';
import { SideNavService } from './sidenav.service';
import { RouteService } from '../../services/route.service';
import { IRoute } from '../../model/IRoute';
import { onSideNavChange, animateText } from './side-nav.animations';

const isIframe = window !== window.parent && !window.opener;

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss'],
  animations: [ onSideNavChange, animateText ]
})
export class SideNavComponent implements OnInit {

  isIframe = isIframe;

  public isOpen: boolean = false;
  public showTitle: boolean = false;

  public sideNavChange: boolean = false;
  public sideNavRoutes: IRoute[] | undefined = [];
  public rootRoute!: string;
  public secondaryRoute!: string;

  constructor(
    private _sideNavService: SideNavService,
    private _routeService: RouteService,
  ) {
    this._sideNavService.sideNavState$.subscribe(res => {
      this.sideNavChange = res;
    })
    this._routeService.routeState$.subscribe(res => {
      this.sideNavRoutes = res;
    })
    this._routeService.rootRoute$.subscribe(res => {
      this.rootRoute = res;
    })
    this._routeService.secondaryRoute$.subscribe(res => {
      this.secondaryRoute = res;
    })
  }

  ngOnInit() {}

  onSideNavToggle(toggle: boolean) {
    this.isOpen = toggle;
    
    setTimeout(() => {
      this.showTitle = this.isOpen;
    }, 200)
    this._sideNavService.sideNavState$.next(this.isOpen)
  }

  activeExpansionPanel(expRoute: string) {
    return expRoute === this.secondaryRoute ? 'sidenav-link-active' : '';
  }
}
