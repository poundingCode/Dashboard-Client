import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-dash-login',
  templateUrl: './dash-login.component.html',
  styleUrls: ['./dash-login.component.scss']
})
export class DashLoginComponent implements OnInit {
  isLoggedIn = false;

  constructor(
    private _appComponent: AppComponent,
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = this._appComponent.isLoggedIn;
  }

  login() {
    this._appComponent.login();
  }

}
