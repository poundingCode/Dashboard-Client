import { Component, Input, OnInit } from '@angular/core';
import { APIService } from 'src/app/services/api.service';
import { BarChartOptions } from 'src/app/model/component/BarChart';
import { ScaleType } from '@swimlane/ngx-charts';
import { DATA_VIZ_PALETTE } from '../../constants/ui-constants';
import { ChartHelper } from 'src/app/utils/chart-utils';
import { ModalService } from '../../services/modal.service';
import { ModalHelper } from '../../utils/modal-utils';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit {

  @Input() barChartResourceUrl!: string;
  @Input() barChartOptions!: BarChartOptions;

  barChartProperties!: BarChartOptions;

  isLoading: boolean = true;

  globalBarProps: any;

  constructor (
    private _apiService: APIService<any>,
    private _modalService: ModalService,
  ) {}


  ngOnInit() {
    if (this.barChartResourceUrl) {
      this._apiService.getAll(this.barChartResourceUrl).subscribe({
        next: (response: any) => {
          this.barChartProperties = this.setBarChartOptions(this.processDataByChartType(response));
          this.isLoading = false;
        },
        error: (error: any) => {
          console.log(error);
          this.isLoading = false;
        }
      })
    }
    else if (this.barChartOptions) {
      this.barChartProperties = this.setBarChartOptions(this.processDataByChartType(this.barChartOptions?.results || null));
      this.isLoading = false;
    }
  }

  setBarChartOptions(data: any) {
    this.globalBarProps = {
      showGridLines: false,
      roundEdges: false,
      barPadding: this.barChartOptions?.chartType?.includes('vert') ? 10 : 3,
      groupPadding: 10,
    };

    const {
      title,
      results,
      colorScheme,
    } = this.barChartOptions;
    return {
      ...this.barChartOptions,
      title: title || '',
      results: data || results || [],
      colorScheme: colorScheme || {
        name: 'scheme',
        selectable: false,
        group: ScaleType.Linear,
        domain: DATA_VIZ_PALETTE,
      },
    };
  }

  processDataByChartType = (data: any) => {
    const { chartType, dataNameProperty, dataValueProperty, dataPercentPair, dataSeriesProperties, useLog } = this.barChartOptions || {};
    return {
      'vert': () => dataPercentPair ? ChartHelper.getChartDataPercent(data, dataNameProperty, dataPercentPair) : ChartHelper.processSimpleChartData(data, dataNameProperty, dataValueProperty, useLog),
      'horiz': () => dataPercentPair ? ChartHelper.getChartDataPercent(data, dataNameProperty, dataPercentPair) : ChartHelper.processSimpleChartData(data, dataNameProperty, dataValueProperty, useLog),
      'vertMulti': () => (dataNameProperty && dataSeriesProperties) ? ChartHelper.normalizedToSeriesChart(ChartHelper.pruneMultiToLatest(data), dataNameProperty, dataSeriesProperties, useLog) : ChartHelper.processSeriesChartData(ChartHelper.pruneMultiToLatest(data), useLog),
      'horizMulti': () => (dataNameProperty && dataSeriesProperties) ? ChartHelper.normalizedToSeriesChart(ChartHelper.pruneMultiToLatest(data), dataNameProperty, dataSeriesProperties, useLog) : ChartHelper.processSeriesChartData(ChartHelper.pruneMultiToLatest(data), useLog),
      'vertStacked': () => (dataNameProperty && dataSeriesProperties) ? ChartHelper.normalizedToSeriesChart(data, dataNameProperty, dataSeriesProperties, useLog) : ChartHelper.processSeriesChartData(data, useLog),
      'horizStacked': () => (dataNameProperty && dataSeriesProperties) ? ChartHelper.normalizedToSeriesChart(data, dataNameProperty, dataSeriesProperties, useLog) : ChartHelper.processSeriesChartData(data, useLog),
      'default': () => data,
    }[chartType || 'default']();
  };

  onSelect(event: any) {
    ModalHelper.handleModalClick(this._modalService, event, this.barChartProperties);
  }
}
