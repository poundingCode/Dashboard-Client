import { Component, OnInit, Input } from '@angular/core';
import { NO_RECORDS_FOUND } from 'src/app/constants/data-constants';
import { TileCard } from '../../model/component/TileCard';
import { TileHelper } from '../../utils/tile-utils';
import { Button } from '../../model/component/Button';
import { Utils } from '../../utils/utils';

@Component({
  selector: 'app-tile-card',
  templateUrl: './tile-card.component.html',
  styleUrls: ['./tile-card.component.scss']
})
export class TileCardComponent implements OnInit {

  @Input() cardData!: TileCard;

  isLoading: boolean = true;

  constructor() { }

  ngOnInit() {
    if(this.cardData) {
      this.isLoading = false;
    }
  }

  get cardClass() {
    return `tile-card${this.cardData.cardType ? ` ${this.cardData.cardType}` : ''}`
  }

  get cardCurrentValue() {
    const { numType, currentValue: { displayValue } } = this.cardData;
    return `${TileHelper.abbreviateNumber(displayValue, numType)}`;
  }

  get cardPrevValue() {
    const { numType, prevValue: { displayValue } = {} } = this.cardData;
    if (displayValue) {
      return `${TileHelper.abbreviateNumber(displayValue, numType)}`;
    }
    return '';
  }

  get currentPeriod() {
    return this.cardData.currentValue?.periodLabel || '';
  }

  get previousPeriod() {
    return this.cardData?.prevValue?.periodLabel ? 'vs. '+this.cardData?.prevValue?.periodLabel : '';
  }

  get trendPercent() {
    const { currentValue: { displayValue: curVal }, prevValue: { displayValue: prevVal } = {} } = this.cardData;
    if (curVal && prevVal && parseFloat(prevVal) > 0) {
      const getPercent = Math.round(((parseFloat(curVal) - parseFloat(prevVal)) / parseFloat(prevVal)) * 100);
      return Number.isNaN(getPercent) ? 0 : getPercent;
    }
    return 0;
  }

  get trendDirection() {
    if(this.trendPercent === 0) {
      return 'flat';
    }
    if (this.trendPercent) {
      return this.trendPercent >= 0 ? 'up' : 'down';
    }
    return '';
  }

  get trendDisposition() {
    if (this.trendPercent === 0) {
      return 'flat';
    }
    if (this.cardData.invertTrend) {
      return this.trendPercent >= 0 ? 'bad' : 'good';
    }
    return this.trendPercent >= 0 ? 'good' : 'bad';
  }

  get trendIcon() {
    return 'trending_'+this.trendDirection;
  }

  formatTrend = (val: any) => parseInt(val) === 0 ? '0%' : Utils.formatInteger(val) + '%';

  get notFoundBtnData(): Button {
    return {
      buttonIconType: 'outlined',
      buttonLabel: NO_RECORDS_FOUND,
      hideLabel: true,
      buttonIcon: 'info_circle',
      buttonColor: 'light',
      tooltipBackground: 'neutral'
    }
  }
}
