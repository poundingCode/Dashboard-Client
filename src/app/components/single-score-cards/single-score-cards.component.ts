import { Component, Input, OnInit } from '@angular/core';
import { SingleScoreCard } from '../../model/component/SingleScoreCards';

@Component({
  selector: 'app-single-score-cards',
  templateUrl: './single-score-cards.component.html',
  styleUrls: ['./single-score-cards.component.scss']
})
export class SingleScoreCardsComponent implements OnInit {

  @Input() scoreCardsData: SingleScoreCard[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
