import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleScoreCardsComponent } from './single-score-cards.component';

describe('SingleScoreCardsComponent', () => {
  let component: SingleScoreCardsComponent;
  let fixture: ComponentFixture<SingleScoreCardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleScoreCardsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SingleScoreCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
