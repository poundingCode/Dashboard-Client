import { Component, Input, OnInit } from '@angular/core';
import { ThemeColor } from 'src/app/model/Theme';

@Component({
  selector: 'app-expandable-panel',
  templateUrl: './expandable-panel.component.html',
  styleUrls: ['./expandable-panel.component.scss']
})
export class ExpandablePanelComponent implements OnInit {

  @Input() panelTitle: string = '';
  @Input() panelLinkUrl!: string | undefined;
  @Input() panelIcon!: string | undefined;
  @Input() panelIconColor!: ThemeColor;

  constructor() { }

  ngOnInit(): void {
  }

}
