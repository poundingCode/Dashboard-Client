import { Component, Input, OnInit } from '@angular/core';
import { APIService } from '../../services/api.service';
import { forkJoin, Observable } from 'rxjs';

@Component({
  selector: 'app-html-container',
  templateUrl: './html-container.component.html',
  styleUrls: ['./html-container.component.scss']
})
export class HtmlContainerComponent implements OnInit {

  @Input() htmlSource!: any;
  @Input() sourceUrls!: Array<string>;

  resourceData: Array<any> = [];
  srcResponses: Array<any> = [];

  isLoading: boolean = true;

  constructor(private _apiService: APIService<any>) { }

  ngOnInit(): void {
    if (this.sourceUrls) {
      this.requestAllDataSources().subscribe({
        next: (responses: any) => {
          responses.forEach((res: any, i: any) => {
            this.resourceData.push(res.result);
          })
          this.isLoading = false;
        },
        error: (error) => {
          console.log(error);
          this.isLoading = false;
        }
      });
    }
    this.isLoading = false;
  }

  requestAllDataSources(): Observable<any> {
    this.sourceUrls.forEach((src: any, i: number) => {
      this.srcResponses.push(this._apiService.getAll(src));
    })
    return forkJoin([...this.srcResponses]);
  }
}
