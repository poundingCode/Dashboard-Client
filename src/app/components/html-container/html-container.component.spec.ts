import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlContainerComponent } from './html-container.component';

describe('HtmlContainerComponent', () => {
  let component: HtmlContainerComponent;
  let fixture: ComponentFixture<HtmlContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HtmlContainerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HtmlContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
