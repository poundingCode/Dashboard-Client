import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalResourceLayoutComponent } from './external-resource-layout.component';

describe('ExternalResourceLayoutComponent', () => {
  let component: ExternalResourceLayoutComponent;
  let fixture: ComponentFixture<ExternalResourceLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExternalResourceLayoutComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExternalResourceLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
