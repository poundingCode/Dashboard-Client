import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Button } from 'src/app/model/component/Button';

@Component({
  selector: 'app-external-resource-layout',
  templateUrl: './external-resource-layout.component.html',
  styleUrls: ['./external-resource-layout.component.scss']
})
export class ExternalResourceLayoutComponent implements OnInit {

  @Input() resourceTitle!: string;
  @Input() logoImage!: string;
  @Input() resourceSummary!: string;
  @Input() bulletedList!: Array<any>;
  @Input() envMessage!: string;
  @Input() resourceUrl!: string;

  resourceButtonSettings!: Button;

  @ViewChild('resourceSummary') el!:ElementRef;

  constructor() { }

  ngOnInit(): void {
    this.resourceButtonSettings = {
      buttonType: 'flat',
      buttonLabel: `Access ${this.resourceTitle}`,
      buttonColor: 'btnblue',
      buttonAction: this.resourceUrl,
    }
  }
  
  ngAfterViewInit() {
    this.el.nativeElement.innerHTML = this.resourceSummary;
  }

}
