import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-img-container',
  templateUrl: './img-container.component.html',
  styleUrls: ['./img-container.component.scss']
})
export class ImgContainerComponent implements OnInit {

  @Input() imgTitle!: string;
  @Input() sourceUrl!: string;
  @Input() linkUrl!: string | undefined;
  @Input() width!: string;
  @Input() height!: string;
  @Input() buttonLabel!: string;

  constructor() {}

  ngOnInit() {}

}
