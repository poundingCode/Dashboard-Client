import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';
import { APP_TITLE } from '../../constants/app-constants';
import { BASE_ROUTES } from '../../constants/routes/route-constants';
import { IRoute } from '../../model/IRoute';

@Component({
  selector: 'app-dash-navbar',
  templateUrl: './dash-navbar.component.html',
  styleUrls: ['./dash-navbar.component.scss']
})
export class DashNavbarComponent implements OnInit {
  title = APP_TITLE;
  photo = '';
  username = '';
  isLoggedIn = false;

  public appRoutes: IRoute[] = BASE_ROUTES;

  constructor(
    private _appComponent: AppComponent
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = this._appComponent.isLoggedIn;
    this.photo = this._appComponent.photo;
    this.username = this._appComponent.username;
  }

  logout() {
    this._appComponent.logout();
  }

}
