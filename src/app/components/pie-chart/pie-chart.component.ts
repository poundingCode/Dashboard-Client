import { Component, OnInit, Input } from '@angular/core';
import { APIService } from 'src/app/services/api.service';
import { ChartHelper } from '../../utils/chart-utils';
import { LegendPosition, ScaleType } from '@swimlane/ngx-charts';
import { DATA_VIZ_PALETTE } from '../../constants/ui-constants';
import { PieChartOptions } from '../../model/component/PieChart';
import { ModalService } from '../../services/modal.service';
import { ModalHelper } from '../../utils/modal-utils';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit {

  @Input() pieChartResourceUrl!: string;
  @Input() pieChartOptions!: PieChartOptions;

  pieChartProperties!: PieChartOptions;

  isLoading: boolean = true;

  constructor(
    private _apiService: APIService<any>,
    private _modalService: ModalService,
  ) {}

  ngOnInit() {
    if (this.pieChartResourceUrl) {
      this._apiService.getAll(this.pieChartResourceUrl).subscribe({
        next: (response: any) => {
          this.pieChartProperties = this.setPieChartOptions(this.processDataByChartType(response));
          this.isLoading = false;
        },
        error: (error: any) => {
          console.log(error);
          this.isLoading = false;
        }
      })
    }
    else if (this.pieChartOptions) {
      this.pieChartProperties = this.setPieChartOptions(this.processDataByChartType(this.pieChartOptions?.results || null));
      this.isLoading = false;
    }
  }

  setPieChartOptions(data: any) {
    const {
      title,
      results,
      colorScheme,
      legend,
      legendPosition,
      showLabels,
    } = this.pieChartOptions;
    return {
      ...this.pieChartOptions,
      title: title || '',
      results: data || results || [],
      colorScheme: colorScheme || {
        name: 'scheme',
        selectable: false,
        group: ScaleType.Linear,
        domain: DATA_VIZ_PALETTE,
      },
      legend: legend || true,
      legendPosition: legendPosition || LegendPosition.Right,
      showLabels: showLabels || false,
    };
  }

  processDataByChartType = (data: any) => {
    const { chartType, dataNameProperty, dataValueProperty, dataPercentPair, useLog } = this.pieChartOptions || [];
    return {
      'normal': () => ChartHelper.processSimpleChartData(data, dataNameProperty, dataValueProperty, useLog),
      'advanced': () => ChartHelper.processSimpleChartData(data, dataNameProperty, dataValueProperty, useLog),
      'default': () => ChartHelper.processSimpleChartData(data, dataNameProperty, dataValueProperty, useLog),
      'grid': () =>  dataPercentPair ? ChartHelper.getChartDataPercent(data, dataNameProperty, dataPercentPair) : ChartHelper.processSimpleChartData(data, dataNameProperty, dataValueProperty, useLog),
    }[chartType || 'default']();
  }

  onSelect(event: any) {
    ModalHelper.handleModalClick(this._modalService, event, this.pieChartProperties);
  }
}