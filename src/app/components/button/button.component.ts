import { Component, Input, OnInit } from '@angular/core';
import { Button } from '../../model/component/Button';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() buttonSettings!: Button;

  constructor() { }

  ngOnInit(): void {}

  get tooltipClasses() {
    return `${this.buttonSettings.buttonColor || this.buttonSettings.tooltipBackground} btn-tooltip`;
  }

  get iconType() {
    return this.buttonSettings.buttonIconType === 'outlined' ? 'material-icons-outlined' : '';
  }

}
