import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-dash-footer',
  templateUrl: './dash-footer.component.html',
  styleUrls: ['./dash-footer.component.scss']
})
export class DashFooterComponent implements OnInit {

  isLoggedIn = false;

  constructor(
    private _appComponent: AppComponent,
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = this._appComponent.isLoggedIn;
  }

}
