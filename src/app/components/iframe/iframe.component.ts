import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { IFrame } from '../../model/component/Frame';


@Component({
  selector: 'app-iframe',
  templateUrl: './iframe.component.html',
  styleUrls: ['./iframe.component.scss']
})
export class IframeComponent implements OnInit {

  @Input() iFrameData!: IFrame[];

  sanitizedData!: IFrame[];

  constructor(public sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.sanitizedData = this.iFrameData.map((iframe: IFrame) => {
      const urlSrc: string = iframe.sourceUrl;
      return {
        ...iframe,
        safeUrl: this.sanitizer.bypassSecurityTrustResourceUrl(urlSrc),
      }
    })
  }
}
