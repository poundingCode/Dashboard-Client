import { Component, Input, OnInit } from '@angular/core';
import { PROJECTED_ANNUAL_INCREASE_PERCENTAGE } from 'src/app/constants/data-constants';
import { TileCard, ITileValue } from '../../model/component/TileCard';
import { APIService } from '../../services/api.service';
import { TableDataService } from '../../services/table-data.service';
import { TileHelper } from '../../utils/tile-utils';

@Component({
  selector: 'app-tile-row',
  templateUrl: './tile-row.component.html',
  styleUrls: ['./tile-row.component.scss']
})
export class TileRowComponent implements OnInit {
  @Input() tileRowConfig!: TileCard[]; // detailed settings for this tile row
  @Input() tileRowResourceUrl!: string; // endpoint

  tileRowDataSource!: any; // any data fetched from an API
  cardDataSources: Array<any> = [[], [], []]; // array of data (one array element per card) fetched by individual cards in a row
  mappedData!: TileCard[]; // tile row config array sent to the template

  sharedTableData!: Array<any>; // data will be served from a table component (if it exists) on the same dynamic dashboard
  periodFilteredTableData!: Array<any>; // sharedTableData filtered by time period

  constructor(
    private _apiService: APIService<any>,
    private _tableDataService: TableDataService,
  ) { }

  ngOnInit() {
    this._tableDataService.sharedTableData$.subscribe({
      next: (res) => {
        this.sharedTableData = res;
        this.setTileRowDataSources();
      },
      error: (error) => {
        console.log(error);
        this.setTileRowDataSources();
      }
    })
  }

  setTileRowDataSources() {
    if (this.tileRowResourceUrl) {
      this._apiService.getAll(this.tileRowResourceUrl).subscribe({
        next: (response) => {
          this.tileRowDataSource = response;
          this.mappedData = this.mapDataToTileProperties();
        },
        error: (error) => {
          console.log(error);
        }
      })
    }
    else if (this.tileRowConfig) {
      this.mappedData = this.mapDataToTileProperties();
    }
  }

  fetchAndStoreCardDataSource(srcUrl: any, cardLocation: any) {
    if (srcUrl) {
      this._apiService.getAll(srcUrl).subscribe({
        next: (response) => {
          this.cardDataSources[cardLocation] = response;
        },
        error: (error) => {
          console.log(error);
        }
      })
    }
  }

  mapDataToTileProperties() {
    return this.tileRowConfig.map((tileCard: TileCard, cardLocation: number) => {
      const {
        cardResourceUrl = '',
        currentValue,
        prevValue = {},
      } = tileCard;

      const { 
        currentValue: {
          periodType: curPerType,
          period: curPer,
          periodLabel: curLabel,
          showRecordsOf: curShowRecs,
        },
        prevValue: {
          tableColumnName: prevColName = '',
          periodType: prevPerType = '',
          period: prevPer = '',
          periodLabel: prevLabel = '',
          showRecordsOf: prevShowRecs,
        } = {}
      } = tileCard;
      return {
        ...tileCard,
        currentValue: {
          ...currentValue,
          displayValue: this.calculateTileValue(currentValue, cardLocation, cardResourceUrl),
          periodLabel: curLabel ? curLabel : ((curPer && curPerType) ? this.formatLabel(currentValue) : curLabel),
          recordsOf: curShowRecs ? this.getRecordsOf(currentValue) : '',
        },
        prevValue: {
          ...prevValue,
          displayValue: this.calculateTileValue(prevValue, cardLocation, cardResourceUrl),
          periodLabel: prevLabel ? prevLabel : ((prevPer && prevPerType && prevColName) ? this.formatLabel(prevValue) : prevLabel),
          recordsOf: prevShowRecs ? this.getRecordsOf(prevValue) : '',
        },
      }
    });
  }

  calculateTileValue(valueObj: ITileValue, cardLoc: any, srcUrl: string) {
    this.periodFilteredTableData = this.sharedTableData;
    const {
      displayValue,
      dataValue,
      tableColumnName = '',
      tableFilterColumn = '',
      filterValue = '',
      tableColumnPropertyName = '',
      tableColumnPropertyValues = [],
      tableColumnValuesExcludedFromTotal = [],
      tableColumnMath = '',
      period,
      periodType,
      periodTableColumn = '' } = valueObj;
    if (!!displayValue && !dataValue) {
      return displayValue;
    }
    // filter table data by date period (month, quarter, year) and period type (current, previous, year-ago)
    if (TileHelper.tableHasColumn(this.sharedTableData, tableColumnName)) {
      if (TileHelper.tableHasColumn(this.sharedTableData, periodTableColumn) && periodType && period) {
        this.periodFilteredTableData = TileHelper.filterTableRowsByDateRange(this.sharedTableData, periodTableColumn, period, periodType);
      }
      const doTableMath: any = {
        'sum': TileHelper.sumTableColumn(this.periodFilteredTableData, tableColumnName),
        'avg': TileHelper.averageTableColumn(this.periodFilteredTableData, tableColumnName),
        'filteredAvg': TileHelper.filteredAverageTableColumn(this.periodFilteredTableData, tableColumnName, tableFilterColumn, filterValue),
        'percentFilter': TileHelper.getPercentageOfTotalRecordsByFilter(this.periodFilteredTableData, tableColumnName, tableColumnPropertyName, tableColumnPropertyValues, tableColumnValuesExcludedFromTotal),
        'countRecords': TileHelper.countRecordsWithValue(this.periodFilteredTableData, tableColumnName, tableColumnPropertyName, tableColumnPropertyValues),
        'highVal': TileHelper.getHighestValueInColumn(this.periodFilteredTableData, tableColumnName),
        'lowVal': TileHelper.getLowestValueInColumn(this.periodFilteredTableData, tableColumnName),
        'stddev': TileHelper.getStandardDeviation(this.periodFilteredTableData, tableColumnName),
        'highMonth': TileHelper.reportTableColumnValueByMonth(this.periodFilteredTableData, tableColumnName, periodTableColumn, 'highMonth'),
        'lowMonth': TileHelper.reportTableColumnValueByMonth(this.periodFilteredTableData, tableColumnName, periodTableColumn, 'lowMonth'),
        'projected': TileHelper.sumTableColumn(this.periodFilteredTableData, tableColumnName) * (parseFloat('1.' + PROJECTED_ANNUAL_INCREASE_PERCENTAGE)),
      }
      return this.setTileDataValue(doTableMath[tableColumnMath || 'sum'], valueObj, cardLoc, srcUrl);
    }
    return this.setTileDataValue((tableColumnName ? '0' : ''), valueObj, cardLoc, srcUrl);
  }

  setTileDataValue(calculatedValue: any, valueObj: ITileValue, cardLoc: any, srcUrl: string) {
    const { dataValue, dataValueSecond, dataValueMath } = valueObj;
    if (!!dataValue) {
      if(srcUrl) {
        this.fetchAndStoreCardDataSource(srcUrl, cardLoc);
      }
      if(this.cardDataSources[cardLoc][0]) {
        const dataSrc = this.cardDataSources[cardLoc][0];
        const secondValue = (dataValueSecond && dataSrc[dataValueSecond]) || calculatedValue || '';
        if (dataValueMath && !secondValue) {
          const dataValMaths: any = {
            'show': dataSrc[dataValue],
            'projected': dataSrc[dataValue] * (100 + parseInt(PROJECTED_ANNUAL_INCREASE_PERCENTAGE)),
          }
          return dataValueMath ? dataValMaths[dataValueMath] : dataValMaths['show'];
        }
        if (dataValueMath && secondValue) {
          const secondValMaths: any = {
            'subtract': parseInt(dataSrc[dataValue]) - parseInt(secondValue),
            'divideDataValByDisplayVal': ((parseInt(dataSrc[dataValue]) / parseInt(secondValue)) * 100).toFixed(2),
            'divideDisplayValByDataVal': ((parseInt(secondValue) / parseInt(dataSrc[dataValue])) * 100).toFixed(2),
          }
          return secondValMaths[dataValueMath];
        }
        return this.cardDataSources[cardLoc][0][dataValue];
      }
    }
    return calculatedValue;
  }

  formatLabel(valueObj: ITileValue) {
    const { tableColumnName = '', periodTableColumn = '', tableColumnMath = '', period = '', periodType = '' } = valueObj;
    if (tableColumnMath && (tableColumnMath === 'highMonth' || tableColumnMath === 'lowMonth')) {
      return TileHelper.tableColumnValueByMonthLabel(this.periodFilteredTableData, tableColumnName, periodTableColumn, tableColumnMath);
    }
    return TileHelper.formatPeriodLabel(periodType, period, this.sharedTableData, periodTableColumn);
  }

  getRecordsOf(valueObj: any) {
    const { tableColumnName, tableColumnPropertyName, tableColumnPropertyValues, tableColumnValuesExcludedFromTotal } = valueObj;
    return `${TileHelper.sumRecords(TileHelper.filterTableRows(this.periodFilteredTableData, tableColumnName, tableColumnPropertyName, tableColumnPropertyValues))} of ${TileHelper.sumRecords(TileHelper.tableRowExclusionFilter(this.periodFilteredTableData, tableColumnName, tableColumnPropertyName, tableColumnValuesExcludedFromTotal))}`;
  }
}
