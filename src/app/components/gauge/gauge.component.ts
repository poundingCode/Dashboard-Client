import { Component, OnInit, Input } from '@angular/core';
import { APIService } from 'src/app/services/api.service';
import { ChartHelper } from '../../utils/chart-utils';
import { LegendPosition, ScaleType } from '@swimlane/ngx-charts';
import { DATA_VIZ_PALETTE, SEVERITY_LEVEL_PALETTE } from '../../constants/ui-constants';
import { GaugeOptions } from '../../model/component/Gauge';
import { ModalService } from '../../services/modal.service';
import { ModalHelper } from '../../utils/modal-utils';

@Component({
  selector: 'app-gauge',
  templateUrl: './gauge.component.html',
  styleUrls: ['./gauge.component.scss']
})
export class GaugeComponent implements OnInit {

  @Input() gaugeResourceUrl!: string;
  @Input() gaugeOptions!: GaugeOptions;

  gaugeProperties!: GaugeOptions;

  isLoading: boolean = true;

  globalGaugeProps: any;
  
  severityGauge = (name: any) => {
    const value = this.gaugeProperties.results.find((dataPoint) => dataPoint.name === name).value;
    if (value > 70 && value <= 80) {
      return SEVERITY_LEVEL_PALETTE[2];
    }
    if (value > 80) {
      return SEVERITY_LEVEL_PALETTE[4];
    }
    return SEVERITY_LEVEL_PALETTE[0];
  }

  constructor(
    private _apiService: APIService<any>,
    private _modalService: ModalService
  ) {}

  ngOnInit() {
    this.globalGaugeProps = {
      smallSegments: 5,
      bigSegments: 4,
    };
    
    const { singleNameProperty } = this.gaugeOptions;
    if (this.gaugeResourceUrl) {
      this._apiService.getAll(this.gaugeResourceUrl).subscribe({
        next: (response: any) => {
          this.gaugeProperties = singleNameProperty ? this.setgaugeOptions(ChartHelper.pruneChartDataToSingleRecord(response, singleNameProperty)) : this.setgaugeOptions(response);
          this.isLoading = false;
        },
        error: (error: any) => {
          console.log(error);
          this.isLoading = false;
        }
      })
    }
    else if (this.gaugeOptions) {
      this.gaugeProperties = this.setgaugeOptions(null);
      this.isLoading = false;
    }
  }

  setgaugeOptions(data: any) {
    const {
      title,
      results,
      dataNameProperty,
      dataValueProperty,
      colorScheme,
      legendPosition,
      valueFormatting,
      showLabels,
    } = this.gaugeOptions;
    return {
      ...this.gaugeOptions,
      title: title || '',
      results: data || results || [],
      dataNameProperty,
      dataValueProperty,
      colorScheme: colorScheme || {
        name: 'scheme',
        selectable: false,
        group: ScaleType.Linear,
        domain: DATA_VIZ_PALETTE,
      },
      legendPosition: legendPosition || LegendPosition.Right,
      valueFormatting: valueFormatting || null,
      showLabels: showLabels || true,
    };
  }

  onSelect(event: any) {
    ModalHelper.handleModalClick(this._modalService, event, this.gaugeProperties);
  }
}
