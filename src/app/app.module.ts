import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatMenuModule } from '@angular/material/menu';
import { MatChipsModule } from '@angular/material/chips';
import { MatTableExporterModule } from 'mat-table-exporter';
import { MatTableFilterModule } from 'mat-table-filter';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
// for table & pagination
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon'
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// graph components
import { ProfileEditComponent } from './views/users/profile-edit/profile-edit.component';
import { ProfileViewComponent } from './views/users/profile-view/profile-view.component';
import { ProfileService } from './services/profile.service';

// NGX Charts
import { NgxChartsModule } from '@swimlane/ngx-charts';

// RavenVision components
// default
import { AboutComponent } from './about/about.component';

// Views
import { HomeComponent } from './views/general/home.component';
import { ItopsComponent } from './views/itops/itops.component';
import { SecOpsComponent } from './views/secops/secOps.component';
import { PurchasingComponent } from './views/purchasing/purchasing.component';
import { ServOpsComponent } from './views/servops/servOps.component';

import { UsersComponent } from './views/users/users.component'


import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { IPublicClientApplication, PublicClientApplication, InteractionType } from '@azure/msal-browser';
import { MsalGuard, MsalInterceptor, MsalBroadcastService, MsalInterceptorConfiguration, MsalModule, MsalService, MSAL_GUARD_CONFIG, MSAL_INSTANCE, MSAL_INTERCEPTOR_CONFIG, MsalGuardConfiguration, MsalRedirectComponent } from '@azure/msal-angular';
import { msalConfig, loginRequest, protectedResources } from './auth-config';
import { DashNavbarComponent } from './components/dash-navbar/dash-navbar.component';
import { DashLoginComponent } from './components/dash-login/dash-login.component';
import { TileCardComponent } from './components/tile-card/tile-card.component';
import { TileRowComponent } from './components/tile-row/tile-row.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { DashFooterComponent } from './components/dash-footer/dash-footer.component';
import { DataTableComponent } from './components/data-table/data-table.component';
import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';
import { DynamicDashboardComponent } from './components/dynamic-dashboard/dynamic-dashboard.component';
import { DynamicDashboardElementComponent } from './components/dynamic-dashboard-element/dynamic-dashboard-element.component';
import { IframeComponent } from './components/iframe/iframe.component';
import { ImgContainerComponent } from './components/img-container/img-container.component';
import { HtmlContainerComponent } from './components/html-container/html-container.component';
import { ButtonComponent } from './components/button/button.component';
import { ModalComponent } from './components/modal/modal.component';
import { ExternalResourceLayoutComponent } from './components/external-resource-layout/external-resource-layout.component';
import { Utils } from './utils/utils';
import { TableLegendComponent } from './components/data-table/table-legend/table-legend.component';
import { SingleScoreCardsComponent } from './components/single-score-cards/single-score-cards.component';
import { BarChartComponent } from './components/bar-chart/bar-chart.component';
import { ExpandablePanelComponent } from './components/expandable-panel/expandable-panel.component';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';
import { ChartTooltipComponent } from './components/chart-tooltip/chart-tooltip.component';
import { GaugeComponent } from './components/gauge/gauge.component';

//Services
import { ModalService } from './services/modal.service';

// Directives
import { NgxGaugeZeroMarginDirective } from './directives/ngx-charts-gauge.directive';
import { NgxPieZeroMarginDirective } from './directives/ngx-charts-pie.directive';
import { LineChartComponent } from './components/line-chart/line-chart.component';
import { ChipListComponent } from './components/chip-list/chip-list.component';

/**
 * Here we pass the configuration parameters to create an MSAL instance.
 * For more info, visit: https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-angular/docs/v2-docs/configuration.md
 */

export function MSALInstanceFactory(): IPublicClientApplication {
  return new PublicClientApplication(msalConfig);
}

/**
 * MSAL Angular will automatically retrieve tokens for resources 
 * added to protectedResourceMap. For more info, visit: 
 * https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-angular/docs/v2-docs/initialization.md#get-tokens-for-web-api-calls
 */
export function MSALInterceptorConfigFactory(): MsalInterceptorConfiguration {
  const protectedResourceMap = new Map<string, Array<string>>();

  Object.values(protectedResources.azure).forEach((objVal) => {
    Utils.isObject(objVal) && Object.values(objVal).forEach((resValue: any) => {
      protectedResourceMap.set(resValue.endpoint, resValue.scopes);
    })
  });

  return {
    interactionType: InteractionType.Redirect,
    protectedResourceMap
  };
}

/**
 * Set your default interaction type for MSALGuard here. If you have any
 * additional scopes you want the user to consent upon login, add them here as well.
 */
export function MSALGuardConfigFactory(): MsalGuardConfiguration {
  return {
    interactionType: InteractionType.Redirect,
    authRequest: loginRequest
  };
}

@NgModule({
  declarations: [
    AboutComponent,
    AppComponent,
    PurchasingComponent,
    HomeComponent,
    ItopsComponent,
    ProfileViewComponent,
    ProfileEditComponent,
    SecOpsComponent,
    ServOpsComponent,
    UsersComponent,
    DashNavbarComponent,
    DashLoginComponent,
    TileCardComponent,
    TileRowComponent,
    SideNavComponent,
    DashFooterComponent,
    DataTableComponent,
    LoadingSpinnerComponent,
    DynamicDashboardComponent,
    DynamicDashboardElementComponent,
    IframeComponent,
    ImgContainerComponent,
    HtmlContainerComponent,
    ButtonComponent,
    ModalComponent,
    ExternalResourceLayoutComponent,
    TableLegendComponent,
    SingleScoreCardsComponent,
    BarChartComponent,
    ExpandablePanelComponent,
    PieChartComponent,
    ChartTooltipComponent,
    GaugeComponent,
    NgxGaugeZeroMarginDirective,
    NgxPieZeroMarginDirective,
    LineChartComponent,
    ChipListComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatButtonModule,
    MatGridListModule,
    MatToolbarModule,
    MatListModule,
    MatTableModule,
    MatCardModule,
    MatSidenavModule,
    MatInputModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatSelectModule,
    MatSortModule,
    MatTabsModule,
    MatIconModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatChipsModule,
    MatTableExporterModule,
    MatTableFilterModule,
    MatTooltipModule,
    MatDialogModule,
    HttpClientModule,
    FormsModule,
    MsalModule,
    NgxChartsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MsalInterceptor,
      multi: true
    },
    {
      provide: MSAL_INSTANCE,
      useFactory: MSALInstanceFactory
    },
    {
      provide: MSAL_GUARD_CONFIG,
      useFactory: MSALGuardConfigFactory
    },
    {
      provide: MSAL_INTERCEPTOR_CONFIG,
      useFactory: MSALInterceptorConfigFactory
    },
    MsalService,
    MsalGuard,
    MsalBroadcastService,
    ProfileService,
    ModalService,
  ],
  bootstrap: [AppComponent, MsalRedirectComponent]
})
export class AppModule { }
