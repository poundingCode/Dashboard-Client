import { Injectable } from '@angular/core';
import { IRoute } from '../model/IRoute';
import { Event, Router, NavigationEnd } from '@angular/router';
import { RouteHelper } from '../utils/route-utils';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class RouteService {

    public routeState$: BehaviorSubject<IRoute[] | undefined> = new BehaviorSubject(RouteHelper.findRouteObject(this.router.url));

    public rootRoute$: BehaviorSubject<string> = new BehaviorSubject(RouteHelper.getBaseUrl(this.router.url));

    public secondaryRoute$: BehaviorSubject<string> = new BehaviorSubject(RouteHelper.getSecUrl(this.router.url));

    constructor(
        public router: Router
    ) {
      this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.routeState$.next(RouteHelper.findRouteObject(event.url));
        this.rootRoute$.next(RouteHelper.getBaseUrl(this.router.url));
        this.secondaryRoute$.next(RouteHelper.getSecUrl(this.router.url));
      }
    });
    }
}
