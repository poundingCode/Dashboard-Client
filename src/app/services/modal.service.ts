import { Injectable } from '@angular/core';
import { Observable, EMPTY } from 'rxjs';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ModalComponent } from '../components/modal/modal.component';
import { ModalSettings } from '../model/component/Modal';
import { map, take } from 'rxjs/operators';

@Injectable()
  export class ModalService {
    constructor(
      private modal: MatDialog
    ) {}

    modalRef!: MatDialogRef<ModalComponent>;

  public openModal(options: ModalSettings) {
    const { title = '' } = options;
    this.modalRef = this.modal.open(ModalComponent, {
      data: {
        ...options,
        title,
      }
    })
  }

  public confirmed = (): Observable<any> => this.modalRef.afterClosed().pipe(take(1), map(res => res));

}