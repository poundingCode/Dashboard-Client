import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export abstract class APIService<T> {

  constructor(private http: HttpClient) { }

  public getAll(apiUrl: string): Observable<T[]> { 
    return this.http.get<T[]>(apiUrl);
  }

  public get(apiUrl: string, id: number): Observable<T> { 
    return this.http.get<T>(apiUrl + '/' +  id);
  }
  
  public post(apiUrl: string, resource: Partial<T>): Observable<T> { 
    return this.http.post<T>(apiUrl, resource);
  }

  public delete(apiUrl: string, id: number): Observable<void> {
    return this.http.delete<void>(apiUrl + '/' + id);
  }

  public edit(apiUrl: string, resource: Partial<{ id: string }>): Observable<T> { 
    return this.http.put<T>(apiUrl + '/' + resource.id, resource);
  }
}