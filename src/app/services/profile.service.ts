import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { IProfile } from '../profile';
import { protectedResources } from '../auth-config';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  url = protectedResources.azure.user.profile.endpoint;
  photo = protectedResources.azure.user.photo.endpoint;

  constructor(private http: HttpClient) { }

  getProfile(id: string) { 
    return this.http.get<IProfile>(this.url + '/' +  id);
  }

  getPhotoUri(id: string){
    console.info("get photo");
    var uri = this.photo + '/' +  id;
     return uri;
  }
  
  postProfile(profile: IProfile) { 
    return this.http.post<IProfile>(this.url, profile);
  }

  editProfile(profile: IProfile) { 
    return this.http.put<IProfile>(this.url + '/' + profile.id, profile);
  }
}
