import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class TableDataService {

  public sharedTableData$: Subject<Array<any>> = new Subject();
  
  constructor() {}

  storeTableData(data: any) {
    this.sharedTableData$.next(data);
  }
}