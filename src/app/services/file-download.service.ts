import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export abstract class FileDownloadService {

  constructor(private http: HttpClient) { }

  public getFile(apiUrl: string, options: any): Observable<any> { 
    return this.http.get<any>(apiUrl, options);
  }
}