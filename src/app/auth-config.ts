/**
 * This file contains authentication parameters. Contents of this file
 * is roughly the same across other MSAL.js libraries. These parameters
 * are used to initialize Angular and MSAL Angular configurations in
 * in app.module.ts file.
 */

import { LogLevel, Configuration, BrowserCacheLocation } from '@azure/msal-browser';
import { Constants } from './constants';
const isIE = window.navigator.userAgent.indexOf("MSIE ") > -1 || window.navigator.userAgent.indexOf("Trident/") > -1;

/**
 * Configuration object to be passed to MSAL instance on creation. 
 * For a full list of MSAL.js configuration parameters, visit:
 * https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-browser/docs/configuration.md 
 */
export const msalConfig: Configuration = {
  auth: {
    clientId: Constants.msalConfig.auth.clientId, // This is the ONLY mandatory field that you need to supply.
    authority: Constants.msalConfig.auth.authority, // Defaults to "https://login.microsoftonline.com/common"
    redirectUri: Constants.msalConfig.auth.redirectUri, // Points to window.location.origin. You must register this URI on Azure portal/App Registration.
  },
  cache: {
    cacheLocation: BrowserCacheLocation.SessionStorage, // Configures cache location. "SessionStorage" is more secure, but "LocalStorage" gives you SSO between tabs.
    storeAuthStateInCookie: isIE, // Set this to "true" if you are having issues on IE11 or Edge
  },
  system: {
    loggerOptions: {
      loggerCallback(logLevel: LogLevel, message: string) {
        // console.log(message);
      },
      logLevel: LogLevel.Verbose,
      piiLoggingEnabled: false
    }
  }
}

/**
 * Add here the endpoints and scopes when obtaining an access token for protected web APIs. For more information, see:
 * https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-browser/docs/resources-and-scopes.md
 */
// update MSALInterceptorConfigFactory in app.Module to map url w/scope auto magically...
export const protectedResources = {
  azure: {
    graph: {
      todo: { endpoint: Constants.apiRoot + "api/graph/TodoTask/TodoTaskList", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      users: { endpoint: Constants.apiRoot + "api/graph/user/users", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      sharepoint: { endpoint: Constants.apiRoot + "api/graph/sharepoint", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] }
    },
    user: {
      profile: { endpoint: Constants.apiRoot + "api/profile", scopes: [Constants.ApplicationIdUri + '.default'] },
      photo: { endpoint: Constants.apiRoot + "api/photo", scopes: [Constants.ApplicationIdUri + '.default'] },
      task: { endpoint: Constants.apiRoot + "api/task", scopes: [Constants.ApplicationIdUri + '.default'] },
    },
    general: {
      IncidentNotifications: { endpoint: Constants.apiRoot + "api/security/Incident/All", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user]},
    },
    purchasing: {
      // Purchasing
      ContractLevelSummaryCosts: { endpoint: Constants.apiRoot + "api/purchasing/ContractLevelSummaryCosts", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user]},
      TaskOrderPurchasing: { endpoint: Constants.apiRoot + "api/purchasing/TaskOrderPurchasing", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user]},
      TaskOrderPurchasingByMonth: { endpoint: Constants.apiRoot + "api/purchasing/TaskOrderPurchasing/ByMonth/Chart", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user]},
      LaborHoursOverall: { endpoint: Constants.apiRoot + "api/purchasing/LaborHours/Overall", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      LaborHoursEverything: { endpoint: Constants.apiRoot + "api/purchasing/LaborHours/Everything", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      CAGR: { endpoint: Constants.apiRoot + "api/purchasing/TotalContractCAGR", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      TaskOrderSpendingVsCeiling: { endpoint: Constants.apiRoot + "api/purchasing/TaskOrderSpendingVsCeiling", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      TaskOrderSpendingVsCeilingChart: { endpoint: Constants.apiRoot + "api/purchasing/TaskOrderSpendingVsCeiling/Chart", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      ResourceCostsHCE: { endpoint: Constants.apiRoot + "api/purchasing/Resources/Monthly/HCE", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      ResourceCostsCSP: { endpoint: Constants.apiRoot + "api/purchasing/Resources/Monthly/CSP", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      SpaceBasedCosts: { endpoint: Constants.apiRoot + "api/Purchasing/SpaceBased/Overall", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user]},
      ResourceCostsHCEChart: { endpoint: Constants.apiRoot + "api/purchasing/Resources/Monthly/HCE/Chart", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      ResourceCostsCSPChart: { endpoint: Constants.apiRoot + "api/purchasing/Resources/Monthly/CSP/Chart", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      ContractSpendingVsCeiling: { endpoint: Constants.apiRoot + "api/purchasing/ContractSpendingVsCeiling", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      LaborHoursByMonth: { endpoint: Constants.apiRoot + "api/purchasing/LaborHours/ByMonth", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      // Total Purchasing
      ContractPurchasingByServiceCategory: { endpoint: Constants.apiRoot + "api/purchasing/TotalPurchasing/ByIdiqClinCategory", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user]},
      TotalPurchasingByTaskOrder: { endpoint: Constants.apiRoot + "api/purchasing/TotalPurchasing/TaskOrder", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      TotalPurchasingByTaskOrderAndServiceCategory: { endpoint: Constants.apiRoot + "api/purchasing/TotalPurchasing/ByTaskOrderAndServiceCategory", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      TotalPurchasingByMonth: { endpoint: Constants.apiRoot + "api/purchasing/TotalPurchasing/ByMonthChart", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      TotalPurchasingByMonthCots: { endpoint: Constants.apiRoot + "api/purchasing/TotalPurchasing/ByMonth/Cots/Chart", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      TotalPurchasingByMonthServices: { endpoint: Constants.apiRoot + "api/purchasing/TotalPurchasing/ByMonth/Services/Chart", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      // Percentages
      AwardedVsContractCeiling: { endpoint: Constants.apiRoot + "api/purchasing/Percentages/AwardedVsContractCeiling", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      FundedVsContractCeiling: { endpoint: Constants.apiRoot + "api/purchasing/Percentages/FundedVsContractCeiling", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      SpentVsContractCeiling: { endpoint: Constants.apiRoot + "api/purchasing/Percentages/SpentVsContractCeiling", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      // Catalog
      CotsPurchaseDetails: { endpoint: Constants.apiRoot + "api/catalog/CotsPurchaseDetails", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user]},
      ServicePurchaseDetails: { endpoint: Constants.apiRoot + "api/catalog/ServicePurchaseDetails", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user]},
      CotsSummary: { endpoint: Constants.apiRoot + "api/catalog/CotsSummary", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user]},
      ServicesSummary: { endpoint: Constants.apiRoot + "api/catalog/ServiceSummary", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user]},
      // CSV Export
      ContractLevelSummaryCSVExport: { endpoint: Constants.apiRoot + "api/Purchasing/ContractLevelSummaryCosts/Export/CSV", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user]},
      TaskOrderPurchasingCSVExport: { endpoint: Constants.apiRoot + "api/Purchasing/TaskOrderPurchasing/Export/CSV", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user]},
      LaborHoursCSVExport: { endpoint: Constants.apiRoot + "api/Purchasing/LaborHours/Export/CSV", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user]},
      SpaceBasedCSVExport: { endpoint: Constants.apiRoot + "api/Purchasing/SpaceBased/Export/CSV", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user]},
      // Other
      contractList: { endpoint: Constants.apiRoot + "api/purchasing/contractList", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      TaskOrderClinCategoryList: { endpoint: Constants.apiRoot + "api/purchasing/TaskOrderClinCategoryList", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      TaskOrderClinList: { endpoint: Constants.apiRoot + "api/purchasing/TaskOrderClinList", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      TaskOrderList: { endpoint: Constants.apiRoot + "api/purchasing/TaskOrderList", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      totalContractExpenditure: { endpoint: Constants.apiRoot + "api/purchasing/totalContractExpenditure", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
    },
    itops: {
      DeviceHealth: { endpoint: Constants.apiRoot + "api/Network/DeviceHealth", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      DeviceHealthByMonth: { endpoint: Constants.apiRoot + "api/Network/DeviceHealthByMonth", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      DeviceHealthChart: { endpoint: Constants.apiRoot + "api/Network/DeviceHealthChart", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      DeviceHealthThreshold: { endpoint: Constants.apiRoot + "api/Network/DeviceHealthThreshold", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      CapacityUtilization: { endpoint: Constants.apiRoot + "api/Network/CapacityAndUtilizationLatest", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      NetworkMetrics: { endpoint: Constants.apiRoot + "api/Network/NetworkMetrics", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      CapUtilSummary: { endpoint: Constants.apiRoot + "api/Network/capandutilrollup", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      // SNow
      SNowHardwareInventoryManagement: { endpoint: Constants.apiRoot + "api/Snow/HardwareInventory/HardwareInventoryManagement", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user ] },
      SNowDayAgingByHardwareAsset: { endpoint: Constants.apiRoot + "api/Snow/HardwareInventory/DayAgingByHardwareAsset", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user ] },
      SNowHardwareAsset: { endpoint: Constants.apiRoot + "api/Snow/HardwareInventory/HardwareAsset", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user ] },
      SNowMaintenanceAgreementEndDates: { endpoint: Constants.apiRoot + "api/Snow/HardwareInventory/MaintenanceAgreementEndDates", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user ] },
      SNowClosed: { endpoint: Constants.apiRoot + "api/Snow/ServiceManagement/Closed", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user ] },
      SNowEscalated: { endpoint: Constants.apiRoot + "api/Snow/ServiceManagement/Escalated", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user ] },
      SNowInitiated: { endpoint: Constants.apiRoot + "api/Snow/ServiceManagement/Initiated", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user ] },
      SNowMTTR: { endpoint: Constants.apiRoot + "api/Snow/ServiceManagement/MTTR", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user ] },
      SNowOpenLastMonth: { endpoint: Constants.apiRoot + "api/Snow/ServiceManagement/OpenLastMonth", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user ] },
      SNowPreviousMonthDaily: { endpoint: Constants.apiRoot + "api/Snow/ServiceManagement/PreviousMonthDaily", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user ] },
      SNowSlaExceeded: { endpoint: Constants.apiRoot + "api/Snow/ServiceManagement/SlaExceeded", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user ] },
      SNowSlaMet: { endpoint: Constants.apiRoot + "api/Snow/ServiceManagement/SlaMet", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user ] },
      SNowSlaMetList: { endpoint: Constants.apiRoot + "api/Snow/ServiceManagement/SlaMetList", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user ] },
      SNowSlaNotMetList: { endpoint: Constants.apiRoot + "api/Snow/ServiceManagement/SlaNotMetList", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user ] },
      SNowAllSoftware: { endpoint: Constants.apiRoot + "api/Snow/ServiceManagement/SoftwareInventory/Allsoftware", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user ] },
      SNowSoftwareWithStatusPie: { endpoint: Constants.apiRoot + "api/Snow/ServiceManagement/ServiceManagement/SoftwareInventory/SoftwareWithStatusPie", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user ] },
    },
    servOps: {
      serviceLevelAgreement: { endpoint: Constants.apiRoot + "api/service/catalog/Sla", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] },
      serviceOutage: { endpoint: Constants.apiRoot + "api/service/serviceOutage", scopes: [Constants.ApplicationIdUri + Constants.scopes.access_as_user] }
    },
    secOps: {
    },
  }
}

/**
 * Scopes you add here will be prompted for user consent during sign-in.
 * By default, MSAL.js will add OIDC scopes (openid, profile, email) to any login request.
 * For more information about OIDC scopes, visit: 
 * https://docs.microsoft.com/en-us/azure/active-directory/develop/v2-permissions-and-consent#openid-connect-scopes
 */
export const loginRequest = {
  scopes: [...protectedResources.azure.user.profile.scopes]
};