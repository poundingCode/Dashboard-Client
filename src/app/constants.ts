import { environment } from '../environments/environment';

const spa = environment.spaKey;
const applicationIdUri = 'api://' + environment.apiKey + '/';
const apiRootUri = environment.apiUrl;

export class Constants {
  private static authority = 'https://login.microsoftonline.us/'+ environment.tenantId;
  public static apiRoot = apiRootUri;
  public static scopes = {
    access_as_user: 'access_as_user',
    people_Read: 'People.Read'
  };
  
  // note: concatinating the client id seems to fail. Use magic string for ravenVisionApi or ravenVisionApi
  public static ApplicationIdUri = applicationIdUri;//'api://1b0b607e-4ba9-40f3-a43d-026e9d93fb00/'

  // sync with appsettings.json
  public static  msalConfig = {
    auth: {
      clientId: spa,
      authority: Constants.authority,
      redirectUri: '/'
    },
    protectedResourceMap: 'https://graph.microsoft.us/v1.0/me',
    cache: {
      cacheLocation: "sessionStorage", // This configures where your cache will be stored
      storeAuthStateInCookie: false, // Set this to "true" if you are having issues on IE11 or Edge
    }
  };

  // Add scopes here for ID token to be used at Microsoft identity platform endpoints.
  public static  loginRequest = {
    scopes: ["openid", "profile", "User.Read"]
  };

  // Add scopes here for access token to be used at Microsoft Graph API endpoints.
  public static tokenRequest = {
    scopes: ["User.Read", "Mail.Read"]
  };
}
