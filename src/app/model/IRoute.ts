export type Route = {
    path: string,
    title: string,
    icon?: any,
    redirectTo?: string,
    pathMatch?: 'prefix' | 'full',
    component?: any,
    data?: any,
    canActivate?: any,
}

export interface IRoute extends Route {
    children?: IRoute[],
}
