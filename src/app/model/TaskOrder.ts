export class TaskOrder implements ITaskOrder {
    id!: string;
    TASK_ORDER_NAME!: string;
    START_DATE!: number;
    END_DATE!: string;
    STATUS!: string;
  }
  
  export interface ITaskOrder {
    id: string;
    TASK_ORDER_NAME: string;
    START_DATE: number;
    END_DATE: string;
    STATUS: string;
  }

export const STATUS_FILTER_OPTIONS = {
  'Active': 'active',
  'Inactive': 'inactive'
};