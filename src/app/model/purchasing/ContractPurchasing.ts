export class ContractPurchasing implements IContractPurchasing {
  clin_name!: string;
  contract!: string;
  task_order!: string
  clin!: string;
  sum!: string;
}

export interface IContractPurchasing {
  clin_name: string;
  contract: string;
  task_order: string;
  clin: string;
  sum: string;
}