export class TotalContractExpenditure implements ITotalContractExpenditure {
  YEAR!: string;  
  MAX_MONTH_DATE!: string;
  COMPONENT!: string;
  TASK_ORDER_ID!: number;
  TASK_ORDER_NO!: string;
  TASK_ORDER_NAME!: string;
  OBLIGATED!: number;
  CEILING!: number;
  SPENT!: number;
}
  
export interface ITotalContractExpenditure {
  YEAR: string;
  MAX_MONTH_DATE: string;
  COMPONENT: string;
  TASK_ORDER_ID: number;
  TASK_ORDER_NO: string;
  TASK_ORDER_NAME: string;
  OBLIGATED: number;
  CEILING: number;
  SPENT: number;
}