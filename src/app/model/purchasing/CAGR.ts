export class CompoundAnnualGrowthRate implements ICompoundAnnualGrowthRate {
  YEAR!: number;
  AMOUNT!: number;
  CAGR!: number;
}

export interface ICompoundAnnualGrowthRate {
  YEAR: number,
  AMOUNT: number,
  CAGR: number,
}
