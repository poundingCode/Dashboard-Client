export class IndividualResourceCosts implements IIndividualResourceCosts {
  YEAR!: number;
  QUARTER!: number;
  MONTH_DATE!: string;
  IDIQ_CLIN_NAME!: string;
  IDIQ_CLIN_NUMBER!: string;
  ENVIRONMENT_NAME!: string;
  RESOURCE_TYPE_NAME!: string;
  STORAGE_IN_GB!: number;
  AMOUNT!: number;
  COUNT!: number;
}

export interface IIndividualResourceCosts {
  YEAR: number;
  QUARTER: number;
  MONTH_DATE: string;
  IDIQ_CLIN_NAME: string;
  IDIQ_CLIN_NUMBER: string;
  ENVIRONMENT_NAME: string;
  RESOURCE_TYPE_NAME: string;
  STORAGE_IN_GB: number;
  AMOUNT: number;
  COUNT: number;
}
