export class ContractLevelSummaryCosts implements IContractLevelSummaryCosts {
  YEAR!: string; 
  QUARTER!: string; 
  MONTH_DATE!: string; 
  COMPONENT_ACRONYM!: string; 
  TASK_ORDER_ID!: string; 
  TASK_ORDER_NAME!: string; 
  TASK_ORDER_NUMBER!: string;
  CONTRACT_YEAR!: string; 
  CATEGORY_NAME!: string; 
  PERCENT_BY_TASK_ORDER!: string;
  AMOUNT!: string;
}

export interface IContractLevelSummaryCosts {
  YEAR: string; 
  QUARTER: string; 
  MONTH_DATE: string; 
  COMPONENT_ACRONYM: string; 
  TASK_ORDER_ID: string; 
  TASK_ORDER_NAME: string; 
  TASK_ORDER_NUMBER: string;
  CONTRACT_YEAR: string; 
  CATEGORY_NAME: string; 
  PERCENT_BY_TASK_ORDER: string;
  AMOUNT: string;
}

export class TotalPurchasingByTaskOrder implements ITotalPurchasingByTaskOrder {
  TASK_ORDER_ID!: number;
  TASK_ORDER_NO!: string;
  CONTRACT_YEAR!: string;
  TASK_ORDER_FUNDING!: number;
  TASK_ORDER_NAME!: string;
  AMOUNT!: number;
  SPEND_PCT!: number;
}

export interface ITotalPurchasingByTaskOrder {

  TASK_ORDER_ID: number;
  TASK_ORDER_NO: string;
  CONTRACT_YEAR: string;
  TASK_ORDER_FUNDING: number;
  TASK_ORDER_NAME: string;
  AMOUNT: number;
  SPEND_PCT: number;
}

export class ContractPurchasingByServiceCategory implements IContractPurchasingByServiceCategory {
  CATEGORY_NAME!: string;
  AMOUNT!: number;
  SPEND_PCT!: number;
}

export interface IContractPurchasingByServiceCategory {
  CATEGORY_NAME: string;
  AMOUNT: number;
  SPEND_PCT: number;
}
