export class AverageCostLabor implements IAverageCostLabor {
  id!: string;
  description!: string;
  date!: Date;
  category!: string;
}

export interface IAverageCostLabor {
  id: string;
  description: string;
  date: Date;
  category: string;
}

export class LaborHourCosts implements ILaborHourCosts {
  LABOR_CATEGORY_ID!: number;
  LABOR_CATEGORY_CODE!: string;
  LABOR_CATEGORY_DESCRIPTION!: string;
  IDIQ_CLIN_DESCRIPTION!: string;
  LABOR_HOURS!: number;
  AMOUNT!: number;
  RECORD_COUNT!: number;
  LABOR_RATE!: number;
}

export interface ILaborHourCosts {
  LABOR_CATEGORY_ID: number;
  LABOR_CATEGORY_CODE: string;
  LABOR_CATEGORY_DESCRIPTION: string;
  IDIQ_CLIN_DESCRIPTION: string;
  LABOR_HOURS: number;
  AMOUNT: number;
  RECORD_COUNT: number;
  LABOR_RATE: number;
}

export class LaborHourCostsDetail implements ILaborHourCostsDetail {
  YEAR!: string;
  QUARTER!: string;
  MONTH_DATE!: string;
  LABOR_CATEGORY_ID!: number;
  LABOR_CATEGORY_CODE!: string;
  LABOR_CATEGORY_DESCRIPTION!: string;
  IDIQ_CLIN_DESCRIPTION!: string;
  LABOR_HOURS!: number;
  AMOUNT!: number;
  RECORD_COUNT!: number;
  LABOR_RATE!: number;
}

export interface ILaborHourCostsDetail {
  YEAR: string;
  QUARTER: string;
  MONTH_DATE: string;
  LABOR_CATEGORY_ID: number;
  LABOR_CATEGORY_CODE: string;
  LABOR_CATEGORY_DESCRIPTION: string;
  IDIQ_CLIN_DESCRIPTION: string;
  LABOR_HOURS: number;
  AMOUNT: number;
  RECORD_COUNT: number;
  LABOR_RATE: number;
}
