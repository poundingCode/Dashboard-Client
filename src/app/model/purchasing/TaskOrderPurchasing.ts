export class TaskOrderPurchasing implements ITaskOrderPurchasing {
  YEAR!: string; 
  QUARTER!: string; 
  MONTH_DATE!: string; 
  COMPONENT_ACRONYM!: string; 
  TASK_ORDER_ID!: string; 
  TASK_ORDER_NO!: number;
  TASK_ORDER_NAME!: string; 
  CONTRACT_YEAR!: string; 
  CATEGORY_NAME!: string; 
  AMOUNT!: string;
  SPEND_PCT!: number;
}

export interface ITaskOrderPurchasing {
  YEAR: string; 
  QUARTER: string; 
  MONTH_DATE: string; 
  COMPONENT_ACRONYM: string; 
  TASK_ORDER_ID: string;
  TASK_ORDER_NO: number;
  TASK_ORDER_NAME: string; 
  CONTRACT_YEAR: string; 
  CATEGORY_NAME: string; 
  AMOUNT: string;
  SPEND_PCT: number;
}
