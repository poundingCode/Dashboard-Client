export class CotsPurchasing implements ICotsPurchasing {
  INVOICE_DATE!: string;
  INVOICE_ID!: string;
  YEAR!: number;
  QUARTER!: number;
  MONTH_DATE!: string;
  CONTRACT_PERIOD!: number;
  TASK_ORDER_NAME!: string;
  TASK_ORDER_NO!: string;
  SERVICE_CATEGORY!: string;
  CLIN!: number;
  CLIN_NAME!: string;
  COMPONENT!: string;
  ITEM_NAME!: string;
  ITEM_DESCRIPTION!: string;
  ITEM_QUANTITY!: number;
  ITEM_COST!: number;
  AMOUNT!: number;
}

export interface ICotsPurchasing {
  INVOICE_DATE: string;
  INVOICE_ID: string;
  YEAR: number;
  QUARTER: number;
  MONTH_DATE: string;
  CONTRACT_PERIOD: number;
  TASK_ORDER_NAME: string;
  TASK_ORDER_NO: string;
  SERVICE_CATEGORY: string;
  CLIN: number;
  CLIN_NAME: string;
  COMPONENT: string;
  ITEM_NAME: string;
  ITEM_DESCRIPTION: string;
  ITEM_QUANTITY: number;
  ITEM_COST: number;
  AMOUNT: number;
}

export class CotsSummary implements ICotsSummary {
  TASK_ORDER_NAME!: string;
  TASK_ORDER_NO!: string;
  SERVICE_CATEGORY!: string;
  ITEM_NAME!: string;
  ITEM_DESCRIPTION!: string;
  ITEM_QUANTITY!: number;
  AMOUNT!: number;
}

export interface ICotsSummary {
  TASK_ORDER_NAME: string;
  TASK_ORDER_NO: string;
  SERVICE_CATEGORY: string;
  ITEM_NAME: string;
  ITEM_DESCRIPTION: string;
  ITEM_QUANTITY: number;
  AMOUNT: number;
}
