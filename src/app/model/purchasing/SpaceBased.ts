export class SpaceBasedCosts implements ISpaceBasedCosts {
  YEAR!: string;
  QUARTER!: string;
  MONTH_DATE!: string;
  COMPONENT_ACRONYM!: string;
  TASK_ORDER_NAME!: string;
  CATEGORY_NAME!: string;
  CLIN_NAME!: string;
  LOCATION!: string;
  SQUARE_FOOTAGE!: string;
  AMOUNT!: string;
}

export interface ISpaceBasedCosts {
  YEAR: string;
  QUARTER: string;
  MONTH_DATE: string;
  COMPONENT_ACRONYM: string;
  TASK_ORDER_NAME: string;
  CATEGORY_NAME: string;
  CLIN_NAME: string;
  LOCATION: string;
  SQUARE_FOOTAGE: string;
  AMOUNT: string;
}