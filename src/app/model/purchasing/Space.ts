export class SpaceBasedCosts implements ISpaceBasedCosts {
  YEAR!: number;
  QUARTER!: number;
  MONTH_DATE!: string;
  TASK_ORDER_NAME!: string;
  IDIQ_CLIN_NAME!: string;
  CATEGORY_NAME!: string;
  LOCATION_NAME!: string;
  COMPONENT_ACRONYM!: string;
  SQUARE_FOOTAGE!: number;
  AMOUNT!: number;
  PRICE_PER_SQ_FT!: number;
}

export interface ISpaceBasedCosts {
  YEAR: number;
  QUARTER: number;
  MONTH_DATE: string;
  TASK_ORDER_NAME: string;
  IDIQ_CLIN_NAME: string;
  CATEGORY_NAME: string;
  LOCATION_NAME: string;
  COMPONENT_ACRONYM: string;
  SQUARE_FOOTAGE: number;
  AMOUNT: number;
  PRICE_PER_SQ_FT: number;
}