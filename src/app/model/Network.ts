export class NetworkMetrics implements INetworkMetrics {
  DisplayByOctet1!: string;
  DisplayByOctet2!: string;
  DisplayByOctet3!: string;
  LATENCY!: number;
  LatencyDeviceCount!: number;
  FLAG_AVAILABLE!: number;
  AvailDeviceCount!: number;
  BPS_IN!: number;
  BPS_OUT!: number;
  Bandwidth!: number;
}

export interface INetworkMetrics {
  DisplayByOctet1: string,
  DisplayByOctet2: string,
  DisplayByOctet3: string,
  LATENCY: number,
  LatencyDeviceCount: number,
  FLAG_AVAILABLE: number,
  AvailDeviceCount: number,
  BPS_IN: number,
  BPS_OUT: number,
  Bandwidth: number
}