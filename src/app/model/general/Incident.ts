export class IncidentNotification implements IIncidentNotification {
  NUMBER!: string;
  OPENED!: string;
  SEVERITY!: number;
  SYSTEM_ENVIRONMENT!: string;
  SHORT_DESCRIPTION!: string;
  STATE!: string;
}

export interface IIncidentNotification {
  NUMBER: string,
  OPENED: string,
  SEVERITY: number,
  SYSTEM_ENVIRONMENT: string,
  SHORT_DESCRIPTION: string,
  STATE: string,
}
