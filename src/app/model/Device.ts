export class DeviceHealth implements IDeviceHealth {
  COMPONENT!: string;
  CPU_UTILIZATION!: number;
  DEVICE_ID!: number;
  DEVICE_NAME!: string;
  DEVICE_TYPE!: string;
  LOCATION!: string;
  MEMORY_UTILIZATION!: number;
  MONTH!: number;
  TASK_ORDER!: string;
  YEAR!: number;
}

export interface IDeviceHealth {
  COMPONENT: string;
  CPU_UTILIZATION: number;
  DEVICE_ID: number;
  DEVICE_NAME: string;
  DEVICE_TYPE: string;
  LOCATION: string;
  MEMORY_UTILIZATION: number;
  MONTH: number;
  TASK_ORDER: string;
  YEAR: number;
}

export class DeviceHealthByMonth implements IDeviceHealthByMonth {
  YEAR!: number;
  MONTH!: number;
  MEMORY_UTILIZATION!: number;
  CPU_UTILIZATION!: number;
}

export interface IDeviceHealthByMonth {
  YEAR: number;
  MONTH: number;
  MEMORY_UTILIZATION: number;
  CPU_UTILIZATION: number;
}