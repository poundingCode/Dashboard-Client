export class SlaScore implements ISlaScore {
  SLA_VALUE_DATE!: string;
  MET!: number;
  EXCEEDED!: number;
  FAIL!: number;
  NA!: number;
}

export interface ISlaScore {
  SLA_VALUE_DATE: string,
  MET: number,
  EXCEEDED: number,
  FAIL: number,
  NA: number
}

export class SlaSummary implements ISlaSummary {
  SLA_NUMBER!: string;
  SERVICE_LEVEL!: string;
  SERVICE_CATEGORY!: string;
  ACCEPTED_QUALITY_LEVEL!: string;
  ['1/1/2023']!: Grade;
  ['2/1/2023']!: Grade;
  ['3/1/2023']!: Grade;
  ['4/1/2023']!: Grade;
  ['5/1/2023']!: Grade;
  ['6/1/2023']!: Grade;
  ['7/1/2023']!: Grade;
  ['8/1/2023']!: Grade;
  ['9/1/2023']!: Grade;
  ['10/1/2023']!: Grade;
  ['11/1/2023']!: Grade;
  ['12/1/2023']!: Grade;
}

export interface ISlaSummary {
  SLA_NUMBER: string;
  SERVICE_LEVEL: string;
  SERVICE_CATEGORY: string;
  ACCEPTED_QUALITY_LEVEL: string;
  ['1/1/2023']: Grade;
  ['2/1/2023']: Grade;
  ['3/1/2023']: Grade;
  ['4/1/2023']: Grade;
  ['5/1/2023']: Grade;
  ['6/1/2023']: Grade;
  ['7/1/2023']: Grade;
  ['8/1/2023']: Grade;
  ['9/1/2023']: Grade;
  ['10/1/2023']: Grade;
  ['11/1/2023']: Grade;
  ['12/1/2023']: Grade;
}

export class Grade implements IGrade {
  value!: string;
  outcome!: 'exceeded' | 'met' | 'failed' | 'notapplicable';
}

export interface IGrade {
  value: string;
  outcome: 'exceeded' | 'met' | 'failed' | 'notapplicable';
}

export const OutcomeToColorMap: { [k: string]: string } = {
  'exceeded': 'accent',
  'met': 'success',
  'failed': 'warn',
  'notapplicable': 'neutral',
  'default': '',
};

export const OutcomeToIconMap: { [k: string]: string } = {
  'exceeded': 'grade',
  'met': 'check',
  'failed': 'close',
  'notapplicable': 'remove',
  'default': '',
};

export const PERFORMANCE_ANALYSIS_FILTER_OPTIONS = {
  'Exceeded': 'exceeded',
  'Met': 'met',
  'Failed': 'failed',
  'Not Applicable': 'notapplicable',
};
