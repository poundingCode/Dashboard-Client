export class CapacityUtilization implements ICapacityUtilization {
  DEVICE_ID!: number;
  DEVICE_NAME!: string;
  COMPONENT!: string;
  CPU_UTILIZATION!: number;
  MEMORY_UTILIZATION!: number;
  DEVICE_TYPE!: string;
  TASK_ORDER!: string;
  LOCATION!: string;  
}

export interface ICapacityUtilization {
  DEVICE_ID: number;
  DEVICE_NAME: string;
  COMPONENT: string;
  CPU_UTILIZATION: number;
  MEMORY_UTILIZATION: number;
  DEVICE_TYPE: string;
  TASK_ORDER: string;
  LOCATION: string;
}