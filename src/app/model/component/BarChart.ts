import { Color, ScaleType } from '@swimlane/ngx-charts';
import { IChartOptions } from "./Chart";
import { ThemeColor } from '../Theme';


export class BarChartOptions implements IBarChartOptions, IChartOptions, IBarChartCustomOptions {
  title!: string;
  results!: any[];
  dataNameProperty!: string;
  dataValueProperty!: string;
  dataPercentPair!: [ string, string ];
  dataSeriesProperties!: [ string, string ];
  colorScheme!: string | Color;
  linkUrl!: string | undefined;
  panelIcon!: string;
  panelIconColor!: ThemeColor;
  tooltipType!: string;
  showXAxis!: boolean;
  showYAxis!: boolean;
  legend!: boolean;
  showXAxisLabel!: boolean;
  showYAxisLabel!: boolean;
  xAxisLabel!: string;
  yAxisLabel!: string;
  xAxisTickFormatting!: () => string;
  yAxisTickFormatting!: () => string;
  xScaleMax!: number;
  legendTitle!: string;
  chartType!: 'vert' | 'vertMulti' | 'vertStacked' | 'horiz' | 'horizMulti' | 'horizStacked' | undefined | null;
  useLog!: boolean;
  extraDataFormatter!: any;
}

export interface IBarChartOptions {
  showXAxis: boolean;
  showYAxis: boolean;
  legend: boolean;
  xAxisLabel: string;
  yAxisLabel: string;
  showXAxisLabel: boolean;
  showYAxisLabel: boolean;
  xAxisTickFormatting: () => string;
  yAxisTickFormatting: () => string;
  xScaleMax: number;
  legendTitle: string;
}

export class BarChartCustomOptions implements IBarChartCustomOptions {
  chartType!: 'vert' | 'vertMulti' | 'vertStacked' | 'horiz' | 'horizMulti' | 'horizStacked' | undefined | null;
}

export interface IBarChartCustomOptions {
  chartType: 'vert' | 'vertMulti' | 'vertStacked' | 'horiz' | 'horizMulti' | 'horizStacked' | undefined | null;
}
