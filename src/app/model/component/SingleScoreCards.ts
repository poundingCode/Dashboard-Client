export interface SingleScoreCard {
  title: string;
  count: string;
  textColor?: string;
  bgColor?: string;
  linkUrl?: string;
}
