import { ThemeColor } from "../Theme";

export type TableColumn = {
  dataValue: string,
  headerLabel?: string, // optional - table will generate column headers on its own if this isn't set
  cellFormatter?: any, // optional - table will use default dataValue property if this isn't set
  actionsFormatter?: any, // for displaying non-text content
  alignContent?: 'left' | 'right' | 'center',
  noFilter?: boolean,
  filterOptions?: { // whenever you need to override the auto-generated values in the filter dropdown
    [k: string]: string,
  },
  useFilterDropdown?: boolean, // gets the possible values from the table data and changes the free text filter field to a select dropdown
  filterProperty?: any, // if the column's data is an object, specify a property of that object
  filterType?: 'ANYWHERE' | 'EQUALS', // type of filter method for the column
}

export type TableData = [
  {
    [k: string]: any
  }
];

export type DataTableConfig = {
  tableTitle?: string,
  tableDataType: any,
  tableResourceUrl: any,
  customDisplayedColumns?: Array<string>,
  columnFormatter?: Array<TableColumn>,
  noService?: boolean, // true = do not store data from this table in the data-table service provider for the current dashboard
  tableLegend?: TableLegendItem[],
  panelIcon?: string;
  panelIconColor?: ThemeColor;
}

export type TableLegendItem = {
  legendText: string;
  legendIcon: string;
  legendColor: ThemeColor;
}