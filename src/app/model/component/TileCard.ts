/* 
  A tile can get its data from four different sources:
  - A 'displayValue' hard-coded into the tile's dashboard config file
  - A 'tileRowResourceUrl' at the tile row level
  - A 'cardResoureUrl' at the card level
  - Data passed from a data table on the same dashboard (which is now passed automatically - you can choose whether or not to use it by how the tile row is configured)

  All of the above data sources will write the value displayed in the tile to the 'displayValue' property.
  If you've set the 'displayValue' property directly in the config, your value will not be overwritten by any other data source.
*/


export interface ITileValue {
  displayValue?: string, // override for any value derived from data
  dataValue?: string, // the name of a field in the data source (if you have set a tileRowResourceUrl AND/OR cardResourceUrl)
  dataValueSecond?: string, // the name of a second field with which to do math (see next property)
  dataValueMath?: 'show' | 'projected' | 'subtract' | 'divideDisplayValByDataVal' | 'divideDataValByDisplayVal' | string, // how to process the data value from 'cardResourceUrl' - 'subtract' and 'divide' both require a 'dataValueSecond' property
  tableColumnName?: string, // the name of a table column, if you want to use data coming from a data table on the same dashboard
  tableFilterColumn?: string, // table column to filter by
  filterValue?: string, // value to keep
  tableColumnPropertyName?: string, // name of the nested property to use if the column data is an object
  tableColumnPropertyValues?: Array<any>, // an array of value of the above property to check against if the table column's data is an object
  tableColumnValuesExcludedFromTotal?: Array<any>, // array of values of the above property to exclude from the 'percentFilter' calculation
  tableColumnMath?: // how to process the data from 'tableColumnName'
  'sum' // get the sum of all values in tableColumnName
  | 'avg' // get the average of all values in tableColumnName
  | 'filteredAvg' // get the average of all values in tableColumnName, filtered by the filterValue in tableFilterColumn
  | 'percentFilter' // filter the tableColumnName and determine the percentage of the total records / table rows (ie., table has 50 rows, filter down to 25 rows = 50%)
  | 'countRecords' // get the number of records in the table column with a given value (string) or set of values (array)
  | 'highVal' // get the highest value in tableColumnName
  | 'lowVal' // get the lowest value in tableColumnName
  | 'stddev' // get the standard deviation of all values in the table column
  | 'highMonth' // sort the table data by month, sum each month by tableColumnName, and find the highest value
  | 'lowMonth'  // sort the table data by month, sum each month by tableColumnName, and find the lowest value
  | 'projected', // get a future value with a given increase percentage (currently hard-coded in constants)
  periodLabel?: string, // override - if you omit this value, the component will attempt to write a value using the periodType and period fields
  periodType?: 'next' | 'current' | 'previous' | 'year-ago',
  period?: 'month' | 'quarter' | 'year',
  periodMath?: 'high' | 'low',
  periodTableColumn?: string, // the 'date' table column - used when filtering for date periods (periodType and period properties above)
  recordsOf?: string, // how many records out of the total data rows are represented in the tile's display value, ie., 42 of 43
  showRecordsOf?: boolean, // whether to show recordsOf
}

export type TileCard = {
    cardType: 'base-blue' | 'base-red' | 'sec-blue' | 'tert-blue' | 'dark-blue',
    cardTitle: string,
    cardResourceUrl?: string,
    currentValue: ITileValue,
    prevValue?: ITileValue,
    numType?: '$' | '%' | string,
    invertTrend?: boolean, // When a downtrend in the current vs. prev comparison is good...
    hideTrend?: boolean, // When a trendline is not applicable to the two values shown
    linkUrl?: string, // Creates a clickable link on the card
    tileWidth?: 'tile-wide' | 'tile-third' | 'tile-quarter' | 'tile-narrow' | 'tile-half' | 'tile-full',
}

export type TileRow = TileCard[];

export type TileRowConfig = {
  tileRowConfig: TileCard[];
  tileRowResourceUrl?: string;
}
