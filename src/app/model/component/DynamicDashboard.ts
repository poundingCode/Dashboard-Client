import { DataTableConfig } from './DataTable';
import { TileRowConfig } from './TileCard';
import { IFrameConfig } from './Frame';
import { ImageContainerConfig } from './ImageContainer';
import { HTMLContainerConfig } from './HTMLContainer';
import { ExternalResourceConfig } from './ExternalResource';

export interface DynamicDashboardElement {
    elementType: 'DataTable' | 'TileRow' | 'IFrame' | 'ImageContainer' | 'HTMLContainer' | 'ExternalResourceLayout' | 'BarChart' | 'PieChart' | 'LineChart' | 'Gauge' | 'ChipList',
    elementInputs: any; // DataTableConfig | TileRowConfig | IFrameConfig | ImageContainerConfig | HTMLContainerConfig | ExternalResourceConfig | 'BarChartOptions' | 'PieChartOptions' | 'LineChartOptions' | 'GaugeOptions' | 'ChipListConfig',
    elementWidth?: 'fifth' | 'quarter' | 'third' | 'half' | 'twoThirds' | 'threeFourths' | 'full';
}

export type DashboardConfig = {
    title: string,
    elements: DynamicDashboardElement[],
}
