import { Color, LegendPosition } from '@swimlane/ngx-charts';
import { ThemeColor } from '../Theme';
import { IChartOptions } from "./Chart";

export class PieChartOptions implements IPieChartOptions, IChartOptions, IPieChartCustomOptions {
  title!: string;
  results!: any[];
  dataNameProperty!: string;
  dataValueProperty!: string;
  dataPercentPair!: [ string, string ];
  colorScheme!: string | Color;
  linkUrl!: string | undefined;
  panelIcon!: string;
  panelIconColor!: ThemeColor;
  tooltipType!: string;
  legend!: boolean;
  showLabels!: boolean;
  isDoughnut!: boolean;
  legendPosition!: LegendPosition;
  chartType!: 'normal' | 'advanced' | 'grid' | undefined | null;
  useLog!: boolean;
  extraDataFormatter!: any;
}

export interface IPieChartOptions {
  legend: boolean;
  showLabels: boolean;
  isDoughnut: boolean;
  legendPosition: LegendPosition;
}

export class PieChartCustomOptions implements IPieChartCustomOptions {
  chartType!: 'normal' | 'advanced' | 'grid' | undefined | null;
}

export interface IPieChartCustomOptions {
  chartType: 'normal' | 'advanced' | 'grid' | undefined | null;
}