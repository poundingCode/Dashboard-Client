import { ThemeColor } from '../Theme';

export interface Button {
  buttonType?: 'fab' | 'mini-fab' | 'flat',
  buttonIconType?: 'filled' | 'outlined',
  buttonLabel: string,
  hideLabel?: boolean,
  buttonIcon?: string,
  buttonColor?: ThemeColor,
  tooltipBackground?: ThemeColor,
  buttonAction?: string | (() => void),
  actionType?: 'modal' | 'button' | 'link' | undefined,
}