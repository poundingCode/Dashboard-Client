import { Color } from '@swimlane/ngx-charts';
import { ThemeColor } from '../Theme';
import { IChartOptions } from "./Chart";


export class LineChartOptions implements ILineChartOptions, IChartOptions {
  title!: string;
  results!: any[];
  dataNameProperty!: string;
  dataValueProperty!: string;
  colorScheme!: string | Color;
  linkUrl!: string | undefined;
  panelIcon!: string;
  panelIconColor!: ThemeColor;
  tooltipType!: string;
  showXAxis!: boolean;
  showYAxis!: boolean;
  legend!: boolean;
  showXAxisLabel!: boolean;
  xAxisLabel!: string;
  showYAxisLabel!: boolean;
  yAxisLabel!: string;
  xAxisTickFormatting!: () => string;
  yAxisTickFormatting!: () => string;
  legendTitle!: string;
  timeline!: boolean;
  useLog!: boolean;
  extraDataFormatter!: any;
}

export interface ILineChartOptions {
  showXAxis: boolean;
  showYAxis: boolean;
  legend: boolean;
  showXAxisLabel: boolean;
  xAxisLabel: string;
  showYAxisLabel: boolean;
  xAxisTickFormatting: () => string;
  yAxisTickFormatting: () => string;
  yAxisLabel: string;
  legendTitle: string;
  timeline: boolean;
}
