export interface ModalSettings {
  name?: string;
  value?: any;
  extra?: any;
  title: string;
  modalContentString?: string;
  modalContentType?: string | 'Pie' | 'BarVert' | 'BarHoriz' | 'BarStackedVert' | 'BarStackedHoriz' | 'BarMultiVert' | 'BarMultiHoriz';
  modalContentResourceUrl?: string;
  confirmButtonText?: string;
  cancelButtonText?: string;
  extraDataFormatter?: ExtraDataFormatter[];
}

export type ExtraDataFormatter = {
  dataValue: string,
  displayLabel?: string, // optional - modal will generate display values on its own if this isn't set
  valueFormatter?: any, // optional - modal will use default value property if this isn't set
}