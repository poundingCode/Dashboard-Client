import { ThemeColor } from "../Theme";

export type ImageContainerConfig = {
  imgTitle?: string;
  panelIcon?: string;
  panelIconColor?: ThemeColor;
  sourceUrl: string;
  linkUrl?: string;
  width?: string;
  height?: string;
  buttonLabel?: string;
}