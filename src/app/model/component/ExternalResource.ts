export type ExternalResource = {
  resourceTitle: string;
  logoImage: string;
  resourceSummary: string;
  bulletedList?: Array<any>;
  envMessage?: string;
  resourceUrl: string;
}

export type ExternalResourceConfig = ExternalResource;