import { SafeResourceUrl } from '@angular/platform-browser';
import { ThemeColor } from '../Theme';

export class IFrame implements IIFrame {
  frameTitle!: string;
  panelIcon!: string;
  panelIconColor!: ThemeColor
  sourceUrl!: string;
  safeUrl!: SafeResourceUrl;
  linkUrl!: string | undefined;
  linkToSrc!: boolean;
  width!: string;
  height!: string;
}

export interface IIFrame {
  frameTitle?: string;
  panelIcon?: string;
  panelIconColor?: ThemeColor;
  sourceUrl: string;
  safeUrl?: SafeResourceUrl;
  linkUrl?: string | undefined;
  linkToSrc?: boolean;
  width?: string;
  height?: string;
}

export type IFrameConfig = {
  iFrameData: IIFrame[];
}
