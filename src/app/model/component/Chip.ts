import { ThemeColor } from "../Theme";

export class Chip implements IChip {
  label!: string;
  color!: ThemeColor;
  icon!: string;
}

export interface IChip {
  label: string;
  color: ThemeColor;
  icon: string;
}

export type ChipList = {
  chipListConfig: Chip[];
}