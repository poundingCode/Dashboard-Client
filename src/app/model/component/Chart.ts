import { Color } from '@swimlane/ngx-charts';
import { ThemeColor } from '../Theme';

export class ChartOptions implements IChartOptions {
  title!: string;
  results!: any[];
  dataNameProperty!: string;
  dataValueProperty!: string;
  dataPercentPair!: [ string, string ];
  dataSeriesProperties!: [ string, string ]; // [ nameProperty, valueProperty ]
  colorScheme!: string | Color;
  linkUrl!: string | undefined;
  panelIcon!: string;
  panelIconColor!: ThemeColor;
  tooltipType!: string;
  useLog!: boolean;
  extraDataFormatter!: any;
}

export interface IChartOptions {
  title: string;
  results: any[];
  dataNameProperty: string;
  dataValueProperty: string;
  dataPercentPair?: [ string, string ];
  dataSeriesProperties?: [ string, string ];
  colorScheme: string | Color;
  linkUrl: string | undefined;
  panelIcon: string;
  panelIconColor: ThemeColor;
  tooltipType?: string;
  useLog?: boolean;
  extraDataFormatter?: any;
}
