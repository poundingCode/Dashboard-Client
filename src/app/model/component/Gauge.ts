import { Color, LegendPosition } from '@swimlane/ngx-charts';
import { ThemeColor } from '../Theme';
import { IChartOptions } from "./Chart";

export class GaugeOptions implements IGaugeOptions, IChartOptions {
  title!: string;
  results!: any[];
  dataNameProperty!: string;
  dataValueProperty!: string;
  colorScheme!: string | Color;
  linkUrl!: string | undefined;
  panelIcon!: string;
  panelIconColor!: ThemeColor;
  tooltipType!: string;
  legend!: boolean;
  showLabels!: boolean;
  legendPosition!: LegendPosition;
  min!: number;
  max!: number;
  valueFormatting!: () => string;
  axisTickFormatting!: () => string;
  singleNameProperty!: string;
  useLog!: boolean;
  extraDataFormatter!: any;
}

export interface IGaugeOptions {
  legend: boolean;
  showLabels: boolean;
  legendPosition: LegendPosition;
  min: number;
  max: number;
  valueFormatting: () => string;
  axisTickFormatting: () => string;
  singleNameProperty: string;
}
