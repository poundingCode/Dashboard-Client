import {Directive, Self} from '@angular/core';
import { GaugeComponent } from '@swimlane/ngx-charts';

@Directive({
    selector: 'ngx-charts-gauge'
})
export class NgxGaugeZeroMarginDirective {
    constructor(
      @Self() gauge: GaugeComponent
    ) {
      gauge.margin = [0, 0, -100, 0];
    }
}