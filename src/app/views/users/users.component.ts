import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSelectChange } from '@angular/material/select';
//import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { IUser, User } from './users';
import { UserService } from './user.service';
import {AppComponent } from '../../app.component'

@Component({
  selector: 'app-users-component',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})

export class UsersComponent implements  OnInit {

  public users!: IUser[];
  displayedColumns = ['manager', 'displayName',  'jobTitle', 'mail'];
  dataSource: MatTableDataSource<IUser> = new MatTableDataSource<IUser>([])
  service;
  //@ViewChild(MatSort, { static: false }) sort!: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;    
 
  constructor(service: UserService, private _appComponent: AppComponent) {
    console.log("users.component invoked");
    this.service = service;
  }

  ngAfterViewInit() {
    console.log("UserService.ngAfterViewInit invoked");
  }
    pgIndex= 0;
    firstLastButtons= true;
    pnDisabled= true;
    hdPageSize= true;

  async ngOnInit() {
    await this.getData();
  }

  async getData()  {
    console.log("UserService.getAll invoked");
    if (this._appComponent.isLoggedIn) {
    this.service.getAll().subscribe({
      next: (response) => {
        this.setUsers(response);
      },
      error: (error) => {
        console.log("user is null or expired." + error);
      }
  });
  }}

  setUsers(users: IUser[]) {
    console.log("setUsers invoked");
    this.users = users;
    
    this.dataSource = new MatTableDataSource<IUser>(this.users);
    this.dataSource.paginator = this.paginator;
   // this.dataSource.sort = this.sort;
  }
  
  onChangePage(pe:PageEvent) {
    console.log(pe.pageIndex);
    console.log(pe.pageSize);
  } 
}
