import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, IUser } from './users';
import { protectedResources } from '../../auth-config';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  url = protectedResources.azure.graph.users.endpoint;

  constructor(private http: HttpClient) { 
    console.log("user.service init");

  }

  getAll() : Observable<User[]>{ 
    console.log("UserService.getAll()")
    return this.http.get<User[]>(this.url);
  }

  get(id: number) { 
    return this.http.get<IUser>(this.url + '/' +  id);
  }
  
  post(systemowner: IUser) { 
    return this.http.post<IUser>(this.url, systemowner);
  }

  delete(id: number) {
    return this.http.delete(this.url + '/' + id);
  }

  edit(systemowner: IUser) { 
    return this.http.put<IUser>(this.url + '/' + systemowner.id, systemowner);
  }
}
