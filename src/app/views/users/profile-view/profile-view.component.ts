import { Component, OnInit } from '@angular/core';

import { AccountInfo } from '@azure/msal-browser';
import { MsalService } from '@azure/msal-angular';

import { ProfileService } from '../../../services/profile.service';
import { IProfile } from '../../../profile';

interface Account extends AccountInfo {
  idTokenClaims?: {
    oid?: string // "oid" claim helps us recognize a user
  }
}

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.scss']
})
export class ProfileViewComponent implements OnInit {

  profile: IProfile = {
    id: "",
    userPrincipalName: "",
    givenName: "",
    surname: "",
    jobTitle: "",
    managerName: "",
    mobilePhone: "",
    preferredLanguage: "",
    firstLogin: true
  };

  userExists = false;
  dataSource: any[] = [];
  photo: string = '';
  displayedColumns = ['claim', 'value'];

  constructor(private profileService: ProfileService, private authService: MsalService) { }

  ngOnInit(): void {

    // Our mock database assign user Ids based on MS Graph API account id, which corresponds to the "oid" claim in the id_token
    // visit https://docs.microsoft.com/en-us/azure/active-directory/develop/id-tokens for more information
    const account: Account = this.authService.instance.getAllAccounts()[0];
    this.getProfile(account.idTokenClaims?.oid!);
  }

  getProfile(id: string): void {
    this.profileService.getProfile(id)
      .subscribe({
        next: (profile: IProfile) => {
          this.userExists = true;
          this.profile = profile; 
          this.dataSource = Object.entries(this.profile);
        },
        error: (error) => {
          console.log(error)
          this.userExists = false;
        },
        complete:() =>{
          // this.photo = this.profileService.getPhotoUri(id);
        }
      });
  }

  submitProfile(profile: IProfile): void {
    this.profileService.postProfile(profile)
      .subscribe({
        next: (profile: IProfile) => {
          this.userExists = true;
          this.profile = profile;
          this.dataSource = Object.entries(this.profile);
        },
        error: (error) => {
          console.log(error)
          this.userExists = false;
        }
      })
  }
}
