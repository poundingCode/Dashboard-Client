import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ProfileService } from '../../../services/profile.service';
import { IProfile } from '../../../profile';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss']
})
export class ProfileEditComponent implements OnInit {

  public photo:string ='';

  profile: IProfile = {
    id: "",
    userPrincipalName: "",
    givenName: "",
    surname: "",
    jobTitle: "",
    managerName: "",
    mobilePhone: "",
    preferredLanguage: "",
    firstLogin: true,
  };

  constructor(private route: ActivatedRoute, private router: Router, private profileService: ProfileService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      let id: string = params.get('id')!;

      this.profileService.getProfile(id)
        .subscribe((profile: IProfile) => {
          this.profile = profile;
        })
    })
  }

  editProfile(profile: IProfile): void {
    this.profileService.editProfile(this.profile)
      .subscribe((ex) => {
        console.log(ex)
        this.router.navigate(['/profile-view']);
      })
  }
}
