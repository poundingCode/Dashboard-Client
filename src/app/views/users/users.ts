export class User implements IUser {
  id!: string;
  displayName!: string;
  givenName!: string;
  jobTitle!: string;
  mail!: string;
  surname!: string;
  manager!: IManager;
  managerName!: IManager;
}

export interface IUser {
  id: string;
  displayName: string;
  givenName: string;
  jobTitle: string;
  mail: string;
  surname: string;
  manager: IManager;
  managerName: IManager;
}

export interface IManager {
  displayName: string;
}