export interface IProfile {
  id: string,
  userPrincipalName: string,
  givenName: string,
  surname: string,
  jobTitle: string,
  mobilePhone: string,
  preferredLanguage: string,
  managerName: string,
  firstLogin: boolean
}
