import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MsalGuard } from '@azure/msal-angular';
import { BASE_ROUTES } from '../app/constants/routes/route-constants';
// providers
import { AppTitleStrategy } from './providers/browser-title.provider';
import { TitleStrategy } from '@angular/router';
// container
import { HomeComponent } from './views/general/home.component';
import { DynamicDashboardComponent } from './components/dynamic-dashboard/dynamic-dashboard.component';
// graph 
import { ProfileViewComponent } from './views/users/profile-view/profile-view.component';
import { ProfileEditComponent } from './views/users/profile-edit/profile-edit.component';
// routeConfigs
import { SERVICE_OFFERINGS_PAGE_CONFIG } from './constants/routes/routeConfigs/misc/service-offerings-page-config';
import { ORDERING_INFO_PAGE_CONFIG } from './constants/routes/routeConfigs/misc/ordering-info-page-config';
import { POINTS_OF_CONTACT_PAGE_CONFIG } from './constants/routes/routeConfigs/misc/points-of-contact-page-config';


/**
 * MSAL Angular can protect routes in your application
 * using MsalGuard. For more info, visit:
 * https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-angular/docs/v2-docs/initialization.md#secure-the-routes-in-your-application
 */
const GLOBAL_ROUTES: Routes = [
  { path: 'profile-edit/:id', component: ProfileEditComponent, canActivate: [MsalGuard] },
  { path: 'profile-view', component: ProfileViewComponent, canActivate: [MsalGuard] },
  { path: 'error', component: HomeComponent }, // Needed for hash routing
  { path: 'state', component: HomeComponent }, // Needed for hash routing
  { path: 'code', component: HomeComponent }, // Needed for hash routing
  { path: 'service-offerings', component: DynamicDashboardComponent, data: { dashboardConfig: SERVICE_OFFERINGS_PAGE_CONFIG } },
  { path: 'ordering-info', component: DynamicDashboardComponent, data: { dashboardConfig: ORDERING_INFO_PAGE_CONFIG } },
  { path: 'points-of-contact', component: DynamicDashboardComponent, data: { dashboardConfig: POINTS_OF_CONTACT_PAGE_CONFIG } },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'error', pathMatch: 'full' },
];

const routes: Routes = [...BASE_ROUTES, ...GLOBAL_ROUTES];

const isIframe = window !== window.parent && !window.opener;

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true,
    scrollPositionRestoration: 'enabled', // Go to top of page on route change
    // Don't perform initial navigation in iframes
    initialNavigation: !isIframe ? 'enabledNonBlocking' : 'disabled'
  })],
  exports: [RouterModule],
  providers: [
    {
      provide: TitleStrategy,
      useClass: AppTitleStrategy
    }
  ]
})
export class AppRoutingModule { }
